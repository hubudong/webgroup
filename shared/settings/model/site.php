<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['models']['site']=array (
  'id' => '2',
  'name' => 'site',
  'description' => '站点',
  'perpage' => '20',
  'hasattach' => '0',
  'built_in' => '0',
  'thumb_preferences' => NULL,
  'fields' => 
  array (
    8 => 
    array (
      'id' => '8',
      'name' => 'site_name',
      'description' => '站点名称',
      'model' => '2',
      'type' => 'input',
      'length' => '0',
      'values' => '',
      'width' => '0',
      'height' => '0',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '1',
      'listable' => '1',
      'order' => '0',
      'editable' => '1',
    ),
    9 => 
    array (
      'id' => '9',
      'name' => 'site_url',
      'description' => '站点地址',
      'model' => '2',
      'type' => 'input',
      'length' => '0',
      'values' => '',
      'width' => '0',
      'height' => '0',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '1',
      'listable' => '1',
      'order' => '1',
      'editable' => '1',
    ),
  ),
  'listable' => 
  array (
    0 => '8',
    1 => '9',
  ),
  'searchable' => 
  array (
    0 => '8',
    1 => '9',
  ),
);