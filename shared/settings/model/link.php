<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['models']['link']=array (
  'id' => '7',
  'name' => 'link',
  'description' => '链接表',
  'perpage' => '20',
  'hasattach' => '1',
  'built_in' => '0',
  'thumb_preferences' => '{"enabled":[],"default":"original"}',
  'fields' => 
  array (
    30 => 
    array (
      'id' => '30',
      'name' => 'link_name',
      'description' => '链接名',
      'model' => '7',
      'type' => 'input',
      'length' => '0',
      'values' => '',
      'width' => '0',
      'height' => '0',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '1',
      'listable' => '1',
      'order' => '0',
      'editable' => '1',
    ),
    31 => 
    array (
      'id' => '31',
      'name' => 'link_url',
      'description' => '链接地址',
      'model' => '7',
      'type' => 'input',
      'length' => '0',
      'values' => '',
      'width' => '0',
      'height' => '0',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '1',
      'listable' => '1',
      'order' => '1',
      'editable' => '1',
    ),
    32 => 
    array (
      'id' => '32',
      'name' => 'link_cate',
      'description' => '链接分类',
      'model' => '7',
      'type' => 'select_from_model',
      'length' => '0',
      'values' => 'link_cate|link_cate_name',
      'width' => '0',
      'height' => '0',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '2',
      'editable' => '1',
    ),
    33 => 
    array (
      'id' => '33',
      'name' => 'site',
      'description' => '站点',
      'model' => '7',
      'type' => 'select_from_model',
      'length' => '0',
      'values' => 'site|site_name',
      'width' => '0',
      'height' => '0',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '3',
      'editable' => '1',
    ),
  ),
  'listable' => 
  array (
    0 => '30',
    1 => '31',
    2 => '32',
    3 => '33',
  ),
  'searchable' => 
  array (
    0 => '30',
    1 => '31',
  ),
);