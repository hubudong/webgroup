<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['category']['templates']=array (
  1 => 
  array (
    'classid' => '1',
    'parentid' => '0',
    'level' => '1',
    'path' => '{0}',
    'template_name' => '院校模板',
    'index' => '<div class="banner" id="banner"></div>
		<script type="text/javascript">
				var bannerSource=[
						@@Banner@@
				];
				var bannerObj=$(\'#banner\'),tmpBc="",bannerCount=0,bannerLen=bannerSource.length;
				for(var bI=0;bI<bannerLen;bI++){
						tmpBc+=\'<a href="\'+bannerSource[bI].url+\'" target="blank"><img src="\'+bannerSource[bI].pic+\'" alt="\'+bannerSource[bI].txt+\'" title="\'+bannerSource[bI].txt+\'" /></a>\';
				}
				bannerObj.html(tmpBc);
				var bannerImgObj=$(\'#banner img\');
				var bannerShow=function(index){
						bannerImgObj.not(":eq("+index+")").hide(\'fast\');
						bannerImgObj.eq(index).fadeIn(\'fast\');
				}
				var bannerRun=function(){
						if(bannerCount==bannerLen){
								bannerCount=0;
						};
						bannerShow(bannerCount);
						bannerCount++;
						setTimeout(bannerRun,10000);
				}
				bannerRun();
		</script>
		<div class="main clr">
				<div class="focus">
						<!--焦点图容器-开始-->
						<div id="focus">
								<div id="focuspic"></div>
								<div id="focustitle"></div>
								<div id="focusnav"></div>
						</div>
						<!--焦点图容器-结束-->
						<script type="text/javascript">
						//定义焦点图的资源
						var picArray=[
								@@焦点图@@
						];var len = picArray.length;
						var titleArray=[@@焦点图标题@@];
						var linkArray=[@@焦点图链接@@];

						//设定焦点图参数
						var para={
								focusAreaBox:"focus",//焦点图大容器ID
								focusNavBox:"focusnav",//焦点图数字导航容器ID
								focusPicBox:"focuspic",//焦点图图片容器ID
								//focusTitleBox:"focustitle",//焦点图标题容器ID
								focusPics:picArray,//焦点图图片数组
								focusTitle:titleArray,//焦点图标题数组
								focusLinks:linkArray,//焦点图链接数组
								focusNums:len ,//需要显示的焦点图个数
								focusSpeed:5000,//焦点图自动切换的时间（单位毫秒）
								focusWidth:"365px",//焦点图宽度
								focusHeight:"300px"//焦点图高度
						};
						//建立焦点图对象
						var focusObj=new Focus(para)
						//运行焦点图
						focusObj.run();
				</script>
				</div>
				<div class="newsMoudle">
						<div class="newNav clr">
								<div class="tab current">本系新闻</div>
								<div class="tab">通知公告</div>
								<div id="dTime"></div>
						</div>
						<ul class="newsUl news" id="news">
@@新闻@@
								<!--li class="newsLi clr"><a href="#"><span class="newsTxt">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li-->
						</ul>
						<ul class="newsUl notice" id="notice">
@@通知@@
								<!--li class="newsLi clr"><a href="#"><span class="newsTxt">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li>
								<li class="newsLi clr"><a href="#"><span class="newsTxt">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class="newsTime">[2013-12-12]</span></a></li-->
						</ul>
				</div>
		</div>',
    'common_top' => '<!doctype html>
<html>
<head>
<meta charset="gb2312">
<title>@@网站名称@@</title>
<link href="../templates/temp1/style/global.css" rel="stylesheet" type="text/css">
<link href="../templates/temp1/style/index.css" rel="stylesheet" type="text/css">
<link href="../templates/temp1/style/subpage.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../templates/temp1/script/jquery.js"></script>
<script type="text/javascript" src="../templates/temp1/script/jQuery.imgFloat.js"></script>
<script type="text/javascript" src="../templates/temp1/script/jQuery.focus.1.0.min.js"></script>
<script type="text/javascript" src="../templates/temp1/script/ctd.js"></script>
<style type="text/css">
/*漂浮图片start*/
.imgFloat{width:250px;height:100px;position:absolute;background-image:url(template/images/imgFloat.jpg); z-index:999;}
#java{background-position:0 -100px;}
/*漂浮图片end*/
</style>
</head>
<body>
<!--漂浮图片start-->
<!--div id="iphone" class="imgFloat" title="iPhone手机开发工程师方向·深圳软件行业协会订单班"></div>
<div id="java" class="imgFloat" title="Java软件外包方向·中软国际定向班"></div-->
<script type="text/javascript">
/*
$("#iphone").imgFloat({speed:40,xPos:10,yPos:1000});
$("#iphone").click(function (){window.open("#");});
$("#java").imgFloat({speed:40,xPos:10,yPos:10});
$("#java").click(function (){window.open("#");});
*/
</script>
<!--漂浮图片end-->
<div class="topBg"></div>
<div class="warp">
		<div class="top clr">
				<div id="logo" title="返回首页"><img src = "../attachments/@@Logo@@"></div>
				<div class="searchArea"> <a href="@@首页链接@@">首页</a> <a href="#" onclick="SetHome(this,window.location)">设为首页</a> <a  href="#" onclick="AddFavorite(window.location,document.title)" >收藏本站</a> <a href="#"><span class="enEditon">English</span></a>
						<form action="@@搜索@@" method="post">
								<input id="keywords" type="text" name="keywords" placeholder="请输入关键字" speech x-webkit-speech class="inputArea" />
								<input name="site" type="hidden" value="@@站点@@" />
                                                                <input type="submit" value="搜索" />
						</form>
				</div>
		</div>
		<div class="nav">
				<div class="navBg navLeft"></div>
				<ul class="navUl clr">
						<li class="navLi"><a href="@@Site_index@@">首　　页</a></li>
						@@一级栏目@@
				</ul>
				<div class="navBg navRight"></div>
		</div>
		<div class="subNav">
				<div id="subNavItems">
						<div class="subNavItem">欢迎来到@@网站名称@@^_^</div>
						@@二级栏目@@
				</div>
		</div>',
    'common_foot' => '<div class="footNav">
				<div class="footNavBg footNavLeft"></div>
				<div class="quickLinks"> 快速访问： @@链接@@...... </div>
				<div class="cooperation">合作单位：
						<select name="copr">
								@@单位@@
						</select>
				</div>
				<div class="footNavBg footNavRight"></div>
		</div>
		<div class="copy">@@copyRight@@</div>
</div>
</body>
<script>
		function AddFavorite(sURL, sTitle){
		    try{
		        window.external.addFavorite(sURL, sTitle);
		    }
		    catch (e){
		        try{
		            window.sidebar.addPanel(sTitle, sURL, "");
		        }
		        catch (e){
		            alert("加入收藏失败，请使用Ctrl+D进行添加");
		        }
		    }
		}
		//设为首页 <a onclick="SetHome(this,window.location)">设为首页</a>
		function SetHome(obj,vrl){
	        try{
	            obj.style.behavior=\'url(#default#homepage)\';obj.setHomePage(vrl);
	        }
	        catch(e){
	            if(window.netscape) {
	                try {
	                    netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
	                }
	                catch (e) {
	                    alert("此操作被浏览器拒绝！\\n请在浏览器地址栏输入“about:config”并回车\\n然后将 [signed.applets.codebase_principal_support]的值设置为\'true\',双击即可。");
	                }
	                var prefs = Components.classes[\'@mozilla.org/preferences-service;1\'].getService(Components.interfaces.nsIPrefBranch);
	                 prefs.setCharPref(\'browser.startup.homepage\',vrl);
	            }
	        }
		}
	</script>
</html>',
    'list' => '<div class="mainList clr">
				<div class="subPageMenu">
						<div class="subPageMenuTit">
								<span class="cnTxt">@@栏目名1@@</span>
						</div>
						<div class="subPageMenuList">
								<ul class="subPageMenuUl">

										<!--li class="subPageMenuLi"><a href="#">本系概括</a></li>
										<li class="subPageMenuLi"><a href="#">现任领导</a></li>
										<li class="subPageMenuLi"><a href="#">所获荣誉</a></li>
										<li class="subPageMenuLi"><a href="#">校企合作</a></li>
										<li class="subPageMenuLi"><a href="#">学生获奖</a></li>
										<li class="subPageMenuLi"><a href="#">优秀毕业生</a></li-->
                                                                              @@侧边导航@@
								</ul>
						</div>
						<div class="links">
								<select name="links">
										@@友情链接@@
								</select>
						</div>
				</div>
				<div class="subPageContentArea">
						<div class="subPagePath">您现在所在的位置是：<a href="@@首页链接@@">首页</a> > <a href="@@栏目链接@@">@@栏目名1@@</a> > @@栏目名2@@</div>
						<div class="subPageContent"><ul>@@列表页@@</ul></div>
                                               <div class="pageDown">@@翻页@@</div>
				</div>
		</div>',
    'content' => '<div class="mainList clr">
				<div class="subPageMenu">
						<div class="subPageMenuTit">
								<span class="cnTxt">@@栏目名1@@</span>
						</div>
						<div class="subPageMenuList">
								<ul class="subPageMenuUl">
										<!--li class="subPageMenuLi"><a href="#">本系概括</a></li>
										<li class="subPageMenuLi"><a href="#">现任领导</a></li>
										<li class="subPageMenuLi"><a href="#">所获荣誉</a></li>
										<li class="subPageMenuLi"><a href="#">校企合作</a></li>
										<li class="subPageMenuLi"><a href="#">学生获奖</a></li>
										<li class="subPageMenuLi"><a href="#">优秀毕业生</a></li-->
                                                                        @@侧边导航@@
								</ul>
						</div>
						<div class="links">
								<select name="links">
										@@友情链接@@
								</select>
						</div>
				</div>
				<div class="subPageContentArea">
						<div class="subPagePath">您现在所在的位置是：<a href="@@首页链接@@">首页</a> > <a href="@@栏目链接@@">@@栏目名1@@</a> > @@栏目名2@@</div>
						<div class="subPageContent"><p style="text-align:center;font-size:18px;">@@文章标题@@</p><p style="text-align:center;font-size:12px;">@@文章详细@@</p>@@文章内容@@</div>
				</div>
		</div>',
    'msg_board' => '<div class=\'mainList clr\'>
				<div class=\'subPageMenu\'>
						<div class=\'subPageMenuTit\'>
								<span class=\'cnTxt\'>留言</span>
								<span class=\'enTxt\'>Message</span>
						</div>
				</div>
				<div class=\'subPageContentArea\'>
						<div class=\'subPagePath\'><span>请写下您的疑问</span></div>
						<div class=\'subPageContent\'>
							<form action=\'@@留言@@\' method=\'post\' class=\'msg clr\'>
								<p>标&nbsp;&nbsp;题:&nbsp;<input type=\'text\' name=\'title\' id=\'title\' value=\'\' /></p>
								<p>e-mail&nbsp;:&nbsp;<input type=\'email\' name=\'lxfs\' id=\'lxfs\' value=\'\' /></p>
								<span>内&nbsp;&nbsp;容:</span><textarea cols=\'40\' rows=\'10\' style="margin-left:20px"></textarea>
								<p style=\'text-align:center;\'><input type=\'submit\' value=\'提交\' /></p>
							</form>
						</div>
				</div>
		</div>',
    'search' => '<div class="mainList clr">
				<div class="subPageMenu">
						<div class="subPageMenuTit">
								<span class="cnTxt">搜索</span>
								<span class="enTxt">Search</span>
						</div>
				</div>
				<div class="subPageContentArea">
						<div class="subPagePath">您现在搜索的关键字是：<a href="#">@@关键字@@</a> <span>共有@@数量@@条</span></div>
						<div class="subPageContent">
							<ul>
<!--
								<li class="newsLi clr"><a href="#"><span class="newsTxt">测试base_url1</span><span class="newsTime">2016-09-22</span></a></li>
-->
                                                                     @@搜索列表@@
							</ul>
						</div>
                                     <div class="pageDown">@@翻页@@</div>
				</div>
		</div>',
    'children' => '0',
    'deep' => 0,
  ),
  2 => 
  array (
    'classid' => '2',
    'parentid' => '0',
    'level' => '1',
    'path' => '{0}',
    'template_name' => '院校模板1',
    'index' => '<!--中间-->
	<div class="content">
		<!--图片轮播区域-->
		<div id="rotator">
		<script type=text/javascript>
			jQuery(function($) {$(document).ready(function() {
				$(\'#rotator\').crossSlide(
					{sleep: 2, fade: 1, debug: true},
					[
						@@Banner@@
					]
				);
				});
			});
		</script>
		</div>
		<!--图片轮播区域-->
		<div class="box">
			<!--中间上面-->
			<div class="box-1 clear">
				<!--通知公告-->
				<div class="Inform">
					<div class="Ititle clear">
						<span class="notice fl">通知公告</span>
						<span class="more fr"><a href="#"><img src="../templates/temp2/img/more.gif" /></a></span>
					</div>
					<div class="Icontent">
						<ul>
@@新闻@@
						</ul>
					</div>
				</div>
				<!--通知公告-->
				<!--图片新闻-->
				<div class="flash">
					<div class="mov">
						<!--焦点图容器-开始-->
						<div id="focus">
								<div id="focuspic"></div>
								<div id="focustitle"></div>
								<div id="focusnav"></div>
						</div>
						<!--焦点图容器-结束-->
						<script type="text/javascript">
						//定义焦点图的资源
						var picArray=[
								@@焦点图@@
						];
var len = picArray.length;
						var titleArray=[@@焦点图标题@@];
						var linkArray=[@@焦点图链接@@];
						//设定焦点图参数
						var para={
								focusAreaBox:"focus",//焦点图大容器ID
								focusNavBox:"focusnav",//焦点图数字导航容器ID
								focusPicBox:"focuspic",//焦点图图片容器ID
								//focusTitleBox:"focustitle",//焦点图标题容器ID
								focusPics:picArray,//焦点图图片数组
								focusTitle:titleArray,//焦点图标题数组
								focusLinks:linkArray,//焦点图链接数组
								focusNums:len ,//需要显示的焦点图个数
								focusSpeed:5000,//焦点图自动切换的时间（单位毫秒）
								focusWidth:"375px",//焦点图宽度
								focusHeight:"330px"//焦点图高度
						};
						//建立焦点图对象
						var focusObj=new Focus(para)
						//运行焦点图
						focusObj.run();
					</script>
					</div>
				</div>
				<!--图片新闻-->
				<!--新闻动态-->
				<div class="iNews">
					<div class="Ntitle">
						<span class="notice fl">新闻动态</span>
						<span class="more fr"><a href="#"><img src="../templates/temp2/img/more.gif" /></a></span>
					</div>
					<div class="Ncontent">
						<ul>
@@通知@@
              			</ul>
					</div>
				</div>
				<!--新闻动态-->
			</div>
			<!--中间上面-->
		</div>
	</div>',
    'common_top' => '<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>@@网站名称@@</title>
		<!--中部-->
		<link rel="stylesheet" href="../templates/temp2/css/base_index.css" />
		<!--底部-->
		<script type="text/javascript" src="../templates/temp2/js/main.js" ></script>
		<script type="text/javascript" src="../templates/temp2/js/jquery.cross-slide.js" ></script>
		<script type="text/javascript" src="../templates/temp2/js/jQuery.focus.1.0.min.js" ></script>
		<style type="text/css">
			#rotator{
				width: 1024px;
				height:200px;
				margin:10px auto;
				z-index:-1;
			}
		</style>
	</head>
	<body>
	<!--top start-->
	<div class="top clr">
<div class="logo fl">
		<img src="../attachments/@@Logo@@" height="100">
</div>
<div class="searchArea fr"> <a href="@@首页链接@@">首页</a> <a href="#" onclick="SetHome(this,window.location)">设为首页</a> <a  href="#" onclick="AddFavorite(window.location,document.title)" >收藏本站</a> 
						<form action="@@搜索@@" method="post">
								<input id="keywords" type="text" name="keywords" placeholder="请输入关键字" speech x-webkit-speech class="inputArea" />
								<input name="site" type="hidden" value="@@站点@@" />
                                                                <input type="submit" value="搜索" />
						</form>
				</div>
	</div>
	<div id="nav" class="nav">
	    <ul class="sf-menu">
<li style="border-top-left-radius:10px;"><a href="@@首页链接@@" id="a1"><span class="arrow">首页</span></a></li>
@@一级栏目@@
		</ul>
	</div>
	<!--导航条区域-->
	<script type="text/javascript">    
	    $("#nav .sf-menu").supersubs().superfish();   
	</script>
	<!--top end-->',
    'common_foot' => '<!--bottom start-->
	<div class="links">
			<div class="lt clr">
				<span class="ft fl">友情链接</span>
			</div>
			<div class="lcon">
			</div>
		</div>
		<div class="foot">
@@copyRight@@
		</div>
	<!--bottom end-->
	</body>
<script>
		function AddFavorite(sURL, sTitle){
		    try{
		        window.external.addFavorite(sURL, sTitle);
		    }
		    catch (e){
		        try{
		            window.sidebar.addPanel(sTitle, sURL, "");
		        }
		        catch (e){
		            alert("加入收藏失败，请使用Ctrl+D进行添加");
		        }
		    }
		}
		//设为首页 <a onclick="SetHome(this,window.location)">设为首页</a>
		function SetHome(obj,vrl){
	        try{
	            obj.style.behavior=\'url(#default#homepage)\';obj.setHomePage(vrl);
	        }
	        catch(e){
	            if(window.netscape) {
	                try {
	                    netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
	                }
	                catch (e) {
	                    alert("此操作被浏览器拒绝！\\n请在浏览器地址栏输入“about:config”并回车\\n然后将 [signed.applets.codebase_principal_support]的值设置为\'true\',双击即可。");
	                }
	                var prefs = Components.classes[\'@mozilla.org/preferences-service;1\'].getService(Components.interfaces.nsIPrefBranch);
	                 prefs.setCharPref(\'browser.startup.homepage\',vrl);
	            }
	        }
		}
	</script>
</html>',
    'list' => '<div class="box clr">
		<!--left-->
		<div class="left">
			<div id="lnavtop"></div>
			<div class="navleft">
				<div id="navtitle">@@栏目名1@@</div>
	            <div class="navbody">
	            	<ul>
@@侧边导航@@
	                </ul>
	            </div>
			</div>
			<div id="lnavbottom">
			</div>
		</div>
		<!--left-->
		<!--right-->
		<div class="right">
			<div class="rtitle">
				<ul>
	            	<li style="width:100px; height:38px; text-align:center;	background-image:url(../templates/temp2/img/dt-5.jpg);">@@栏目名2@@</li>
	                <li style="width:614px; height:37px; text-align:right; background-image:url(../templates/temp2/img/dt-6.jpg); padding:0 10px 0 0">当前位置：<a href="@@首页链接@@">网站首页</a> >> <a href=\'@@栏目链接@@\'>@@栏目名1@@</a> </li>
            	</ul>
			</div>
			<div class="rbody">
				<div class="rbodyin">
					<ul>
	                	<li style="font-size:16px; font-weight:bold;text-align:center;"></li>
	                    <li><hr /></li>
	                    @@列表页@@
	                    <li><hr /></li>
	                    <li style="margin-top:5px;"> @@翻页@@</li>
                	</ul>
				</div>
			</div>
		</div>
		<!--right-->
	</div>',
    'content' => '<div class="box clr">
		<!--left-->
		<div class="left">
			<div id="lnavtop"></div>
			<div class="navleft">
				<div id="navtitle">@@栏目名1@@</div>
	            <div class="navbody">
	            	<ul>
@@侧边导航@@
	                </ul>
	            </div>
			</div>
			<div id="lnavbottom">
			</div>
		</div>
		<!--left-->
		<!--right-->
		<div class="right">
			<div class="rtitle">
				<ul>
	            	<li style="width:100px; height:38px; text-align:center;	background-image:url(../templates/temp2/img/dt-5.jpg);">@@栏目名2@@</li>
	                <li style="width:614px; height:37px; text-align:right; background-image:url(../templates/temp2/img/dt-6.jpg); padding:0 10px 0 0">当前位置：<a href="@@首页链接@@">网站首页</a> >> <a href=\'@@栏目链接@@\'>@@栏目名1@@</a> </li>
            	</ul>
			</div>
			<div class="rbody">
				<div class="rbodyin">
					<ul>
	                	<li style="font-size:16px; font-weight:bold;text-align:center;">@@文章标题@@</li>
	                    <li style="font-size:12px; color:#999; margin:10px auto; text-align:center;"> @@文章详细@@</li>
	                    <li><hr style="width:715px; border-style:dashed; color:#999; margin:5px auto;" /></li>
	                    <div class="contents">
@@文章内容@@
	                    </div>
	                    <li><hr style="width:715px; border-style:dashed; color:#999; margin:5px auto;" /></li>
                	</ul>
				</div>
			</div>
		</div>
		<!--right-->
	</div>',
    'msg_board' => '<div class="box clr">
		<!--left-->
		<div class="left">
			<div id="lnavtop"></div>
			<div class="navleft">
				<div id="navtitle">留言</div>
	            <div class="navbody">
	            	<ul>
	            		<li>在这里可以写下您的意见</li>
	                </ul>
	            </div>
			</div>
			<div id="lnavbottom">
			</div>
		</div>
		<!--left-->
		<!--right-->
		<div class="right">
			<div class="rtitle">
				<ul>
	            	<li style="width:100px; height:38px; text-align:center;	background-image:url(../templates/temp2/img/dt-5.jpg);">留言</li>
	                <li style="width:614px; height:37px; text-align:right; background-image:url(../templates/temp2/img/dt-6.jpg); padding:0 10px 0 0">当前位置：<a href="index.asp">网站首页</a> >> <a href=\'newslist.asp?lm=1\'>留言</a> </li>
            	</ul>
			</div>
			<div class="rbody">
				<div class="rbodyin">
					<ul>
	                	<li style="font-size:16px; font-weight:bold;text-align:center;">留言</li>
	                    <li><hr /></li>
							<div class=\'subPageContent\'>
								<form action=\'@@留言@@\' method=\'post\' class=\'msg clr\'>
									<p>标&nbsp;&nbsp;题:&nbsp;&nbsp;&nbsp;<input type=\'text\' name=\'title\' id=\'title\' value=\'\' /></p>
									<input type="hidden" name="site" value="@@站点ID@@">
									<input type="hidden" name="site_url" value="@@站点URL@@">
									<p>e-mail&nbsp;:&nbsp;<input type=\'email\' name=\'lxfs\' id=\'lxfs\' value=\'\' /></p>
									<p><div class="ta clr"><span>内容:</span><textarea cols=\'40\' rows=\'10\' name="content"  style="margin-left:20px"></textarea></div></p>
									<p><input type=\'submit\' value=\'提交\' /></p>
								</form>
							</div>
	                    <li><hr /></li>
                	</ul>
				</div>
			</div>
		</div>
		<!--right-->
	</div>',
    'search' => '<!--top end-->
	<div class="box clr">
		<!--left-->
		<div class="left">
			<div id="lnavtop"></div>
			<div class="navleft">
				<div id="navtitle">搜索</div>
	            <div class="navbody">
	            	<ul>
	                	<li>您搜索的(@@关键字@@)</li>
						<li>结果共有@@数量@@条</li>
	                </ul>
	            </div>
			</div>
			<div id="lnavbottom">
			</div>
		</div>
		<!--left-->
		<!--right-->
		<div class="right">
			<div class="rtitle">
				<ul>
	            	<li style="width:100px; height:38px; text-align:center;	background-image:url(../templates/temp2/img/dt-5.jpg);">搜索</li>
	                <li style="width:614px; height:37px; text-align:right; background-image:url(../templates/temp2/img/dt-6.jpg); padding:0 10px 0 0">当前位置：<a href="@@首页链接@@">网站首页</a> >> <a href=\'#\'>搜索</a> </li>
            	</ul>
			</div>
			<div class="rbody">
				<div class="rbodyin">
					<ul>
	                	<li style="font-size:16px; font-weight:bold;text-align:center;"></li>
	                    <li><hr /></li>
@@搜索列表@@
	                    <li><hr /></li>
	                    <li style="margin-top:5px;"> @@翻页@@</li>
                	</ul>
				</div>
			</div>
		</div>
		<!--right-->
	</div>',
    'children' => '0',
    'deep' => 0,
  ),
  3 => 
  array (
    'classid' => '3',
    'parentid' => '0',
    'level' => '1',
    'path' => '{0}',
    'template_name' => '基础模板',
    'index' => '<!--banner开始-->
@@Banner@@
<!--banner结束-->
<!--焦点图开始-->
@@焦点图@@
@@焦点图标题@@
@@焦点图链接@@
<!--焦点图结束-->',
    'common_top' => '<!--搜索开始-->
<form action="@@搜索@@" method="post">
<input id="keywords" type="text" name="keywords">
<input type="submit" value="搜索">
</form>
<!--搜索结束-->
<!--首页链接开始-->
@@首页链接@@
<!--首页链接结束-->
<!--网站名称开始-->
<title>
@@网站名称@@
</title>
<!--网站名称结束-->
<!--Logo开始-->
<img src="../attachments/
@@Logo@@
"/>
<!--Logo结束-->
@@Site_index@@
<!--Site_index结束-->
<!--一级栏目开始-->
@@一级栏目@@
<!--一级栏目结束-->
<!--二级栏目开始-->
@@二级栏目@@
<!--二级栏目结束-->',
    'common_foot' => '@@copyRight@@
@@链接@@
@@单位@@',
    'list' => '@@栏目链接@@
@@首页链接@@
@@栏目名1@@
@@栏目名2@@
@@列表页@@
@@友情链接@@
@@侧边导航@@
@@翻页@@',
    'content' => '@@栏目链接@@
@@首页链接@@
@@栏目名1@@
@@栏目名2@@
@@文章标题@@
@@文章内容@@
@@文章详细@@
@@友情链接@@
@@侧边导航@@',
    'msg_board' => '@@首页链接@@
@@友情链接@@
@@留言@@
@@站点ID@@
@@站点URL@@',
    'search' => '@@关键字@@
@@首页链接@@
@@数量@@
@@搜索列表@@
@@友情链接@@
@@翻页@@',
    'children' => '0',
    'deep' => 0,
  ),
);