<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['cate_models']['category']=array (
  'id' => '2',
  'name' => 'category',
  'description' => '栏目分类',
  'perpage' => '20',
  'level' => '2',
  'hasattach' => '1',
  'built_in' => '0',
  'auto_update' => '1',
  'thumb_preferences' => '{"enabled":[],"default":"original"}',
  'fields' => 
  array (
    3 => 
    array (
      'id' => '3',
      'name' => 'category_name',
      'description' => '分类名称',
      'model' => '2',
      'type' => 'input',
      'length' => '0',
      'values' => '',
      'width' => '0',
      'height' => '0',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '1',
      'listable' => '1',
      'order' => '0',
      'editable' => '1',
    ),
    13 => 
    array (
      'id' => '13',
      'name' => 'site',
      'description' => '站点',
      'model' => '2',
      'type' => 'select_from_model',
      'length' => '0',
      'values' => 'site|site_name',
      'width' => '0',
      'height' => '0',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '2',
      'editable' => '1',
    ),
  ),
  'listable' => 
  array (
    0 => '3',
    1 => '13',
  ),
  'searchable' => 
  array (
    0 => '3',
  ),
);