<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['cate_models']['link_cate']=array (
  'id' => '5',
  'name' => 'link_cate',
  'description' => '链接分类',
  'perpage' => '20',
  'level' => '1',
  'hasattach' => '1',
  'built_in' => '0',
  'auto_update' => '1',
  'thumb_preferences' => '{"enabled":[],"default":"original"}',
  'fields' => 
  array (
    14 => 
    array (
      'id' => '14',
      'name' => 'link_cate_name',
      'description' => '分类名',
      'model' => '5',
      'type' => 'input',
      'length' => '0',
      'values' => '',
      'width' => '0',
      'height' => '0',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '1',
      'listable' => '1',
      'order' => '0',
      'editable' => '1',
    ),
    23 => 
    array (
      'id' => '23',
      'name' => 'site',
      'description' => '站点',
      'model' => '5',
      'type' => 'select_from_model',
      'length' => '0',
      'values' => 'site|site_name',
      'width' => '0',
      'height' => '0',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '1',
      'editable' => '1',
    ),
  ),
  'listable' => 
  array (
    0 => '14',
    1 => '23',
  ),
  'searchable' => 
  array (
    0 => '14',
  ),
);