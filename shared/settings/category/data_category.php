<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['category']['category']=array (
  1 => 
  array (
    'classid' => '1',
    'parentid' => '0',
    'level' => '1',
    'path' => '{0}',
    'category_name' => '学院概况',
    'site' => '1',
    'children' => '2',
    'deep' => 0,
  ),
  2 => 
  array (
    'classid' => '2',
    'parentid' => '1',
    'level' => '2',
    'path' => '{0},{1}',
    'category_name' => '学院简介',
    'site' => '1',
    'children' => '0',
    'deep' => 1,
  ),
  3 => 
  array (
    'classid' => '3',
    'parentid' => '1',
    'level' => '2',
    'path' => '{0},{1}',
    'category_name' => '现任领导',
    'site' => '1',
    'children' => '0',
    'deep' => 1,
  ),
  4 => 
  array (
    'classid' => '4',
    'parentid' => '0',
    'level' => '1',
    'path' => '{0}',
    'category_name' => '新闻通知',
    'site' => '1',
    'children' => '2',
    'deep' => 0,
  ),
  5 => 
  array (
    'classid' => '5',
    'parentid' => '4',
    'level' => '2',
    'path' => '{0},{4}',
    'category_name' => '本系新闻',
    'site' => '1',
    'children' => '0',
    'deep' => 1,
  ),
  6 => 
  array (
    'classid' => '6',
    'parentid' => '4',
    'level' => '2',
    'path' => '{0},{4}',
    'category_name' => '通知公告',
    'site' => '1',
    'children' => '0',
    'deep' => 1,
  ),
  7 => 
  array (
    'classid' => '7',
    'parentid' => '0',
    'level' => '1',
    'path' => '',
    'category_name' => '工作动态',
    'site' => '1',
    'children' => '1',
    'deep' => 0,
  ),
  9 => 
  array (
    'classid' => '9',
    'parentid' => '7',
    'level' => '2',
    'path' => '',
    'category_name' => '学生工作',
    'site' => '1',
    'children' => '0',
    'deep' => 1,
  ),
);