<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting=array (
  'backend_theme' => 'default',
  'backend_lang' => 'zh-cn',
  'backend_root_access' => '1',
  'backend_access_point' => '',
  'backend_title' => '站群管理系统',
  'backend_logo' => 'images/logo.png',
  'plugin_dev_mode' => '0',
  'backend_http_auth_on' => '0',
  'backend_http_auth_user' => '',
  'backend_http_auth_password' => '',
);