<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Logout extends CI_Controller {
	/**
	 * 注销已登录管理员信息
	 */
	public function index() {
		$this->load->library ( 'session' );
		$session = array(
				'username' => $username
		);
		$this->session->unset_userdata ( $session );
		redirect ( base_url ( 'login' ) );
	}
}