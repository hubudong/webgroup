<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->db = $this->load->database ( "default", TRUE );//连接数据库
		header ( "content-type:text/html;charset=utf-8" );
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function toLogin(){
		$admin_user = $this->input->post('username');
		$admin_psd = $this->input->post('password');
		$this->admin->model = 'user_model';
		$this->admin->model();
		if (! empty ( $admin_user ) && ! empty ( $admin_psd )) {
			$re = $this->admin->isExistAdminUser ( $admin_user,$admin_psd);
			if (! $re) {
				$this->show_msg("账号不存在!!", site_url('login'));
				exit;
			} else {
				$re = $this->admin->adminUserPwd ( $admin_user, $admin_psd );
				if (! $re) {
					$this->show_msg("密码错误!!", site_url('login'));
					exit;
				} else {
					$this->_success ( $admin_user);
				}
			}
		} else {
			$this->_failure ();
		}
	}

	/**
	 * 登录成功
	 */
	private function _success($admin_user) {
		$this->admin->login( $admin_user);
		$this->show_msg("登录成功!!", base_url('main'));
	}

	/**
	 * 登录失败
	 */
	private function _failure() {
		$url = base_url ( 'login' );
		redirect ( $url );
	}

	/*
	 * 信息提示
	 */
	public function show_msg($msg,$url){
		echo "<script type='text/javascript'>alert('{$msg}');window.location.href='{$url}';</script>";
	}
	public function to_url($url){
		echo "<script type='text/javascript'>window.location.href='{$url}';</script>";
	}
}
