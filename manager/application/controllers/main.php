<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin->isLogin(base_url('login'));
        $this->db = $this->load->database("default", TRUE);//连接数据库
        header("content-type:text/html;charset=utf-8");

        $this->load->model("user_model");
        $this->load->model("site_model");
        $this->load->model("cate_model");
        $this->load->model("banner_model");
        $this->load->model("tag_model");
        $this->load->model("temp_model");
        $this->load->model("link_model");
        $this->load->model("linkcate_model");
        $this->load->model("config_model");
        $this->load->model("msg_model");
    }

    public function index()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $re = $this->site_model->getSiteInfo($user['classid']);
        $data['temp'] = $this->temp_model->getTemplate();
        if (!empty($re)) {
            $site = $re['classid'];
            $data['num'] = $this->msg_model->getNumMsg($site);
            $data['user'] = $user;
            $data['site'] = $re;
            $this->load->view('main', $data);
        } else {
            $this->load->view('siteInit', $data);
        }
    }

    public function siteInit()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $data['site_name'] = $this->input->post('site_name');
        $data['site_url'] = $this->input->post('url');
        $data['temp'] = $this->input->post('temp');
        $data['site_copy'] = $this->input->post('site_copy');
        $focus = $this->input->post('img_file');
        $focus = $this->img_path($focus);
        $data['site_logo'] = '/' . $focus;
        $data['user'] = $user['classid'];
        $re = $this->db->insert('web_u_c_site', $data);

        if ($re) {
            $this->show_msg("上传成功", base_url('main'));
        } else {
            $this->show_msg("上传失败", base_url('main'));
        }
    }

    //站点设置
    public function siteSetting()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $data['site'] = $this->site_model->getSiteInfo($user['classid']);
        $data['temp'] = $this->temp_model->getTemplate();
        $this->load->view('setting', $data);
    }

    public function siteSet()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $data['site_name'] = $this->input->post('site_name');
        $data['site_url'] = $this->input->post('url');
        $data['temp'] = $this->input->post('temp');
        $data['site_copy'] = $this->input->post('site_copy');
        $focus = $this->input->post('img_file');
        $focus = $this->img_path($focus);
        $data['site_logo'] = '/' . $focus;
        $re = $this->site_model->setSiteSetting($data, $user['classid']);
        if ($re) {
            $this->show_msg("上传成功", base_url('main/siteSetting'));
        } else {
            $this->show_msg("上传失败", base_url('main/siteSetting'));
        }
    }

    //栏目管理
    public function cate()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSiteInfo($user['classid']);
        $data['firstCategory'] = $this->cate_model->getFirstCategory($site['classid']);
        $this->load->view('cate', $data);
    }

    //查看子栏目
    public function subCate($parentid = 1)
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSiteInfo($user['classid']);
        $data['parentid'] = $this->input->get('id');
        $data['subCategory'] = $this->cate_model->getSubCategory($site['classid'], $data['parentid']);
        $this->load->view('subCate', $data);
    }

    //添加栏目
    public function addCate()
    {
        $data['parentid'] = $this->input->get('id');
        if ($data['parentid'] == 0) {
            $data['level'] = 1;
        } else {
            $data['level'] = 2;
        }
        $this->load->view("addCate", $data);
    }

    public function addCateName()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSiteInfo($user['classid']);
        $category_name = $this->input->post('category');
        $parentid = $this->input->post('parentid');
        $level = $this->input->post('level');

        $data['category_name'] = $category_name;
        $data['level'] = $level;
        $data['parentid'] = $parentid;
        $data['site'] = $site['classid'];

        $re = $this->db->insert('web_u_c_category', $data);

        if ($re) {
            if ($parentid == 0) {
                $this->show_msg('添加成功', base_url('main/cate'));
            } else {
                $this->show_msg('添加成功', base_url('main/subCate/?id=' . $parentid));
            }
        } else {
            $this->show_msg('添加失败', base_url('main/addCate/id=' . $parentid));
        }
    }

    //编辑栏目
    public function editCate()
    {
        $classid = $this->input->get('id');
        $category = $this->cate_model->getCategory($classid);
        $data['category'] = $category;
        $this->load->view('editCate', $data);
    }

    public function updateCate()
    {
        $classid = $this->input->post('classid');
        $parentid = $this->input->post('parentid');
        $data['category_name'] = $this->input->post('category');
        $where = array(
            "classid" => $classid
        );
        $re = $this->db->update('web_u_c_category', $data, $where);

        if ($re) {
            if ($parentid == 0) {
                $this->show_msg('修改成功', base_url('main/cate'));
            } else {
                $this->show_msg('修改成功', base_url('main/subCate/?id=' . $parentid));
            }
        } else {
            if ($parentid == 0) {
                $this->show_msg('修改失败', base_url('main/cate'));
            } else {
                $this->show_msg('修改失败', base_url('main/subCate/id=' . $parentid));
            }
        }
    }

    //删除栏目
    public function deleteCate($id)
    {
        $classid = $this->input->get('id');
        $parentid = $this->input->get('pid');
        $re = $this->cate_model->deleteCategory($category);
        if ($re) {
            if ($parentid == 0) {
                $this->show_msg('删除成功', base_url('main/cate'));
            } else {
                $this->show_msg('删除成功', base_url('main/subCate/?id=' . $parentid));
            }
        } else {
            if ($parentid == 0) {
                $this->show_msg('删除失败', base_url('main/cate'));
            } else {
                $this->show_msg('删除失败', base_url('main/subCate/id=' . $parentid));
            }
        }
    }

    //banner管理
    public function banner()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);
        $data['bannerList'] = $this->banner_model->getBanner($site);
        $this->load->view('bannerList', $data);
    }

    //添加Banner
    public function addBannerList()
    {
        $this->load->view('addBanner');
    }

    //添加Banner逻辑
    public function addBanner()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);
        $data['title'] = $this->input->post('title');
        $data['url'] = $this->input->post('url');
        $banner = $this->input->post('img_file');
        $banner = $this->img_path($banner);
        $data['banner'] = '/' . $banner;
        $data['create_time'] = time();
        $data['site'] = $site;

        $re = $this->db->insert("web_u_m_banner", $data);
        if ($re) {
            $this->show_msg("添加成功", base_url('main/banner'));
        } else {
            $this->show_msg("添加失败", base_url("main/addBannerList"));
        }
    }

    //编辑Banner
    public function editBanner()
    {
        $id = $this->input->get("id");
        $data = $this->banner_model->getOneBanner($id);
        $this->load->view('editBanner', $data);
    }

    //update Banner
    public function updateBanner()
    {
        $id = $this->input->post('id');
        $data['title'] = $this->input->post('title');
        $data['url'] = $this->input->post('url');
        $banner = $this->input->post('img_file');
        $banner = $this->img_path($banner);
        $data['banner'] = '/' . $banner;
        $where = "id = {$id}";
        $re = $this->db->update("web_u_m_banner", $data, $where);
        if ($re) {
            $this->show_msg("修改成功", base_url('main/banner'));
        } else {
            $this->show_msg("修改失败", base_url("main/editBanner/?id={$id}"));
        }
    }

    //删除Banner
    public function deleteBanner()
    {
        $id = $this->input->get("id");
        $sql = "delete from web_u_m_banner where id = {$id}";
        $re = $this->db->query($sql);
        if ($re) {
            $this->show_msg("删除成功", base_url('main/banner'));
        } else {
            $this->show_msg("删除失败", base_url('main/banner'));
        }
    }

    //link 管理
    public function link()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);
        $data['linkList'] = $this->link_model->getLinkList($site);
        $this->load->view('linkList', $data);
    }

    //删除link
    public function delLink()
    {
        $id = $this->input->get('id');
        $this->db->where('id', $id);
        $re = $this->db->delete('web_u_m_link');

        if ($re) {
            $this->show_msg('删除成功', base_url('main/link'));
        } else {
            $this->show_msg('删除失败', base_url('main/link'));
        }
    }

    //编辑页面
    public function editLink()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);
        $id = $this->input->get('id');
        $data['linkCon'] = $this->object_array($this->link_model->getLinkCon($id));
        $data['linkCate'] = $this->object_array($this->linkcate_model->getAllCate($site));
        $this->load->view('editLink', $data);
    }

    //修改提交link
    public function updateLink()
    {
        $id = $this->input->post('id');
        $data['link_name'] = $this->input->post('link_name');
        $data['link_url'] = $this->input->post('link_url');
        $data['link_cate'] = $this->input->post('link_cate_name');
        $data['create_time'] = time();
        $re = $this->db->update('web_u_m_link', $data, array('id' => $id));
        if ($re) {
            $this->show_msg('修改成功', base_url('main/link'));
        } else {
            $this->show_msg('修改失败', base_url('main/editLink/?id=' . $id));
        }
    }

    //添加 link
    public function addLink()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);
        $data['linkCate'] = $this->object_array($this->linkcate_model->getAllCate($site));
        $this->load->view('addLink', $data);
    }

    public function insertLink()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);
        $data['link_name'] = $this->input->post('link_name');
        $data['link_url'] = $this->input->post('link_url');
        $data['link_cate'] = $this->input->post('link_cate_name');
        $data['create_time'] = time();
        $data['site'] = $site;
        $re = $this->db->insert('web_u_m_link', $data);
        if ($re) {
            $this->show_msg('添加成功', base_url('main/link'));
        } else {
            $this->show_msg('添加失败', base_url('main/addLink'));
        }
    }

    //模板页
    public function tempSetting()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);
        $siteTemp = $this->site_model->getSiteTemp($user['classid']);
        $data['template'] = $this->temp_model->getTemplate();
        $data['selectTemplate'] = $siteTemp;
        $this->load->view('tempList', $data);
    }
    //模板页
    public function tempEditList()
    {
        $username = $this->session->userdata('username');
        $data['template'] = $this->temp_model->getTemplate();
        $this->load->view('tempEditList', $data);
    }
    //模板设置
    public function tempEditor($id = 0)
    {
        $username = $this->session->userdata('username');
        if ($id != 0) {
            $data['template'] = $this->temp_model->getTemplateItem($id);
        } else {
            $re=$this->temp_model->recentTemplateItem();
            $data['template']['classid'] = $re['classid']+1;
        }

        $this->load->view('tempEditor', $data);
    }

    public function tempUpdate($id)
    {

        parse_str(file_get_contents("php://input"),$output); // 避免XSS检查导致代码出错
        $data['classid']=$output['classid'];
        $data['common_foot']=$output['common_foot'];
        $data['common_top']=$output['common_top'];
        $data['content']=$output['content'];
        $data['index']=$output['index'];
        $data['list']=$output['list'];
        $data['msg_board']=$output['msg_board'];
        $data['search']=$output['search'];
        $data['template_name']=$output['template_name'];
        $data['level'] = 1;
        $data['parentid'] = 0;
        $data['path'] = '{0}';
//        print_r($data);
//        file_put_contents('a.log',json_encode($data));
        $re = $this->db->replace('web_u_c_templates', $data);
        if ($re) {
            $this->show_msg('修改成功', base_url('main/tempSetting'));
        } else {
            $this->show_msg('修改失败', base_url('main/tempSetting'));
        }
    }

    //选择模板
    public function checkTemp()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);
        $id = $this->input->get('id');
        $id = empty($id) ? 1 : $id;
        $data['temp'] = $id;
        $re = $this->db->update('web_u_c_site', $data, array('classid' => $site));
        if ($re) {
            $this->show_msg('修改成功', base_url('main/tempSetting'));
        } else {
            $this->show_msg('修改失败', base_url('main/tempSetting'));
        }
    }

    //标签管理
    public function tempTags()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);
        $data['tags'] = $this->tag_model->getTempTags($site);
        $this->load->view('tagsList', $data);
    }

    //编辑标签
    public function editTags()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);
        $id = $this->input->get('id');
        $data['tagCon'] = $this->tag_model->getTag($id);
        $data['category'] = $this->cate_model->getAllcategory($site);
        $this->load->view('editTag', $data);
    }

    //删除标签
    public function deleteTags()
    {
        $id = $this->input->get('id');
        $sql = "delete from web_u_m_temptags where id = {$id}";
        $re = $this->db->query($sql);
        if ($re) {
            $this->show_msg('删除成功', base_url('main/tempTags'));
        } else {
            $this->show_msg('删除失败', base_url('main/tempTags'));
        }
    }

    //添加标签
    public function addTag()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);
        $data['category'] = $this->cate_model->getAllcategory($site);
        $data['config_type'] = $this->object_array($this->config_model->getAllConfig());
        $this->load->view('addTag', $data);
    }

    //insert 标签表
    public function insertTag()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);

        $data['tags'] = $this->input->post('tags');
        $data['max_length'] = $this->input->post('max_length');
        $data['config_type'] = $this->input->post('config_type');
        $data['article_cate'] = $this->input->post('article_cate');
        $data['create_time'] = time();
        $data['site'] = $site;

        $re = $this->db->insert("web_u_m_temptags", $data);

        if ($re) {
            $this->show_msg('添加成功', base_url('main/tempTags'));
        } else {
            $this->show_msg('添加失败', base_url('main/addTag'));
        }
    }

    //修改密码
    public function editPassword()
    {
        $this->load->view('editPassword');
    }

    //提交修改密码
    public function changePW()
    {
        $username = $this->session->userdata('username');
        $password = $this->input->post('password');
        $password = $this->user_model->_encrypt($password);

        $sql = "update web_u_c_user set password = '{$password}' where username = '{$username}'";
        $re = $this->db->query($sql);

        if ($re) {
            $this->show_msg("修改成功", base_url('main/editPassword'));
        } else {
            $this->show_msg("修改失败", base_url('main/editPassword'));
        }
    }

    //文章列表
    public function articleList($page = 1)
    {

        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);

        $page = $this->input->get("page");
        $page = (empty($page)) ? 1 : $page;
        $param = $this->getArticlePageData($page);
        $data['pageData'] = $param;
        $pageSize = $param['page_size'];
        $pageSize = (empty($pageSize)) ? 10 : $pageSize;
        $num1 = ($page - 1) * $pageSize;
        $num2 = $page * $pageSize;

        $sql = "select * from web_u_m_article where user={$user['classid']} limit {$num1},{$num2}";
        $articleList = $this->db->query($sql)->result_array();

        foreach ($articleList as $key => $value) {
            $cateSql = "select category_name from web_u_c_category where classid = {$value['category']}";
            $cate = $this->db->query($cateSql)->row_array();
            $articleList[$key]['category'] = $cate['category_name'];
        }

        $data['articleList'] = $articleList;

        $this->load->view('articleList', $data);
    }

    //文章分页
    public function getArticlePageData($page = 1)
    {

        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);

        $sql = "select count(id) as pageMaxSize from web_u_m_article where user = {$user['classid']}";
        $data = $this->db->query($sql)->row_array();
        $count = $data['pageMaxSize'];

        $params = array(
            "url_template" => base_url('main/articleList/?page={page}'),
            "page_count" => $count,
            "page_now" => $page,
            "page_size" => 10
        );

        $params["total_pages"] = ceil($params['page_count'] / $params['page_size']);
        //paging文件在shared文件夹中的library，引入就可以
        $this->load->library('paging', $params);

        return $params;
    }

    //编辑文章
    public function editArticle($id = 1)
    {

        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $sql = "select * from web_u_c_site where user = {$user['classid']}";
        $re = $this->db->query($sql)->row_array();

        $id = $this->input->get('id');
        $sql = 'select * from web_u_m_article where id =' . $id;
        $data = $this->db->query($sql)->row_array();
        $data['category'] = $this->cate_model->getAllcategory($re['classid']);

// 		echo "<pre>";
// 		print_r($data);
// 		exit;

        $this->load->view('articleCon', $data);
    }

    //修改已提交
    public function changeArticle()
    {
        $id = $this->input->post('id');

        $data['title'] = $this->input->post('title');
        $data['author'] = $this->input->post('author');
        $data['source'] = $this->input->post('source');
        $data['category'] = $this->input->post('category');
        $focus = $this->input->post('img_file');
        $focus = $this->img_path($focus);
        $data['focus'] = '/' . $focus;
        $data['content'] = $this->input->post('content');

// 		$where = "id = {$id}";

        $re = $this->db->update("web_u_m_article", $data, array('id' => $id));

        if ($re) {
            $this->show_msg('修改成功', base_url('main/articleList/?page=1'));
        } else {
            $this->show_msg('修改失败', base_url('main/editArticle/?id=' . $id));
        }
    }

    //删除文章
    public function deleteArticle($id = 1)
    {
        $id = $this->input->get('id');
        $sql = "DELETE FROM web_u_m_article WHERE id = {$id} ";
        $re = $this->db->query($sql);
        if ($re) {
            $this->show_msg('删除成功', site_url('main/articleList/?page=1'));
        } else {
            $this->show_msg('删除失败', site_url('main/articleList/?page=1'));
        }
    }

    //查看文章
    public function lookArticle($id = 1)
    {
        $id = $this->input->get('id');
        $data = $this->db->get_where('web_u_m_article', array('id' => $id))->row_array();

        $cateSql = "select category_name from web_u_c_category where classid = {$data['category']}";
        $cate = $this->db->query($cateSql)->row_array();
        $data['category'] = $cate['category_name'];

        $this->load->view('lookArticle', $data);
    }

    //添加文章
    public function addArticle()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $sql = "select * from web_u_c_site where user = {$user['classid']}";
        $re = $this->db->query($sql)->row_array();
        $data['category'] = $this->cate_model->getAllcategory($re['classid']);
// 		echo "<pre>";
// 		print_r($data);
// 		exit;
        $this->load->view('addArticle', $data);
    }

    //插入文章数据
    public function insertArticle()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $data['title'] = $this->input->post('title');
        $data['author'] = $this->input->post('author');
        $data['source'] = $this->input->post('source');
        $data['category'] = $this->input->post('category');
        $focus = $this->input->post('img_file');
        $focus = $this->img_path($focus);
        $data['focus'] = '/' . $focus;
        $data['content'] = $this->input->post('content');
        $data['hits'] = 0;
        $data['add_time'] = date("Y-m-d H:i:s", time());
        $data['user'] = $user['classid'];

// 		echo "<pre>";
// 		print_r($data);
// 		exit;

        $re = $this->db->insert("web_u_m_article", $data);

        if ($re) {
            $this->show_msg('发布成功', base_url('main/articleList/?page=1'));
        } else {
            $this->show_msg('发布失败', base_url('main/addArticle'));
        }
    }

    //留言板--未回复列表
    public function noReplyList()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);

        $temp = 0;

        $page = $this->input->get("page");
        $page = (empty($page)) ? 1 : $page;

        $data['pageData'] = $this->msg_model->getMsgPageData($page, $temp);
        $data['noReplyList'] = $this->msg_model->getMsgData($site, $temp);
        $this->load->view('noReplyList', $data);
    }

    //跳转回复页面
    public function reply()
    {
        $id = $this->input->get('id');
        $data = $this->msg_model->selectMsg($id);
        $this->load->view('replyCon', $data);
    }

    //将回复插入数据库
    public function insertReply()
    {
        $id = $this->input->post('id');
        $data['reply_time'] = date("Y-m-d H:i:s", time());
        $data['reply'] = $this->input->post('reply');
        $data['is_reply'] = 1;
        $where = "id = {$id}";
        $re = $this->db->update("web_u_m_msg_board", $data, $where);
        if ($re) {
            $this->show_msg('提交成功', site_url('main/replyList/?page=1'));
        } else {
            $this->show_msg('提交失败', site_url('main/reply/?id=' . $id));
        }
    }

    //留言板--已回复列表
    public function replyList()
    {
        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);
        $temp = 1;
        $page = $this->input->get("page");
        $page = (empty($page)) ? 1 : $page;

        $data['pageData'] = $this->msg_model->getMsgPageData($page, $temp);
        $data['replyList'] = $this->msg_model->getMsgData($site, $temp);
        $this->load->view('replyList', $data);
    }

    //编辑已回复留言
    public function editReplyCon()
    {
        $id = $this->input->get('id');
        $data = $this->selectMsg($id);
        $this->load->view('editReplyCon', $data);
    }

    //提交编辑内容
    public function updateReply()
    {
        $id = $this->input->post('id');
        $data['reply'] = $this->input->post('reply');
        $where = "id = {$id}";
        $re = $this->db->update("web_u_m_msg_board", $data, $where);
        if ($re) {
            $this->show_msg('提交成功', site_url('main/replyList/?page=1'));
        } else {
            $this->show_msg('提交失败', site_url('main/reply/?id=' . $id));
        }
    }

    //删除消息
    public function deleteMsg()
    {
        $id = $this->input->get('id');
        $del = $this->input->get('del');
        $sql = "DELETE FROM web_u_m_msg_board WHERE id = {$id} ";
        $re = $this->db->query($sql);
        if ($re) {
            if ($del == 1) {
                $this->show_msg('删除成功', site_url('main/noReplyList/?page=1'));
            } elseif ($del == 2) {
                $this->show_msg('删除成功', site_url('main/replyList/?page=1'));
            }
        } else {
            if ($del == 1) {
                $this->show_msg('删除成功', site_url('main/noReplyList/?page=1'));
            } elseif ($del == 2) {
                $this->show_msg('删除成功', site_url('main/replyList/?page=1'));
            }
        }
    }


    /*
     *ajax 图片上传功能
     */
    public function upload_img($form_name, $file_size)
    {
        $params = array(
            'form_name' => $form_name,
            'upload_dir' => '../attachments/uploads',
            'file_size' => $file_size
        );
        $this->load->library("AjaxUpload", $params);
    }

    /*
     * 处理图片路径
     */
    public function img_path($img)
    {
        if (strpos($img, './') === false) {
            return $img;
            exit;
        }
        $img = trim($img, "./");
        $img_path_arr = explode("/", $img);
        $img_path_arr = array_splice($img_path_arr, 1);
        $img = implode("/", $img_path_arr);
        return $img;
    }

    public function img_judge($img)
    {
        $flag = substr($img, 0, 1);

        if ($flag == "/") {
            return $img;
        } else {
            $img = '/' . $img;
            return $img;
        }
    }

    /*
     * 信息提示
     */
    public function show_msg($msg, $url)
    {
        echo "<script type='text/javascript'>alert('{$msg}');window.location.href='{$url}';</script>";
    }

    //stdClass Object转array
    public function object_array($array)
    {
        if (is_object($array)) {
            $array = (array)$array;
        }
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                $array[$key] = $this->object_array($value);
            }
        }
        return $array;
    }
}