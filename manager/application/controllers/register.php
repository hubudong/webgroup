<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->db = $this->load->database ( "default", TRUE );//连接数据库
		$this->load->model('user_model');
		header ( "content-type:text/html;charset=utf-8" );
	}

	public function index(){
		$this->load->view('register');
	}

	public function do_reg(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$comfirm_pw = $this->input->post('comfirm_pw');
		
		if ($password == $comfirm_pw) {
			$password = $this->user_model->_encrypt($password);
			$this->judge_username($username);
			$data['username'] = $username;
			$data['password'] = $password;
			$data['add_time'] = date("Y-m-d",time());
			$re = $this->db->insert('web_u_c_user',$data);
			if($re){
				$this->show_msg("注册成功！！",base_url('login'));
			}else{
				$this->show_msg("注册失败！！",site_url('register'));
			}
		}else{
			$this->show_msg("两次输入密码不相同！！",site_url('register'));
		}
		
	}
	/*
	 * 判断用户名是否存在
	 */
	public function judge_username($username){
		$re = $this->user_model->getUser($username);
		if($re){
			$this->show_msg("该用户名已存在，请再取一个用户名！！",site_url('register'));
			exit;
		}
		return true;
	}

	/*
	 * 信息提示
	 */
	public function show_msg($msg,$url){
		echo "<script type='text/javascript'>alert('{$msg}');window.location.href='{$url}';</script>";
	}
}