<!--head start-->
<?php
	$this->load->view('topbar');
?>
<!--head end-->
<div class="am-cf admin-main">
	<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->

<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
		<div class="am-cf am-padding am-padding-bottom-0">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">主页</strong> /
				<small>Index</small>
			</div>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-12 am-u-sm-centered">
		</div>

		<div class="am-u-sm-12 am-u-md-8 am-u-md-pull-4">
			<h3>欢迎您：<span class="am-text-primary"><?=$user['username']?></span></h3>
			<p>以下是<span class="am-text-danger"><?=$site['site_name']?></span>后台的基本信息:</p>
			<p>你还有<span class="am-text-danger"><?=$num?></span>条留言未回复</p>
		</div>
	</div>
</div>
<?php
	$this->load->view('bottom');
?>
