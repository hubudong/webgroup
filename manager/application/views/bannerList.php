<!--head start-->
<?php
	$this->load->view('topbar');
?>
<!--head end-->
<div class="am-cf admin-main">
<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->

<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
		<div class="am-cf am-padding am-padding-bottom-0">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">Banner列表</strong> /
				<small>Banner List</small>
			</div>
		</div>
		<div class="am-cf am-padding am-padding-bottom-0">
			<a class="am-btn am-btn-primary" href="<?=base_url('main/addBannerList')?>" >添加</a>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-12 am-u-sm-centered">
				<form class="am-form">
		            <table class="am-table am-table-striped am-table-hover table-main" style="text-align: center;">
	                <thead>
		                <tr>
		                	<th class="table-id">序号</th>
		                	<th class="table-title">标题</th>
		                	<th class="table-type">地址</th>
		                	<th class="table-author am-hide-sm-only">路径</th>
		                	<th class="table-date am-hide-sm-only">上传日期</th>
		                	<th class="table-set">操作</th>
		                </tr>
	                </thead>
	              <tbody>
	              <?php
	              if (!empty($bannerList)){
	              	$i=1;
	              	foreach ($bannerList as $key=>$value){
	              ?>
	              <tr>
	                <td><?=$i?></td>
	                <td><a href="#"><?=$value['title']?></a></td>
	                <td class="am-hide-sm-only"><?=$value['url']?></td>
	                <td class="am-hide-sm-only"><?=$value['banner']?></td>
	                <td class="am-hide-sm-only"><?=date("Y-m-d",$value['create_time'])?></td>
	                <td>
	                  <div class="am-btn-toolbar">
	                    <div class="am-btn-group am-btn-group-xs am-pagination-centered fn">
	                      <a class="am-btn am-btn-default am-btn-xs am-text-secondary" style="display: block;background:#fff;color: #0e90d2;" href="<?=base_url('main/editBanner/?id='.$value['id'])?>"><span class="am-icon-pencil-square-o"></span>编辑</a>
	                      <a class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="if(confirm('数据删除后不可恢复，你确定删除吗?')==false)return false;" style="display: block;background:#fff;color:#dd514c;" href="<?=base_url('main/deleteBanner/?id='.$value['id'])?>"><span class="am-icon-times"></span>删除</a>
	                    </div>
	                  </div>
	                </td>
	              </tr>
	              <?php
	              		$i++;
	              	}
	              }
	              ?>
	              </tbody>
	            </table>
	          </form>
		</div>
		</div>
</div>
<?php
	$this->load->view('bottom');
?>