<!--head start-->
<?php
	$this->load->view('topbar');
?>
<!--head end-->
<div class="am-cf admin-main">
<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->

<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
<!-- 		<div class="am-cf am-padding am-padding-bottom-0"> -->
<!-- 			<div class="am-fl am-cf"> -->
<!-- 				<strong class="am-text-primary am-text-lg">文章</strong> / -->
<!-- 				<small>Article</small> -->
<!-- 			</div> -->
<!-- 		</div> -->
		<div class="am-cf am-padding am-padding-bottom-0">
			<a href="<?=base_url('main/articleList')?>">返回列表</a>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-12 am-u-sm-centered">
			
				<div class="am-g my-head">
				  <div class="am-u-sm-12 am-article">
				    <h1 class="am-article-title"><?=$title?></h1>
				    <p class="am-article-meta">作者：<?=$author?>&nbsp;&nbsp;来源：<?=$source?>&nbsp;&nbsp;栏目：<?=$category?>&nbsp;&nbsp;点击量：<?=$hits?></p>
				  </div>
				</div>
				<hr class="am-article-divider"/>
				<div class="am-g am-g-fixed">
				  <div class="am-u-md-12">
				    <div class="am-g">
				      <div class="am-u-sm-11 am-u-sm-centered">
				      <?=$content?>
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		</div>
<?php
	$this->load->view('bottom');
?>