<!--head start-->
<?php
	$this->load->view('topbar');
?>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.config.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.all.min.js') ?>"></script>
<!--head end-->
<div class="am-cf admin-main">
	<!-- sidebar start -->
	<?php
		$this->load->view('menu');
	?>
	<!-- sidebar end -->

	<!-- content start -->
	<div class="admin-content">
		<div class="admin-content-body">
			<div class="am-cf am-padding am-padding-bottom-0">
				<div class="am-fl am-cf">
					<strong class="am-text-primary am-text-lg">添加</strong> /
					<small>Add</small>
				</div>
			</div>
			<hr/>
			<div class="am-g">
				<div class="am-u-sm-12 am-u-sm-centered am-u-md-8">
					<form action="<?=base_url("main/insertLink")?>" method="post" class="am-form">
					  	<fieldset>
						    <div class="am-form-group">
							    <label for="doc-ipt-name-1">链接名称</label>
							    <input type="text" id="title" name="link_name" value="">
						    </div>
						    <div class="am-form-group">
							    <label for="doc-ipt-url-1">链接地址</label>
							    <input type="text" id="author" name="link_url" value="">
						    </div>
						    <div class="am-form-group">
							    <label for="doc-ipt-url-1">栏目</label>
							    <select name="link_cate_name">
					    	<?php
					    	if (!empty($linkCate)) {
					    		foreach ($linkCate as $key=>$value){
					    			if ($value['level']==1) {
					    	?>
					    	<option value="<?=$value['classid']?>"><?=$value['link_cate_name']?></option>
				    		<?php
					    			}else{
					    	?>
					    	<option value="<?=$value['classid']?>">&nbsp;&nbsp;<?=$value['link_cate_name']?></option>
					    	<?php
					    			}
					    		}
				    		}
				    		?>
					    	</select>
						    </div>
						    <div class="am-u-sm-12 am-u-sm-offset-4 am-u-md-12 am-u-md-offset-5" style="margin-top:30px;">
				    			<button type="submit" class="am-btn am-btn-primary">提交</button>
				    		</div>
				  		</fieldset>
					</form>
					<script type="text/javascript">
					$("form").on("submit",function(){
						var title = $("#title").val();
						var author = $("#author").val();
						if(title.length==0){
							alert("没有链接名称");
							return false;
						}
						if(author.length==0){
							alert("没有链接地址");
							return false;
						}
					});
					</script>
				</div>
			</div>
		</div>
		<?php
			$this->load->view('bottom');
		?>