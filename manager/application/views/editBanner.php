<!--head start-->
<?php
	$this->load->view('topbar');
?>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.config.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.all.min.js') ?>"></script>

<!--head end-->
<div class="am-cf admin-main">
	<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->

<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
		<div class="am-cf am-padding am-padding-bottom-0">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">编辑 Banner</strong> /
				<small>Edit Banner</small>
			</div>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-12 am-u-sm-centered am-u-md-8">
				<form action="<?=base_url('main/updateBanner')?>" method="post" class="am-form">
				  	<fieldset>
					    <div class="am-form-group">
						    <label for="doc-ipt-name-1">标题</label>
						    <input type="text" name="title" id="title" value="<?=$title?>">
					    </div>
					    <div class="am-form-group">
						    <label for="doc-ipt-url-1">链接</label>
						    <input type="text" name="url" id="url" value="<?=$url?>">
					    </div>
					     <div class="am-form-group">
						    <input type="hidden" name="id" id="id" value="<?=$id?>">
					    </div>
					    <div class="am-form-group">
							<label for="doc-ipt-file-1">Banner图片</label>
						    <p class="am-form-help">请选择要上传的图片...</p>
						    <div class="img_class">
				                <div class="img_class_input">
				                    <button name="banner" id="upload_button" type="button" class="img_btn">选择要上传的图片</button><img src="<?=base_url('templates')?>/assets/img/loading.gif" style="display:none;" id="loadimg" />
			                		<p class="img_div">
			                			<img id="img_path" src="<?php $str = base_url(); $Url = substr($str,0,strlen($str)-9); echo $Url; ?>/attachments<?=$banner?>"/>
			                			<input name="img_file" id="img_file" type="hidden" name="img_file" value="<?=$banner?>" />
			                		</p>
				                </div>
			        		</div>
					    </div>
			    		<div class="am-form-group" style="text-align:center;">
		    			<button type="submit" class="am-btn am-btn-primary">提交</button>
		    		</div>
		  		</fieldset>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$("form").on("submit",function(){
	var title = $("#title").val();
	var url = $("#url").val();
	var img_file = $("#img_file").val();
	if(title.length==0){
		alert("没有标题");
		return false;
	}
	if(url.length==0){
		alert("没有链接");
		return false;
	}
	if(img_file.length==0){
		alert("没有图片");
		return false;
	}
});
</script>
<?php
	$this->load->view('bottom');
?>