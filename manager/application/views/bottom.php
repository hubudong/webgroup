<footer class="admin-content-footer">
					<hr>
					<p class="am-padding-left">&copy; 2016 XXXXX.</p>
				</footer>
			</div>
			<!-- content end -->
		</div>

		<a href="#" class="am-icon-btn am-icon-th-list am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}"></a>
		<footer>
		  <hr>
		 <p class="am-padding-left">&copy; 2016 景德镇陶瓷大学移动互联协会.</p>
		</footer>

		<!--[if lt IE 9]>
		<script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
		<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
		<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
		<![endif]-->

		<script src="<?=base_url('templates')?>/assets/js/amazeui.min.js"></script>
		<script src="<?=base_url('templates')?>/assets/js/app.js"></script>

		<script type="text/javascript">
			var toolbarArr=[[
			             'bold', //加粗
			             'indent', //首行缩进
			             //'snapscreen', //截图
			             'italic', //斜体
			             'underline', //下划线
			             'strikethrough', //删除线
			             'subscript', //下标
			             'fontborder', //字符边框
			             'superscript', //上标
			             'formatmatch', //格式刷
			             'pasteplain', //纯文本粘贴模式
			             'justifyleft', //居左对齐
			             'justifyright', //居右对齐
			             'justifycenter', //居中对齐
			             'justifyjustify', //两端对齐
			             'forecolor', //字体颜色
			             'backcolor', //背景色
			             'fullscreen', //全屏
			             'insertimage',
			             'imagefloat',
			             'link',
			             'unlink'
			         ]];
			var ue = UE.getEditor('content',{
		        //这里可以选择自己需要的工具按钮名称,此处仅选择如下五个
		        toolbars:toolbarArr,
		        //关闭elementPath
		        elementPathEnabled:false,
		        //关闭字数统计
		        wordCount:false
// 		        initialFrameWidth : 550,
// 		        initialFrameHeight: 200
		    });
		</script>

		<script type="text/javascript" src="<?=base_url('templates')?>/assets/js/ajaxupload.3.5.js" ></script>
		<script type="text/javascript">
    	$(document).ready(function(){
    		ajaxUpload(
	    		'upload_button', //上传的按钮id名称
	    		1024,  //允许上传的文件大小（单位：kb）
	    		"<?=site_url('main/upload_img/imgfile/1024')?>", //提交服务器端地址
	    		'imgfile', //提交服务器文件表单名称
	    		"$(\"#img_path\").attr('src', obj.filename).css({'width':'100%','height':'100%'});$(\"#img_file\").val(obj.filepath);$(\"#is_upload\").val('1');", //上传成功后执行的 js callback
	    		'loadimg'  //loading 图片id
    		);
      });
    	/**
    	 * Ajax 无刷新上传图片（jpg|gif|png）
    	 * @param string id_name id名称
    	 * @param int filesize 文件大小 k 为单位
    	 * @param string url   提交服务器端地址
    	 * @param string filename 提交服务器文件表单名称
    	 * @param string callback 上传成功运行代码
    	 * @param string loadingid loading 图片id名称
    	 * @return json
    	 */
    	function ajaxUpload(id_name, filesize, url, filename, callback, loadingid) {
    	    var button = $('#'+id_name), interval;
    	    var fileType = "pic", fileNum = "one";
    	    new AjaxUpload(button,{
    	        action: url,
    	        name: filename,
    	        onSubmit : function(file, ext){
    	            if(fileType == "pic") {
    	                if (ext && /^(jpg|png|jpeg|gif)$/.test(ext)){
    	                    this.setData({
    	                        'info': '文件类型为图片'
    	                    });
    	                } else {
    	                    alert('提示：您上传的是非图片类型！');
    	                    return false;
    	                }
    	            }
    	            $("#"+loadingid).show();
    	            if(fileNum == 'one') this.disable();
    	        },
    	        onComplete: function(file, response){
    	            eval("var obj="+response);
    	            if (obj.ok) {
    	                eval(callback);
    	            } else {
    	                switch (response) {
    	                    case '1':
    	                        alert('提示：上传失败，图片不能大于'+filesize+'k！');
    	                        break;
    	                    case '3':
    	                        alert('提示：图片只有部分文件被上传，请重新上传！');
    	                        break;
    	                    case '4':
    	                        alert('提示：没有任何文件被上传！');
    	                        break;
    	                    case '5':
    	                        alert('提示：非图片类型，请上传jpg|png|gif图片！');
    	                        break;
    	                    default:
    	                        alert('提示：上传失败，错误未知，请您及时联系网站客服人员！');
    	                        break;
    	                }
    	            }
    	            $("#"+loadingid).hide();
    	            window.clearInterval(interval);
    	            this.enable();
    	        }
    	    });
    	}
    </script>
	</body>
</html>
