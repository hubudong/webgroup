<!--head start-->
<?php
	$this->load->view('topbar');
?>
<style>
form{
	padding:30px;
}
input[type=submit]{
	margin-top:20px;
}
</style>
<!--head end-->
<div class="am-cf admin-main">
<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->
<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
		<div class="am-cf am-padding am-padding-bottom-0">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">编辑栏目</strong> /
				<small>Edit Category</small>
			</div>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-12 am-u-sm-centered">
			  <form class="am-form"  action="<?=base_url('main/updateCate')?>" method="post">
				  	<input type="text" name="category" style="width: 200px;" value="<?=$category['category_name']?>" />
				  	<input type="hidden" name="classid" value="<?=$category['classid']?>" />
				  	<input type="hidden" name="parentid" value="<?=$category['parentid']?>" />
				  	<input class="am-btn am-btn-secondary" type="submit" value="修改"/>
	          </form>
			</div>
		</div>
		</div>

<?php
	$this->load->view('bottom');
?>