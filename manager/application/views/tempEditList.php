<!--head start-->
<?php
	$this->load->view('topbar');
?>
<!--head end-->
<div class="am-cf admin-main">
<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->

<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
		<div class="am-cf am-padding am-padding-bottom-0">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">模板列表</strong> /
				<small>Template List</small>
			</div>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-12 am-u-sm-centered">
				<form class="am-form">
		            <table class="am-table am-table-striped am-table-hover table-main" style="text-align: center;">
	                <thead>
		                <tr>
		                	<th class="table-id">序号</th>
		                	<th class="table-title">模板名</th>
		                	<th class="table-set">操作</th>
		                </tr>
	                </thead>
	              <tbody>
	              <?php
	              if (!empty($template)){
	              	foreach ($template as $key=>$value){
	              ?>
	              <tr>
	                <td><?=$value['classid']?></td>
	                <td><?=$value['template_name']?></td>
	                <td>
	                  <div class="am-btn-toolbar">
	                    <div class="am-btn-group am-btn-group-xs am-pagination-centered fn">
	                      <a class="am-btn am-btn-default am-btn-xs am-text-secondary" style="display: block;background:#fff;color: #0e90d2;" href="<?=base_url('main/tempEditor/'.$value['classid'])?>"><span class="am-icon-pencil-square-o"></span>编辑</a>
	                    </div>
	                  </div>
	                </td>
	              </tr>
	              <?php
	              	}
	              }
	              ?>
	              </tbody>
	            </table>
	          </form>
		</div>
		</div>
</div>
<?php
	$this->load->view('bottom');
?>