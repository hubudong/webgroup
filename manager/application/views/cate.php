<!--head start-->
<?php
	$this->load->view('topbar');
?>
<!--head end-->
<div class="am-cf admin-main">
<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->
<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
		<div class="am-cf am-padding am-padding-bottom-0">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">栏目列表</strong> /
				<small>Category List</small>
			</div>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-12 am-u-sm-centered">
				<a href="<?=base_url('main/addCate/?id=0')?>" class="am-btn am-btn-primary">添加栏目</a>
				<form class="am-form">
				 <table class="am-table am-table-striped am-table-hover table-main" style="text-align: center;">
	                <thead>
		                <tr>
		                	<th class="table-id">序号</th>
		                	<th class="table-title">栏目名</th>
		                	<th class="table-set">操作</th>
		                </tr>
	                </thead>
	              <tbody>
	              <?php
	              if (!empty($firstCategory)){
	              	$i=1;
	              	foreach ($firstCategory as $key=>$value){
	              ?>
	              <tr>
	                <td><?=$i?></td>
	                <td><a href="#"><?=$value['category_name']?></a></td>
	                <td>
	                  <div class="am-btn-toolbar">
	                    <div class="am-btn-group am-btn-group-xs am-pagination-centered fn">
	                      <a class="am-btn am-btn-default am-btn-xs am-text-secondary" style="display: block;background:#fff;color: #0e90d2;" href="<?=base_url('main/editCate/?id='.$value['classid'])?>"><span class="am-icon-pencil-square-o"></span>编辑</a>
	                      <a class="am-btn am-btn-default am-btn-xs am-hide-sm-only" style="display: block;background:#fff;color:#000;" href="<?=base_url('main/subCate/?id='.$value['classid'])?>"><span class="am-icon-copy"></span> 子栏目</a>
	                      <a class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="if(confirm('数据删除后不可恢复，你确定删除吗?')==false)return false;" style="display: block;background:#fff;color:#dd514c;" href="<?=base_url('main/deleteCate/?id='.$value['classid'].'&pid='.$value['parentid'])?>"><span class="am-icon-times"></span>删除</a>
	                    </div>
	                  </div>
	                </td>
	              </tr>
	              <?php
	              		$i++;
	              	}
	              }
	              ?>
	              </tbody>
	            </table>
	          </form>
			</div>
		</div>
		</div>

<?php
	$this->load->view('bottom');
?>