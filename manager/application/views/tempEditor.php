<?php
$this->load->view('topbar');
?>

    <style>
        html, body {
            height: 100%;
            overflow: auto;
        }

        .CodeMirror {
            border: 1px solid #ddd;
            font-size: 1.4rem;
            font-family: "Consolas", "Monaco", "Bitstream Vera Sans Mono", "Courier New", Courier, monospace !important;
        }
    </style>
    <script src="http://cdn.bootcss.com/codemirror/5.26.0/codemirror.js"></script>
    <link href="http://cdn.bootcss.com/codemirror/5.26.0/codemirror.css" rel="stylesheet">
    <script src="http://cdn.bootcss.com/codemirror/5.26.0/mode/javascript/javascript.js"></script>
    <script src="http://cdn.bootcss.com/codemirror/5.26.0/mode/css/css.js"></script>
    <script src="http://cdn.bootcss.com/codemirror/5.26.0/mode/xml/xml.js"></script>
    <script src="http://cdn.bootcss.com/codemirror/5.26.0/mode/htmlmixed/htmlmixed.js"></script>
    <div class="am-container">
        <form class="am-form">
            <div class="am-form-group">
                <input type="text" id="classid" name="classid" value="<?= $template['classid'] ?>" readonly/>
            </div>
            <div class="am-form-group">
                <input type="text" id="template_name" name="template_name"
                       value="<?= !empty($template['template_name']) ? $template['template_name'] : '' ?>"/>
            </div>
            <h2>首页</h2>
            <textarea id="index" cols="120" rows="30" name="index"> </textarea>
            <h2>公共头部</h2>
            <textarea id="common_top" cols="120" rows="30" name="common_top"> </textarea>
            <h2>公共脚部</h2>
            <textarea id="common_foot" cols="120" rows="30" name="common_foot"> </textarea>
            <h2>列表页</h2>
            <textarea id="list" cols="120" rows="30" name="list"> </textarea>
            <h2>内容页</h2>
            <textarea id="content" cols="120" rows="30" name="content"> </textarea>
            <h2>留言</h2>
            <textarea id="msg_board" cols="120" rows="30" name="msg_board"> </textarea>
            <h2>搜索页</h2>
            <textarea id="search" cols="120" rows="30" name="search"> </textarea>
            <button type="button" class="am-btn am-btn-primary am-btn-block" onclick="update()">更新模板</button>
        </form>
    </div>

    <script>
        var data =<?=json_encode($template)?>;
        var config = {
            lineNumbers: true,
            mode: "text/html",
            matchBrackets: true,
            lineWrapping: true
        };

        var index = CodeMirror.fromTextArea(document.getElementById("index"), config);
        var common_top = CodeMirror.fromTextArea(document.getElementById("common_top"), config);
        var common_foot = CodeMirror.fromTextArea(document.getElementById("common_foot"), config);
        var list = CodeMirror.fromTextArea(document.getElementById("list"), config);
        var content = CodeMirror.fromTextArea(document.getElementById("content"), config);
        var msg_board = CodeMirror.fromTextArea(document.getElementById("msg_board"), config);
        var search = CodeMirror.fromTextArea(document.getElementById("search"), config);

        index.setValue(data.index);
        common_top.setValue(data.common_top);
        common_foot.setValue(data.common_foot);
        list.setValue(data.list);
        content.setValue(data.content);
        msg_board.setValue(data.msg_board);
        search.setValue(data.search);

        function update() {
            var data = {};
            data.classid = classid.value;
            data.template_name = template_name.value;
            data.index = index.getValue();
            data.common_top = common_top.getValue();
            data.common_foot = common_foot.getValue();
            data.list = list.getValue();
            data.content = content.getValue();
            data.msg_board = msg_board.getValue();
            data.search = search.getValue();
            console.log(data)
            $.post("<?=base_url('main/tempUpdate/')?>/" + data.classid, data, function (data, status) {
                alert("更新模板 " + status);
            });
        }
    </script>
<?php
$this->load->view('bottom');
?>