<div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
				<div class="am-offcanvas-bar admin-offcanvas-bar">
					<ul class="am-list admin-sidebar-list">
						<li><a href="<?=base_url('main')?>"><span class="am-icon-home"></span> 首页</a></li>
						<li class="admin-parent">
							<a class="am-cf" data-am-collapse="{target: '#collapse-nav'}"><span class="am-icon-key"></span> 账号管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
							<ul class="am-list am-collapse admin-sidebar-sub" id="collapse-nav">
								<li><a href="<?=base_url('main/editPassword')?>" class="am-cf"><span class="am-icon-check"></span> 密码修改</a></li>
							</ul>
						</li>
						<li class="admin-parent">
							<a class="am-cf" data-am-collapse="{target: '#collapse-site'}"><span class="am-icon-file"></span> 站点管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
							<ul class="am-list am-collapse admin-sidebar-sub" id="collapse-site">
								<li><a href="<?=base_url('main/siteSetting')?>" class="am-cf"><span class="am-icon-edit"></span> 站点设置</a></li>
								<li><a href="<?=base_url('main/cate')?>"><span class="am-icon-puzzle-piece"></span> 栏目管理</a></li>
								<li><a href="<?=base_url('main/banner')?>"><span class="am-icon-puzzle-piece"></span> Banner管理</a></li>
								<li><a href="<?=base_url('main/link')?>"><span class="am-icon-puzzle-piece"></span> 链接管理</a></li>
							</ul>
						</li>
						<li class="admin-parent">
							<a class="am-cf" data-am-collapse="{target: '#collapse-temp'}"><span class="am-icon-file"></span> 模板管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
							<ul class="am-list am-collapse admin-sidebar-sub" id="collapse-temp">
								<li><a href="<?=base_url('main/tempSetting')?>" class="am-cf"><span class="am-icon-edit"></span> 模板设置</a></li>
								<li><a href="<?=base_url('main/tempTags')?>"><span class="am-icon-puzzle-piece"></span> 标签管理</a></li>
							</ul>
						</li>
						<li class="admin-parent">
							<a class="am-cf" data-am-collapse="{target: '#collapse-content'}"><span class="am-icon-file-text-o"></span> 文章管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
							<ul class="am-list am-collapse admin-sidebar-sub" id="collapse-content">
								<li><a href="<?=base_url('main/articleList')?>" class="am-cf"><span class="am-icon-list"></span> 文章列表</a></li>
								<li><a href="<?=base_url('main/addArticle')?>"><span class="am-icon-clipboard"></span> 文章发布</a></li>
							</ul>
						</li>
						<li class="admin-parent">
							<a class="am-cf" data-am-collapse="{target: '#collapse-msg'}"><span class="am-icon-newspaper-o"></span> 留言管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
							<ul class="am-list am-collapse admin-sidebar-sub" id="collapse-msg">
								<li><a href="<?=base_url('main/noReplyList')?>" class="am-cf"><span class="am-icon-commenting-o"></span> 未回复列表</a></li>
								<li><a href="<?=base_url('main/replyList')?>" class="am-cf"><span class="am-icon-commenting"></span> 已回复列表</a></li>
							</ul>
						</li>
						<li><a href="<?=base_url('logout')?>"><span class="am-icon-sign-out"></span> 注销</a></li>
					</ul>

					<div class="am-panel am-panel-default admin-sidebar-panel">
						<div class="am-panel-bd">
							<p><span class="am-icon-bookmark"></span> 公告</p>
							<p id="dTime" class="am-text-primary am-text-xs">
								<script>
// 								    var dT = document.getElementsById('dTime');
									var l = ["日","一","二","三","四","五","六"];
									var d = new Date().getDay();
									var date = new Date();
									var str = "今天是"+date.getFullYear()+"年"+(date.getMonth()+1)+"月"+date.getDate()+"日 "+"星期" + l[d];
									document.write(str);
								</script>
							</p>
						</div>
					</div>

<!-- 					<div class="am-panel am-panel-default admin-sidebar-panel"> -->
<!-- 						<div class="am-panel-bd"> -->
<!-- 							<p><span class="am-icon-tag"></span> wiki</p> -->
<!-- 							<p>You still have 6 messages that did not reply!</p> -->
<!-- 						</div> -->
<!-- 					</div> -->
				</div>
			</div>
