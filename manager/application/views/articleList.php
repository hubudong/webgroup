<!--head start-->
<?php
	$this->load->view('topbar');
?>
<!--head end-->
<div class="am-cf admin-main">
<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->

<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
		<div class="am-cf am-padding am-padding-bottom-0">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">文章列表</strong> /
				<small>Article List</small>
			</div>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-12 am-u-sm-centered">
				<form class="am-form">
		            <table class="am-table am-table-striped am-table-hover table-main" style="text-align: center;">
	                <thead>
		                <tr>
		                	<th class="table-id">序号</th>
		                	<th class="table-title">标题</th>
		                	<th class="table-type">所属栏目</th>
		                	<th class="table-author am-hide-sm-only">作者</th>
		                	<th class="table-author am-hide-sm-only">来源</th>
		                	<th class="table-date am-hide-sm-only">发布日期</th>
		                	<th class="table-hit">点击量</th>
		                	<th class="table-set">操作</th>
		                </tr>
	                </thead>
	              <tbody>
	              <?php 
	              if (!empty($articleList)){
	              	$i=1;
	              	foreach ($articleList as $key=>$value){
	              ?>
	              <tr>
	                <td><?=$i?></td>
	                <td><a href="<?=base_url('main/lookArticle/?id='.$value['id'])?>"><?=$value['title']?></a></td>
	                <td><?=$value['category']?></td>
	                <td class="am-hide-sm-only"><?=$value['author']?></td>
	                <td class="am-hide-sm-only"><?=$value['source']?></td>
	                <td class="am-hide-sm-only"><?=date("Y-m-d",strtotime($value['add_time']))?></td>
	                <td><?=$value['hits']?></td>
	                <td>
	                  <div class="am-btn-toolbar">
	                    <div class="am-btn-group am-btn-group-xs am-pagination-centered fn">
	                      <a class="am-btn am-btn-default am-btn-xs am-text-secondary" style="display: block;background:#fff;color: #0e90d2;" href="<?=base_url('main/editArticle/?id='.$value['id'])?>"><span class="am-icon-pencil-square-o"></span>编辑</a>
	                      <a class="am-btn am-btn-default am-btn-xs am-hide-sm-only" style="display: block;background:#fff;color:#000;" href="<?=base_url('main/lookArticle/?id='.$value['id'])?>"><span class="am-icon-copy"></span> 查看</a>
	                      <a class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="if(confirm('数据删除后不可恢复，你确定删除吗?')==false)return false;" style="display: block;background:#fff;color:#dd514c;" href="<?=base_url('main/deleteArticle/?id='.$value['id'])?>"><span class="am-icon-times"></span>删除</a>
	                    </div>
	                  </div>
	                </td>
	              </tr>
	              <?php 
	              		$i++;
	              	}
	              }
	              ?>
	              </tbody>
	            </table>
	            <div class="am-cf">
					共 <?=$this->paging->page_count?> 条记录
					<div class="am-fr">
						<?php $this->paging->create();?>
					</div>
				</div>
	          </form>
		</div>
</div>
<?php
	$this->load->view('bottom');
?>