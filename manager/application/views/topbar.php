<!doctype html>
<html class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>站群管理系统</title>
		<meta name="description" content="站群管理系统">
		<meta name="keywords" content="站群">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<meta name="apple-mobile-web-app-title" content="站群管理系统" />
		<link rel="stylesheet" href="<?=base_url('templates')?>/assets/css/amazeui.min.css"/>
		<link rel="stylesheet" href="<?=base_url('templates')?>/assets/css/admin.css">
		<style type="text/css">
			.am-table thead tr th{
				text-align:center;
			}
			.fn{
				float:none!important;
			}
		    @media only screen and (min-width: 641px) {
		      .am-offcanvas {
		        display: block;
		        position: static;
		        background: none;
		      }

		      .am-offcanvas-bar {
		        position: static;
		        width: auto;
		        background: none;
		        -webkit-transform: translate3d(0, 0, 0);
		        -ms-transform: translate3d(0, 0, 0);
		        transform: translate3d(0, 0, 0);
		      }
		      .am-offcanvas-bar:after {
		        content: none;
		      }

		    }

		    @media only screen and (max-width: 640px) {
		      .am-offcanvas-bar .am-nav>li>a {
		        color:#ccc;
		        border-radius: 0;
		        border-top: 1px solid rgba(0,0,0,.3);
		        box-shadow: inset 0 1px 0 rgba(255,255,255,.05)
		      }

		      .am-offcanvas-bar .am-nav>li>a:hover {
		        background: #404040;
		        color: #fff
		      }

		      .am-offcanvas-bar .am-nav>li.am-nav-header {
		        color: #777;
		        background: #404040;
		        box-shadow: inset 0 1px 0 rgba(255,255,255,.05);
		        text-shadow: 0 1px 0 rgba(0,0,0,.5);
		        border-top: 1px solid rgba(0,0,0,.3);
		        font-weight: 400;
		        font-size: 75%
		      }

		      .am-offcanvas-bar .am-nav>li.am-active>a {
		        background: #1a1a1a;
		        color: #fff;
		        box-shadow: inset 0 1px 3px rgba(0,0,0,.3)
		      }

		      .am-offcanvas-bar .am-nav>li+li {
		        margin-top: 0;
		      }

		      #content{
		      	width: 90%!important;
		      }
		    }

		    .my-head {
		      margin-top: 40px;
		      text-align: center;
		    }

		    .my-button {
		      position: fixed;
		      top: 0;
		      right: 0;
		      border-radius: 0;
		    }
		</style>

		<!--[if (gte IE 9)|!(IE)]><!-->
		<script src="<?=base_url('templates')?>/assets/js/jquery.min.js"></script>
		<!--<![endif]-->

	</head>
	<body>
		<!--[if lte IE 9]>
		<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>以获得更好的体验！</p>
		<![endif]-->
	<div class="am-topbar am-topbar-inverse admin-header">
			<div class="am-topbar-brand">
				<strong>站群管理系统</strong>
			</div>
<!-- 			<button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button> -->
			<div class="am-collapse am-topbar-collapse" id="topbar-collapse">
				<ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list">
					<li class="am-hide-sm-only"><a href="javascript:;" id="admin-fullscreen"><span class="am-icon-arrows-alt"></span> <span class="admin-fullText">开启全屏</span></a></li>
				</ul>
			</div>
		</div>