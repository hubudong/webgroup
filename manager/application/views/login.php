<!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="UTF-8">
  <title>站群管理系统</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="alternate icon" type="image/png" href="<?=base_url('templates')?>/assets/img/logo.png">
  <link rel="stylesheet" href="<?=base_url('templates')?>/assets/css/amazeui.min.css"/>
  <style>
  		.head{
  			heigth:300px;
  		}
  		.login{
  			margin-top:30px;
  		}
		@media only screen and (min-width: 320px) and (max-width: 640px) {
			.logo{
				width:100%;
				margin-top:5%;
			}
			.logo img{
				width:100%;
			}
			.head{
  				heigth:auto;
  			}
			.login{
  				margin-top:0;
  			}
		}
  </style>
</head>
<body>
	<div class="head">
		<div class="am-g">
			<div class="logo" style="text-align:center;">
				<img alt="" src="<?=base_url('templates')?>/assets/img/logo.png">
			</div>
		</div>
	  <hr />
	</div>
	<div class="am-g login">
	  	<div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
	    	<form action="<?=base_url('login/toLogin')?>" method="post" class="am-form" id="form-with">
				 <fieldset>
				    <div class="am-form-group">
				      <label for="doc-vld-name-2-0">用户名：</label>
				      <input name="username" type="text" id="doc-vld-name-2-0" minlength="3" placeholder="输入用户名" required/>
				    </div>
				    <div class="am-form-group">
				      <label for="doc-vld-pwd-1-0">密码：</label>
				      <input  name="password" type="password" id="doc-vld-pwd-1-0" placeholder="请输入密码" minlength="6"  required/>
				    </div>
				    <button class="am-btn am-btn-secondary" type="submit">登录</button>
				    <button class="am-btn am-btn-secondary" type="button" onclick="window.location.href='<?=base_url('register')?>'">注册</button>
				    <button class="am-btn am-btn-danger am-btn-sm am-fr" type="button" onclick="window.location.href='<?=base_url('../')?>'" style="margin-right:10%;">返回主页</button>
				</fieldset>
			</form>
	    	<hr>
	    	<p class="am-padding-left">&copy;<a target="_blank" href="http://mia.pasp.cn/">景德镇陶瓷大学移动互联协会</a></p>
	  	</div>
	</div>
	<script type="text/javascript" src="<?=base_url('templates')?>/assets/js/jquery.min.js" ></script>
  <script type="text/javascript" src="<?=base_url('templates')?>/assets/js/amazeui.min.js" ></script>
</body>
</html>
