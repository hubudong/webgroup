<!--head start-->
<?php
	$this->load->view('topbar');
?>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.config.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.all.min.js') ?>"></script>
<!--head end-->
<div class="am-cf admin-main">
<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->

<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
		<div class="am-cf am-padding am-padding-bottom-0">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">添加标签</strong> /
				<small>Add Tag</small>
			</div>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-12 am-u-sm-centered am-u-md-8">
				<form action="<?=base_url("main/insertTag")?>" method="post" class="am-form">
		  	<fieldset>
			    <div class="am-form-group">
				    <label for="doc-ipt-name-1">标签名</label>
				    <input type="text" id="title" name="tags" >
			    </div>
			    <div class="am-form-group">
				    <label for="doc-ipt-url-1">最大长度</label>
				    <input type="text" id="author" name="max_length" >
			    </div>
			    <div class="am-form-group">
				    <label for="doc-ipt-url-1">config_type</label>
			    	<select name="config_type">
			    	<?php
			    	if (!empty($config_type)) {
			    		foreach ($config_type as $key=>$value){
			    	?>
			    	<option value="<?=$value['classid']?>"><?=$value['config_cate_name']?></option>
			    	<?php
			    		}
		    		}
		    		?>
			    	</select>
			    </div>
			    <div class="am-form-group">
				    <label for="doc-ipt-url-1">栏目</label>
			    	<select name="article_cate">
			    	<option value="0">全部栏目</option>
			    	<?php
			    	if (!empty($category)) {
			    		foreach ($category as $key=>$value){
			    			if ($value['level']==1) {
			    	?>
			    	<option value="<?=$value['classid']?>"><?=$value['category_name']?></option>
		    		<?php
			    			}else{
			    	?>
			    	<option value="<?=$value['classid']?>">&nbsp;&nbsp;<?=$value['category_name']?></option>
			    	<?php
			    			}
			    		}
		    		}
		    		?>
			    	</select>
			    </div>
			    <input type="hidden" id="doc-ipt-name-1" name="id" >
			    <div class="am-u-sm-12 am-u-sm-offset-4 am-u-md-12 am-u-md-offset-5">
	    			<button type="submit" class="am-btn am-btn-primary">提交</button>
	    		</div>
	  		</fieldset>
		</form>
		<script type="text/javascript">
			$("form").on("submit",function(){
				var title = $("#title").val();
				var author = $("#author").val();
				var source = $("#source").val();
				if(title.length==0){
					alert("没有标签名");
					return false;
				}
				if(author.length==0){
					alert("没有最大长度");
					return false;
				}
				if(source.length==0){
					alert("没有config_tpye");
					return false;
				}
			});
			</script>
		</div>
	</div>
</div>
<?php
	$this->load->view('bottom');
?>