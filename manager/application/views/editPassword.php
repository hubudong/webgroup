<!--head start-->
<?php
	$this->load->view('topbar');
?>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.config.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.all.min.js') ?>"></script>

<!--head end-->
<div class="am-cf admin-main">
	<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->

<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
		<div class="am-cf am-padding am-padding-bottom-0">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">修改密码</strong> /
				<small>Edit Password</small>
			</div>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-8 am-u-md-centered am-u-md-8 am-u-md-offset-2">
				<form action="<?=base_url('main/changePW')?>" method="post" class="am-form">
				  	<fieldset>
					    <div class="am-form-group">
						    <label for="doc-ipt-name-1">请输入密码</label>
						    <input style="width: 250px;" type="password" name="password" id="doc-ipt-name-1" placeholder="不少于6个字符" minlength="6"  required>
					    </div>
			    		<div class="am-form-group">
			    			<button type="submit" class="am-btn am-btn-primary">提交</button>
			    		</div>
		  		</fieldset>
			</form>
		</div>
	</div>
</div>

<?php
	$this->load->view('bottom');
?>