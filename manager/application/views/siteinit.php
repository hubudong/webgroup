<!--head start-->
<?php
	$this->load->view('topbar');
?>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.config.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.all.min.js') ?>"></script>

<!--head end-->
<div class="am-cf admin-main">
	<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->

<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
		<div class="am-cf am-padding am-padding-bottom-0">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">站点设置</strong> /
				<small>Site Setting</small>
			</div>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-12 am-u-sm-centered am-u-md-8">
				<form action="<?=base_url('main/siteInit')?>" method="post" class="am-form">
				  	<fieldset>
					    <div class="am-form-group">
						    <label for="doc-ipt-name-1">站点名称</label>
						    <input type="text" class="" name="site_name" id="doc-ipt-name-1" placeholder="填写站点名，例如：信息工程学院" value="">
					    </div>
					    <div class="am-form-group">
						    <label for="doc-ipt-url-1">地址</label>
						    <input type="text" class="" name="url" id="doc-ipt-url-1" placeholder="填写站点地址，例如：xxgcxy" value="">
					    </div>
					    <div class="am-form-group">
							<label for="doc-ipt-file-1">站点Logo</label>
						    <p class="am-form-help">请选择要上传的图片...</p>
						    <div class="img_class">
				                <div class="img_class_input">
				                    <button name="work_pic" id="upload_button" type="button" class="img_btn">选择要上传的图片</button><img src="<?=base_url('templates')?>/assets/img/loading.gif" style="display:none;" id="loadimg" />
				                		<p class="img_div">
				                			<img id="img_path"/>
	                						<input name="img_file" id="img_file" type="hidden" name="img_file" value="" />
				                		</p>
				                </div>
			        		</div>
					    </div>
					    <div class="am-form-group">
					    	<label>选择模板</label>
					    	<select name="temp">
								<?php
									foreach ($temp as $key=>$value){
								?>
								<option value="<?=$value['classid']?>"><?=$value['template_name']?></option>
								<?php
									}
								?>
					    	</select>
					    </div>
					    <div class="am-form-group">
						    <label for="doc-ipt-copy-1">站点底部</label>
						    <textarea name="site_copy" id="content">
						    </textarea>
					    </div>
			    		<div class="am-form-group" style="text-align:center;">
		    			<button type="submit" class="am-btn am-btn-primary">提交</button>
		    		</div>
		  		</fieldset>
			</form>
		</div>
	</div>
</div>

<?php
	$this->load->view('bottom');
?>