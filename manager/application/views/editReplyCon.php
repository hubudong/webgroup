<!--head start-->
<?php
	$this->load->view('topbar');
?>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.config.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.all.min.js') ?>"></script>
<!--head end-->
<div class="am-cf admin-main">
	<!-- sidebar start -->
	<?php
		$this->load->view('menu');
	?>
	<!-- sidebar end -->

	<!-- content start -->
	<div class="admin-content">
		<div class="admin-content-body">
			<div class="am-cf am-padding am-padding-bottom-0">
				<div class="am-fl am-cf">
					<strong class="am-text-primary am-text-lg">回复</strong> /
					<small>Reply</small>
				</div>
			</div>
			<hr/>
			<div class="am-g">
				<div class="am-u-sm-12 am-u-sm-centered am-u-md-8">
					<form action="<?=base_url("main/updateReply")?>" method="post" class="am-form">
					  	<fieldset>
						    <div class="am-form-group">
							    <label for="doc-ipt-name-1">文章标题</label>
							    <input type="text" id="doc-ipt-name-1" name="title" value="<?=$title?>" readonly>
						    </div>
						    <div class="am-form-group">
							    <label for="doc-ipt-url-1">内容</label>
							   <textarea rows="5" cols="" name="content" readonly><?=$content?></textarea>
						    </div>
						    <div class="am-form-group">
							    <label for="doc-ipt-url-1">回复</label>
							    <textarea rows="5" cols="" name="reply"><?=$reply?></textarea>
						    </div>
						    <input type="hidden" id="doc-ipt-name-1" name="id" value="<?=$id?>">
						    <div class="am-u-sm-12 am-u-sm-offset-4 am-u-md-12 am-u-md-offset-5">
				    			<button type="submit" class="am-btn am-btn-primary">提交</button>
				    		</div>
				  		</fieldset>
					</form>
				</div>
			</div>
		</div>
		<?php
			$this->load->view('bottom');
		?>