<!--head start-->
<?php
	$this->load->view('topbar');
?>
<style>
form{
	padding:30px;
}
input[type=submit]{
	margin-top:20px;
}
</style>
<!--head end-->
<div class="am-cf admin-main">
<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->
<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
		<div class="am-cf am-padding am-padding-bottom-0">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">添加栏目</strong> /
				<small>Add Category</small>
			</div>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-12 am-u-sm-centered">
			  <form class="am-form"  action="<?=base_url('main/addCateName')?>" method="post">
				  	<input type="text" name="category"  style="width: 200px;" />
				  	<input type="hidden" name="parentid" value="<?=$parentid?>" />
				  	<input type="hidden" name="level" value="<?=$level?>" />
				  	<input class="am-btn am-btn-secondary" type="submit" value="添加"/>
	          </form>
			</div>
		</div>
		</div>

<?php
	$this->load->view('bottom');
?>