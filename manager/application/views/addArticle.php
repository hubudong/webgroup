<!--head start-->
<?php
	$this->load->view('topbar');
?>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.config.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('org/ueditor/ueditor.all.min.js') ?>"></script>
<!--head end-->
<div class="am-cf admin-main">
<!-- sidebar start -->
<?php
	$this->load->view('menu');
?>
<!-- sidebar end -->

<!-- content start -->
<div class="admin-content">
	<div class="admin-content-body">
		<div class="am-cf am-padding am-padding-bottom-0">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">添加文章</strong> /
				<small>Add Article</small>
			</div>
		</div>
		<hr/>
		<div class="am-g">
			<div class="am-u-sm-12 am-u-sm-centered am-u-md-8">
				<form action="<?=base_url("main/insertArticle")?>" method="post" class="am-form">
		  	<fieldset>
			    <div class="am-form-group">
				    <label for="doc-ipt-name-1">文章标题</label>
				    <input type="text" id="title" name="title" >
			    </div>
			    <div class="am-form-group">
				    <label for="doc-ipt-url-1">作者</label>
				    <input type="text" id="author" name="author" >
			    </div>
			    <div class="am-form-group">
				    <label for="doc-ipt-url-1">来源</label>
				    <input type="text" id="source" name="source" >
			    </div>
			    <div class="am-form-group">
				    <label for="doc-ipt-url-1">栏目</label>
			    	<select name="category">
			    	<?php
			    	if (!empty($category)) {
			    		foreach ($category as $key=>$value){
			    			if ($value['level']==1) {
			    	?>
			    	<option value="<?=$value['classid']?>"><?=$value['category_name']?></option>
		    		<?php
			    			}else{
			    	?>
			    	<option value="<?=$value['classid']?>">&nbsp;&nbsp;<?=$value['category_name']?></option>
			    	<?php
			    			}
			    		}
		    		}
		    		?>
			    	</select>
			    </div>
			    <div class="am-form-group">
			    	<div class="img_class">
						<div class="img_class_input">
		                    <button name="work_pic" id="upload_button" type="button" class="img_btn">选择要上传的图片</button>
	                		<img src="<?=base_url('templates')?>/assets/img/loading.gif" style="display:none;" id="loadimg" />
	                		<p class="img_div">
	                			<img id="img_path"/>
	                			<input name="img_file" id="img_file" type="hidden" name="img_file" value="" />
	                		</p>
		                </div>
	                </div>
			    </div>
			    <div class="am-form-group">
				    <label for="doc-ipt-copy-1">内容</label>
				    <textarea rows="" cols="" id="content" name="content"></textarea>
			    </div>
			    <input type="hidden" id="doc-ipt-name-1" name="id" >
			    <div class="am-u-sm-12 am-u-sm-offset-4 am-u-md-12 am-u-md-offset-5">
	    			<button type="submit" class="am-btn am-btn-primary">提交</button>
	    		</div>
	  		</fieldset>
		</form>
		<script type="text/javascript">
			$("form").on("submit",function(){
				var title = $("#title").val();
				var author = $("#author").val();
				var source = $("#source").val();
				if(title.length==0){
					alert("没有文章标题");
					return false;
				}
				if(author.length==0){
					alert("没有文章作者");
					return false;
				}
				if(source.length==0){
					alert("没有文章来源");
					return false;
				}
			});
			</script>
		</div>
	</div>
</div>
<?php
	$this->load->view('bottom');
?>