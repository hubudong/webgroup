<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Cate_model extends CI_Model {
	public function __construct() {
		parent::__construct ( 'web_u_c_category' );
	}
    //获取所有栏目
    public function getAllcategory($site){
        $sql = "select * from web_u_c_category where site = {$site}";
        $category = $this->db->query($sql)->result_array();
        return $category;
    }
	public function getFirstCategory($site){
		$sql = "select * from web_u_c_category where site = {$site} and level = 1";
		$data = $this->db->query($sql)->result_array();
		return $data;
	}
	public function getSubCategory($site,$parentid){
		$sql = "select * from web_u_c_category where site = {$site} and parentid = {$parentid}";
		$data = $this->db->query($sql)->result_array();
		return $data;
	}
	public function deleteCateogry($calssid){
		$sql="delete from web_u_c_category where classid = {$calssid}";
		$re = $this->db->query($sql);
		return $re;
	}
	public function getCategory($classid){
		$sql="select * from web_u_c_category where classid = {$classid}";
		$data = $this->db->query($sql)->row_array();
		return $data;
	}
}