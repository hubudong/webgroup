<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Msg_model extends CI_Model {
	public function __construct() {
		parent::__construct ( 'web_u_m_msg_board' );
	}
	public function selectMsg($id){
        $data = $this->db->get_where('web_u_m_msg_board',array('id'=>$id))->row_array();
        return $data;
    }
    //查询留言数据
    public function getMsgData($site,$temp){
        $data = $this->db->get_where('web_u_m_msg_board',array('is_reply'=>$temp,'site'=>$site))->result_array();
        return $data;
    }
	public function getNumMsg($site){
		$sql = "select count(id) as num from web_u_m_msg_board where site = $site and is_reply = 0 ";
		$num = $this->db->query($sql)->row_array();
		return $num['num'];
	}
    //留言分页
    public function getMsgPageData($page=1,$temp){

        $username = $this->session->userdata('username');
        $user = $this->user_model->getUser($username);
        $site = $this->site_model->getSite($user['classid']);

        if ($temp==0){
            $where = "is_reply = 0 and site = {$site}";
            $url = base_url('main/noReplyList/?page={page}');
        }else{
            $where = "is_reply = 1 and site = {$site}";
            $url = base_url('main/replyList/?page={page}');
        }
        $sql = "select count(id) as pageMaxSize from web_u_m_msg_board where {$where}";
        $data = $this->db->query($sql)->row_array();
        $count = $data['pageMaxSize'];

        $params = array(
                "url_template" => $url,
                "page_count" => $count,
                "page_now" => $page,
                "page_size" => 10
        );

        $params["total_pages"] = ceil ( $params['page_count'] / $params['page_size'] );
        //paging文件在shared文件夹中的library，引入就可以
        $this->load->library ( 'paging', $params );

        return $params;
    }
}