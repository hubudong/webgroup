<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Tag_model extends CI_Model {
	public function __construct() {
		parent::__construct ( 'web_u_m_temptags' );
	}
	public function getTempTags($site){
		$sql = "select * from web_u_m_temptags where site = {$site} order by id desc";
		$data = $this->db->query($sql)->result_array();
		foreach ($data as $key=>$value){
			if ($value['article_cate']) {
				$cateSql = "select * from web_u_c_category where classid = {$value['article_cate']}";
				$resData = $this->db->query($cateSql)->row_array();
				$data[$key]['article_cate'] = $resData['category_name'];
			}else{
				$data[$key]['article_cate'] = "全部栏目";
			}
			$configSql = "select * from web_u_c_config_cate where classid = {$value['config_type']}";
			$re = $this->db->query($configSql)->row_array();
			$data[$key]['config_type'] = $re['config_cate_name'];
		}
		return $data;
	}
	public function getTag($id){
		$sql = "select * from web_u_m_temptags where id = {$id}";
		$data = $this->db->query($sql)->row_array();
		$cateSql = "select * from web_u_c_category where classid = {$data['article_cate']}";
		$resData = $this->db->query($cateSql)->row_array();
		$data['category_name'] = $resData['category_name'];
		return $data;
	}
}