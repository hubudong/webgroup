<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Banner_model extends CI_Model {
	public function __construct() {
		parent::__construct ( 'web_u_m_banner' );
	}
	public function getBanner($site){
		
		$data = $this->db->where('site',$site)
		->order_by('id','desc')
		->limit(10)
		->get('web_u_m_banner')
		->result_array();
		return $data;
	}
	public function getOneBanner($id){
		$data = $this->db->get_where('web_u_m_banner',array('id'=>$id))->row_array();
		return $data;
	}
}