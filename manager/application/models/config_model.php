<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Config_model extends CI_Model {
	public function __construct() {
		parent::__construct ( 'web_u_c_config_cate' );
	}
	public function getAllConfig(){
		$query = $this->db->get('web_u_c_config_cate');
		return $query->result();
	}
}