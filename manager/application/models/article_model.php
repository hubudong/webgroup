<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Article_model extends CI_Model {
	public function __construct() {
		parent::__construct ( 'web_u_m_article' );
	}
	public function getArticle($site,$max,$cate){
//		$where = "site = $site and category = $cate order by id desc limit 0,$cate";
//		$sql = "select * from web_u_m_article where $where";
//		$data = $this->db->query($sql)->result_array();
        $data = $this->db->where('site',$site)
                 ->where('category',$cate)
                 ->order_by('id','desc')
                 ->limit($max)
                 ->get('web_u_m_article')
                 ->result_array();
		return $data;
	}
	public function getFocus($site,$max=5){
//		$where = "where site = $site and focus != '' order by id desc limit 0,$max";
//		$sql = "select id,focus,title from web_u_m_article $where";
//		$data = $this->db->query($sql)->result_array();
        $data = $this->db->select('id,focus,title')
                 ->where('site',$site)
                 ->where('focus !=','')
                 ->order_by('id','desc')
                 ->limit($max)
                 ->get('web_u_m_article')
                 ->result_array();
		return $data;
	}
	public function getArticleList($id,$offset=0){
	    $limit=10;
//		$sql = "select * from web_u_m_article where category = $id limit 0,10";
		$data = $this->db->get_where('web_u_m_article',array('category'=>$id),$limit,$offset)->result_array();
		return $data;
	}

	public function getArticleCon($id){
//		$sql = "select * from web_u_m_article where id = {$id}";
		$data = $this->db->get_where('web_u_m_article',array('id'=>$id))->row_array();
		return $data;
	}
}