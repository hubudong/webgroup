<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class User_model extends CI_Model {
	public function __construct() {
		parent::__construct ( 'web_u_c_user' );
	}

	public function getUser($username){
		$re = $this->db->get_where('web_u_c_user',array('username'=>$username))->row_array();
		return $re;
	}
	public function count($username){
		$sql = "select count(username) as num from web_u_c_user where username = '{$username}' ";
		$re = $this->db->query($sql)->row_array();
		$data = $re['num'];
		return $data;
	}
    /**
     * 密码加密
     */
    public function _encrypt($str) {
        $this->load->library ( 'encrypt' );
        $re = $this->encrypt->encode ( $str );
        return $re;
    }
}