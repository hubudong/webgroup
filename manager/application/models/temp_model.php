<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Temp_model extends CI_Model {
	public function __construct() {
		parent::__construct ( 'web_u_m_templates' );
	}
	public function getTemplate(){
		$this->db->select('classid,template_name');
		$this->db->order_by('classid','desc');
		$data = $this->db->get('web_u_c_templates')->result_array();
		return $data;
	}
    public function getTemplateItem($id){
        $data = $this->db->get_where('web_u_c_templates',array('classid'=>$id))->row_array();
        return $data;
    }
    public function recentTemplateItem(){
        return $this->db->order_by('classid','desc')->get('web_u_c_templates')->row_array();
    }
}