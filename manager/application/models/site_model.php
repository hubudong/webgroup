<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Site_model extends CI_Model {
	public function __construct() {
		parent::__construct ( 'web_u_c_site' );
	}
    public function getSiteInfo($user){
        $data = $this->db->get_where('web_u_c_site',array('user'=>$user))->row_array();
        return $data;
    }
    public function setSiteSetting($data,$user){
        $data = $this->db->update('web_u_c_site',$data,Array('user'=>$user));
        return $data;
    }
	public function getSite($user){
		$data = $this->getSiteInfo($user);
		return $data['classid'];
	}
    public function getSiteTemp($user){
        $data = $this->getSiteInfo($user);
        return $data['temp'];
    }
}