<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Linkcate_model extends CI_Model {
	public function __construct() {
		parent::__construct ( 'web_u_c_link_cate' );
	}
	public function getAllcate($site){
		$this->db->where('site',$site);
		$query = $this->db->get('web_u_c_link_cate');
		return $query->result();
	}
}