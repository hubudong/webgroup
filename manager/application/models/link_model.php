<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Link_model extends CI_Model {
	public function __construct() {
		parent::__construct ( 'web_u_m_link' );
	}
	public function getLinkList($site){
		$this->db->where('web_u_m_link.site',$site);
		$this->db->order_by('web_u_m_link.id','desc');
        $this->db->select('web_u_m_link.id,web_u_m_link.create_time,web_u_m_link.link_name,web_u_m_link.link_url,web_u_m_link.link_cate,web_u_m_link.site,web_u_c_link_cate.link_cate_name');
        $this->db->join('web_u_c_link_cate', 'web_u_c_link_cate.classid = web_u_m_link.link_cate', 'left');
		$data = $this->db->get('web_u_m_link')->result_array();
		return $data;
	}
	public function getLinkCon($id){
		$this->db->where('id',$id);
		$query = $this->db->get('web_u_m_link');
		return $query->row();
	}
}