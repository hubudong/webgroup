<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 后台核心类
 *
 * @author ry
 *
 */
class Admin {
	protected $CI;
	public $username;
	public $model="user_model";
	public function __construct() {
		$this->CI = & get_instance (); // 调用CI核心对象
	}
	public function model()
	{
		$this->CI->load->model ( $this->model );
	}
	/**
	 * 登记管理员登录状态
	 *
	 * @param string $id
	 */
	public function login($admin_user) {
		$this->CI->load->library ( 'session' );
		if (! empty ( $admin_user )) {
			$this->CI->session->set_userdata ( 'admin_user', $admin_user );
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 登录检测
	 *
	 * @param string $id
	 * @param string $url
	 * @return boolean
	 */
	public function isLogin($url = '') {
		$this->CI->load->library ( 'session' );
		$v = $this->CI->session->userdata ( 'admin_user' );
		if (empty ( $v ) && empty ( $url )) {
			return FALSE;
		} elseif (empty ( $v )) {
			redirect ( $url );
		} else {
			$this->username = $v;
			return TRUE;
		}
	}

	/**
	 * 设置错误码信息
	 *
	 * @param string $code
	 */
	public function setErr($code) {
		$this->CI->load->library ( 'session' );
		if (! empty ( $code )) {
			$this->CI->session->set_flashdata ( 'err', $code );
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 闪出错误信息
	 */
	public function flashErr() {
		$this->CI->load->library ( 'session' );
		$err = $this->CI->session->flashdata ( 'err' );
		echo $err;
	}
	/**
	 * 显示错误信息
	 */
	public function showErr() {
		$this->CI->load->library ( 'session' );
		$this->session->keep_flashdata ( 'err' );
		$err = $this->CI->session->flashdata ( 'err' );
		echo $err;
	}
	/**
	 * 验证是否存在该管理员
	 *
	 * @param string $id
	 */
	public function isExistAdminUser($admin_user) {
		$whereArr = array(
				"admin_user" => $admin_user,
		);
		$re = $this->CI->{$this->model}->count ( $whereArr );
		if ($re > 0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 验证管理员密码
	 *
	 * @param array $whereArr
	 * @param string $password
	 */
	public function adminUserPwd($admin_user, $admin_psd) {
		$whereArr = array(
				"admin_user" => $admin_user,
		);
		$re = $this->CI->{$this->model}->row ( $whereArr );
		if (is_array ( $re ) && $admin_psd === $re["admin_psd"] ) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 密码加密
	 */
	private function _encrypt($str) {
		$this->CI->load->library ( 'encrypt' );
		$re = $this->CI->encrypt->encode ( $str );
		return $re;
	}
	/**
	 * 密码解密
	 */
	private function _decrypt($str) {
		$this->CI->load->library ( 'encrypt' );
		$re = $this->CI->encrypt->decode ( $str );
		return $re;
	}

}