<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
/**
 * Paging class
 *
 * @version : 1.2.0
 * @author : LiChao 2015-5-5
 *         默认使用AmazeUI样式命名
 *        
 */
class Paging {
	public $url_template = '{page}'; // 分页跳转地址模版
	public $page_count = 100; // 总共的数据记录条数
	public $page_size = 10; // 每页显示的数据条数
	public $page_now = 1; // 当前页码
	public $page_display_num = 10; // 显示页码个数
	public $total_pages; // 总共有多少页
	public $both_ends = TRUE; // 显示首尾页按钮
	/* 显示内容定义 */
	public $lang = array(
			'first' => '首页', 
			'last' => '尾页', 
			'prev' => '上一页', 
			'next' => '下一页' 
	);
	/* 常用样式定义 */
	public $class = array(
			'ul' => 'am-pagination am-pagination-centered', 
			'disabled' => 'am-disabled', 
			'active' => 'am-active' 
	);
	/**
	 * 初始化配置参数
	 */
	public function __construct($params = array()) {
		$this->total_pages = ceil ( $this->page_count / $this->page_size );
		$this->init ( $params );
	}
	public function init($params = array()) {
		if (count ( $params ) > 0) {
			foreach ( $params as $key => $val ) {
				if (isset ( $this->$key )) {
					$this->$key = $val;
				}
			}
		}
	}
	/**
	 * 创建分页
	 * <li> 里面的非链接文字需要使用 <span> 包裹。
	 */
	public function create() {
		echo '<ul class=', $this->class['ul'], '>';
		$this->_list ();
		echo '</ul>';
	}
	/**
	 * 构造li标签
	 */
	private function _list() {
		// $page = ceil ( $this->page_count / $this->page_size ); // $page一共有多少页
		if ($this->both_ends === TRUE) {
			$this->_format_href ( 1, $this->lang['first'] );
			$this->_page_list ();
			$this->_format_href ( $this->total_pages, $this->lang['last'] );
		} else {
			$this->_page_list ();
		}
	}
	/**
	 * 构造页码超链接
	 */
	private function _page_list() {
		$c = '';
		$p = $this->page_now; // 当前页码
		$p < 1 ? $p = 1 : $p;
		$pd = $this->page_display_num; // 显示页码个数
		$pt = $this->total_pages;
		$pk = intval ( $pd / 2 );
		$pky = ($pd + 1) % 2;
		$prev = $p - 1;
		$next = $p + 1;
		if ($pt <= $pd) {
			$start = 1;
			$end = $pt;
		} elseif ($pt <= $p + $pk) {
			$start = $pt - $pd + 1;
			$end = $pt;
		} elseif ($p <= $pk) {
			$start = 1;
			$end = $pd;
		} elseif ($p > $pk) {
			$start = $p - $pk + $pky;
			$end = $p + $pk;
		}
		/* 上一页 */
		$this->_format_href ( $prev, $this->lang['prev'] );
		/* build */
		for($i = $start; $i <= $end; $i ++) {
			if ($i == $p) {
				$c = $this->class['active'];
			}
			$this->_format_href ( $i, $i, $c );
			$c = '';
		}
		/* 下一页 */
		$this->_format_href ( $next, $this->lang['next'] );
	}
	/**
	 * 格式化超链接
	 *
	 * @param num $start        	
	 * @param num $end        	
	 */
	private function _format_href($page, $text, $class = '') {
		$c = '';
		if (! empty ( $class )) {
			$c = 'class=' . $class;
		}
		if ($page <= 0 || $page > $this->total_pages) {
			$c = 'class=' . $this->class['disabled'];
		}
		echo '<li ', $c, '><a href="', $this->_format_url ( $page ), '">', $text, '</a></li>';
	}
	
	/**
	 * 格式化分页URL
	 * 替换 {page} 关键词，含半角花括号
	 */
	private function _format_url($page, $type = 1) {
		if ($type === 1) { // 页码链接模版中使用 #page# 表示页码变量
			$page_url = str_replace ( '{page}', $page, $this->url_template );
			return $page_url;
		}
	}
}
 