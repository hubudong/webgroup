<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * Token Class
 *
 * @author LeeNux
 * @version 1.0
 *         
 */
class Token {
	private $code; // 平台身份代码
	private $publicKey; // 公钥
	private $secretKey; // 密钥
	private $timestamp; // 时间戳
	private $getKeyUrl; // 获取公钥地址
	protected $CI;
	public function __construct($params) {
		$this->CI = & get_instance ();
		if ($params['mode'] === 'client') {
			$this->_clientInit ();
		} elseif ($params['mode'] === 'server') {
			$this->_serverInit ();
		}
	}
	// ------------------------------------------------------------------------
	// Client-side
	// ------------------------------------------------------------------------
	/**
	 * 客户端模式初始化
	 */
	private function _clientInit() {
		$this->CI->config->load ( 'token', FALSE ); // load config
		$token = $this->CI->config->item ( 'token' );
		$this->code = $token['code'];
		$this->secretKey = $token['secretKey'];
		$this->getKeyUrl = $token['getKeyUrl'];
	}
	// ------------------------------------------------------------------------
	// Server-side
	// ------------------------------------------------------------------------
	/**
	 * 服务器端模式初始化
	 */
	private function _serverInit() {
		$this->CI->load->model ( 'token_model' );
		$code = $this->CI->input->post_get ( 'code' );
		$whereArr = array(
				'code' => $code 
		);
		$re = $this->CI->token_model->row ( $whereArr );
		if (is_array ( $re )) {
			$this->code = $code;
			$this->publicKey = $re['publicKey'];
			$this->secretKey = $re['secretKey'];
			$this->timestamp = $re['timestamp'];
			return TRUE;
		} else {
			return FALSE;
		}
	}
	/**
	 * 验证token有效性
	 */
	public function isValidToken() {
		$access_token = $this->CI->input->post_get ( 'token' );
		$real_token = $this->_buildToken ();
		if ($access_token === $real_token) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	/**
	 * 获取公钥
	 */
	public function getKey() {
		$whereArr = array(
				'code' => $this->code 
		);
		$dataArr = array(
				'publicKey' => $this->_buildPublicKey (), 
				'timestamp' => time () 
		);
		$re = $this->CI->token_model->update ( $dataArr, $whereArr );
		if ($re > 0) {
			return $dataArr;
		} else {
			return FALSE;
		}
	}
	/**
	 * 更换密钥
	 */
	public function reSecretKey() {
		$whereArr = array(
				'code' => $this->code 
		);
		$dataArr = array(
				'secretKey' => $this->_buildSecretKey () 
		);
		$re = $this->CI->token_model->update ( $dataArr, $whereArr );
		if ($re > 0) {
			return $dataArr;
		} else {
			return FALSE;
		}
	}
	/**
	 * 构造公钥
	 *
	 * @return string
	 */
	private function _buildPublicKey() {
		return random_string ( 'alnum', 10 );
	}
	/**
	 * 构造密钥
	 *
	 * @return string
	 */
	private function _buildSecretKey() {
		return random_string ( 'unique' );
	}
	/**
	 * 构造Token
	 *
	 * @return string
	 */
	private function _buildToken() {
		$code = $this->code;
		$publicKey = $this->publicKey;
		$secretKey = $this->secretKey;
		$timestamp = $this->timestamp;
		if (! empty ( $code ) && ! empty ( $publicKey ) && ! empty ( $secretKey ) && ! empty ( $timestamp )) {
			$token = md5 ( $code . $publicKey . $timestamp . $secretKey );
			return $token;
		} else {
			return FALSE;
		}
	}
	/**
	 * 调试
	 */
	public function debug() {
		$code = $this->code;
		$publicKey = $this->publicKey;
		$secretKey = $this->secretKey;
		$timestamp = $this->timestamp;
		echo '<br>';
		echo $code, $publicKey, $secretKey, $timestamp;
		echo '<br>';
		echo $this->_buildToken ();
		echo '<br>';
	}
}