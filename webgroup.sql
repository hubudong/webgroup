/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : webgroup

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-02-03 11:26:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `web_admins`
-- ----------------------------
DROP TABLE IF EXISTS `web_admins`;
CREATE TABLE `web_admins` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` smallint(5) unsigned DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '1=正常，2=冻结',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`),
  KEY `group` (`role`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_admins
-- ----------------------------
INSERT INTO `web_admins` VALUES ('1', 'superadmin', 'd5a602a16d0c8d5385fcb3656947690ac67ff409', 'fc7ac036f6', 'hello@dilicms.com', '1', '1');

-- ----------------------------
-- Table structure for `web_attachments`
-- ----------------------------
DROP TABLE IF EXISTS `web_attachments`;
CREATE TABLE `web_attachments` (
  `aid` int(10) NOT NULL AUTO_INCREMENT,
  `uid` smallint(10) NOT NULL DEFAULT '0',
  `model` mediumint(10) DEFAULT '0',
  `from` tinyint(1) DEFAULT '0' COMMENT '0:content model,1:cate model',
  `content` int(10) DEFAULT '0',
  `name` varchar(30) DEFAULT NULL,
  `folder` varchar(15) DEFAULT NULL,
  `realname` varchar(50) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `image` tinyint(1) DEFAULT '0',
  `posttime` int(11) DEFAULT '0',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_attachments
-- ----------------------------
INSERT INTO `web_attachments` VALUES ('1', '1', '0', '0', '0', '147312321144d6d354ec72bae0', '2016/09', 'logo', 'jpg', '1', '1473123211');
INSERT INTO `web_attachments` VALUES ('2', '1', '0', '0', '0', '14731234909dacef3a2d6c792d', '2016/09', 'logo', 'jpg', '1', '1473123490');
INSERT INTO `web_attachments` VALUES ('3', '1', '0', '0', '0', '1473124167df49da66274e7963', '2016/09', 'logo', 'jpg', '1', '1473124167');
INSERT INTO `web_attachments` VALUES ('4', '1', '0', '0', '0', '1473127034751d54c5961e3c5c', '2016/09', 'logo', 'jpg', '1', '1473127034');
INSERT INTO `web_attachments` VALUES ('5', '1', '0', '0', '0', '1473127073e199035aaf492210', '2016/09', 'img1', 'jpg', '1', '1473127073');
INSERT INTO `web_attachments` VALUES ('6', '1', '0', '0', '0', '1473127090605b7e3cdfdb79a8', '2016/09', 'kongjian', 'jpg', '1', '1473127090');
INSERT INTO `web_attachments` VALUES ('7', '1', '0', '0', '0', '1473127111b4b91fd56adadd0d', '2016/09', 'img1', 'jpg', '1', '1473127111');
INSERT INTO `web_attachments` VALUES ('9', '1', '6', '0', '2', '1474245354880d269d838a1c8f', '2016/09', '5', 'jpg', '1', '1474245354');
INSERT INTO `web_attachments` VALUES ('10', '1', '6', '0', '2', '14742456354fe63c0488655c1f', '2016/09', '5', 'jpg', '1', '1474245635');
INSERT INTO `web_attachments` VALUES ('11', '1', '6', '0', '2', '1474245756f6a80f4372066b0d', '2016/09', '9', 'jpg', '1', '1474245756');
INSERT INTO `web_attachments` VALUES ('12', '1', '0', '0', '0', '14742458550146a00bc82b50ef', '2016/09', '8', 'jpg', '1', '1474245855');
INSERT INTO `web_attachments` VALUES ('13', '1', '6', '0', '2', '1474247735f4fa158a282a7604', '2016/09', '5', 'jpg', '1', '1474247735');
INSERT INTO `web_attachments` VALUES ('14', '1', '6', '0', '2', '1474247771ae072aeae2c6c008', '2016/09', '8', 'jpg', '1', '1474247771');
INSERT INTO `web_attachments` VALUES ('15', '1', '3', '1', '1', '14748745050a90fdcb7b05f76a', '2016/09', 'ne_11_yud7', 'png', '1', '1474874505');
INSERT INTO `web_attachments` VALUES ('16', '1', '3', '1', '2', '1474874527a61b4c3ddfaa098e', '2016/09', 'ne_11_yud7', 'png', '1', '1474874527');
INSERT INTO `web_attachments` VALUES ('17', '1', '3', '1', '2', '1474938800861e3690d4f4948d', '2016/09', 'top', 'png', '1', '1474938800');
INSERT INTO `web_attachments` VALUES ('18', '1', '3', '1', '1', '1474938953dfc1ac01852bc2c4', '2016/09', 'xxgc', 'png', '1', '1474938953');
INSERT INTO `web_attachments` VALUES ('19', '1', '10', '0', '1', '1475306344bd715006f371d3e6', '2016/10', 'ybt_ad', 'jpg', '1', '1475306344');
INSERT INTO `web_attachments` VALUES ('20', '1', '10', '0', '2', '14753063701804037f5bd27160', '2016/10', 'banner', 'png', '1', '1475306370');
INSERT INTO `web_attachments` VALUES ('21', '1', '6', '0', '9', '1476771548ca25ee98cf122745', '2016/10', 'focus', 'png', '1', '1476771548');

-- ----------------------------
-- Table structure for `web_backend_settings`
-- ----------------------------
DROP TABLE IF EXISTS `web_backend_settings`;
CREATE TABLE `web_backend_settings` (
  `backend_theme` varchar(15) DEFAULT NULL,
  `backend_lang` varchar(10) DEFAULT NULL,
  `backend_root_access` tinyint(1) unsigned DEFAULT '1',
  `backend_access_point` varchar(20) DEFAULT 'admin',
  `backend_title` varchar(100) DEFAULT 'DiliCMS后台管理',
  `backend_logo` varchar(100) DEFAULT 'images/logo.gif',
  `plugin_dev_mode` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `backend_http_auth_on` tinyint(1) DEFAULT '0',
  `backend_http_auth_user` varchar(40) DEFAULT NULL,
  `backend_http_auth_password` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_backend_settings
-- ----------------------------
INSERT INTO `web_backend_settings` VALUES ('default', 'zh-cn', '1', '', '站群管理系统', 'images/logo.png', '0', '0', '', '');

-- ----------------------------
-- Table structure for `web_cate_fields`
-- ----------------------------
DROP TABLE IF EXISTS `web_cate_fields`;
CREATE TABLE `web_cate_fields` (
  `id` mediumint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(40) DEFAULT NULL,
  `model` smallint(10) unsigned DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `length` smallint(10) unsigned DEFAULT NULL,
  `values` tinytext,
  `width` smallint(10) DEFAULT NULL,
  `height` smallint(10) DEFAULT NULL,
  `rules` tinytext,
  `ruledescription` tinytext,
  `searchable` tinyint(1) unsigned DEFAULT NULL,
  `listable` tinyint(1) unsigned DEFAULT NULL,
  `order` int(5) unsigned DEFAULT NULL,
  `editable` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`model`),
  KEY `model` (`model`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_cate_fields
-- ----------------------------
INSERT INTO `web_cate_fields` VALUES ('5', 'site_url', '站点地址', '3', 'input', '0', '', '0', '0', 'required', '', '1', '1', '1', '1');
INSERT INTO `web_cate_fields` VALUES ('4', 'site_name', '站点名称', '3', 'input', '0', '', '0', '0', 'required', '', '1', '1', '0', '1');
INSERT INTO `web_cate_fields` VALUES ('3', 'category_name', '分类名称', '2', 'input', '0', '', '0', '0', 'required', '', '1', '1', '0', '1');
INSERT INTO `web_cate_fields` VALUES ('6', 'template_name', '模板名称', '4', 'input', '0', '', '0', '0', 'required', '', '1', '1', '0', '1');
INSERT INTO `web_cate_fields` VALUES ('7', 'index', '首页', '4', 'textarea', '0', '', '0', '0', 'required', '', '0', '0', '1', '1');
INSERT INTO `web_cate_fields` VALUES ('8', 'common_top', '头部', '4', 'textarea', '0', '', '0', '0', 'required', '', '0', '0', '2', '1');
INSERT INTO `web_cate_fields` VALUES ('9', 'common_foot', '底部', '4', 'textarea', '0', '', '0', '0', 'required', '', '0', '0', '3', '1');
INSERT INTO `web_cate_fields` VALUES ('10', 'list', '列表页', '4', 'textarea', '0', '', '0', '0', 'required', '', '0', '0', '4', '1');
INSERT INTO `web_cate_fields` VALUES ('11', 'content', '内容页', '4', 'textarea', '0', '', '0', '0', 'required', '', '0', '0', '5', '1');
INSERT INTO `web_cate_fields` VALUES ('12', 'msg_board', '留言板', '4', 'textarea', '0', '', '0', '0', '', '', '0', '0', '6', '1');
INSERT INTO `web_cate_fields` VALUES ('13', 'site', '站点', '2', 'select_from_model', '0', 'site|site_name', '0', '0', 'required', '', '0', '1', '2', '1');
INSERT INTO `web_cate_fields` VALUES ('14', 'link_cate_name', '分类名', '5', 'input', '0', '', '0', '0', 'required', '', '1', '1', '0', '1');
INSERT INTO `web_cate_fields` VALUES ('16', 'site_logo', 'Logo', '3', 'file', '0', '', '0', '0', '', '', '0', '0', '2', '1');
INSERT INTO `web_cate_fields` VALUES ('17', 'site_copy', '站点底部', '3', 'wysiwyg', '0', '', '0', '0', 'required', '', '0', '0', '3', '1');
INSERT INTO `web_cate_fields` VALUES ('18', 'temp', '模板', '3', 'select_from_model', '0', 'templates|template_name', '0', '0', 'required', '', '0', '1', '4', '1');
INSERT INTO `web_cate_fields` VALUES ('19', 'username', '用户名', '6', 'input', '0', '', '0', '0', 'required', '', '0', '1', '0', '1');
INSERT INTO `web_cate_fields` VALUES ('20', 'password', '密码', '6', 'input', '0', '', '0', '0', 'required', '', '0', '1', '1', '1');
INSERT INTO `web_cate_fields` VALUES ('21', 'add_time', '添加时间', '6', 'datetime', '0', '', '0', '0', 'required', '', '0', '1', '2', '1');
INSERT INTO `web_cate_fields` VALUES ('22', 'user', '用户', '3', 'select_from_model', '0', 'user|username', '0', '0', 'required', '', '0', '1', '5', '1');
INSERT INTO `web_cate_fields` VALUES ('23', 'site', '站点', '5', 'select_from_model', '0', 'site|site_name', '0', '0', 'required', '', '0', '1', '1', '1');
INSERT INTO `web_cate_fields` VALUES ('24', 'config_cate_name', 'config分类', '7', 'input', '0', '', '0', '0', 'required', '', '1', '1', '0', '1');
INSERT INTO `web_cate_fields` VALUES ('25', 'search', '搜索', '4', 'textarea', '0', '', '0', '0', '', '', '0', '0', '7', '1');

-- ----------------------------
-- Table structure for `web_cate_models`
-- ----------------------------
DROP TABLE IF EXISTS `web_cate_models`;
CREATE TABLE `web_cate_models` (
  `id` mediumint(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(40) NOT NULL,
  `perpage` varchar(2) NOT NULL,
  `level` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `hasattach` tinyint(1) NOT NULL DEFAULT '0',
  `built_in` tinyint(1) DEFAULT '0',
  `auto_update` tinyint(1) DEFAULT '0',
  `thumb_preferences` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_cate_models
-- ----------------------------
INSERT INTO `web_cate_models` VALUES ('3', 'site', '站点表', '20', '1', '1', '0', '1', '{\"enabled\":[],\"default\":\"original\"}');
INSERT INTO `web_cate_models` VALUES ('2', 'category', '栏目分类', '20', '2', '1', '0', '1', '{\"enabled\":[],\"default\":\"original\"}');
INSERT INTO `web_cate_models` VALUES ('4', 'templates', '模板', '20', '1', '1', '0', '1', '{\"enabled\":[],\"default\":\"original\"}');
INSERT INTO `web_cate_models` VALUES ('5', 'link_cate', '链接分类', '20', '1', '1', '0', '1', '{\"enabled\":[],\"default\":\"original\"}');
INSERT INTO `web_cate_models` VALUES ('6', 'user', '用户表', '20', '1', '1', '0', '1', '{\"enabled\":[],\"default\":\"original\"}');
INSERT INTO `web_cate_models` VALUES ('7', 'config_cate', 'config类型', '20', '1', '1', '0', '1', '{\"enabled\":[],\"default\":\"original\"}');

-- ----------------------------
-- Table structure for `web_fieldtypes`
-- ----------------------------
DROP TABLE IF EXISTS `web_fieldtypes`;
CREATE TABLE `web_fieldtypes` (
  `k` varchar(20) NOT NULL,
  `v` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_fieldtypes
-- ----------------------------
INSERT INTO `web_fieldtypes` VALUES ('int', '整形(INT)');
INSERT INTO `web_fieldtypes` VALUES ('float', '浮点型(FLOAT)');
INSERT INTO `web_fieldtypes` VALUES ('input', '单行文本框(VARCHAR)');
INSERT INTO `web_fieldtypes` VALUES ('textarea', '文本区域(VARCHAR)');
INSERT INTO `web_fieldtypes` VALUES ('select', '下拉菜单(VARCHAR)');
INSERT INTO `web_fieldtypes` VALUES ('select_from_model', '下拉菜单(模型数据)(INT)');
INSERT INTO `web_fieldtypes` VALUES ('linked_menu', '联动下拉菜单(VARCHAR)');
INSERT INTO `web_fieldtypes` VALUES ('radio', '单选按钮(VARCHAR)');
INSERT INTO `web_fieldtypes` VALUES ('radio_from_model', '单选按钮(模型数据)(INT)');
INSERT INTO `web_fieldtypes` VALUES ('checkbox', '复选框(VARCHAR)');
INSERT INTO `web_fieldtypes` VALUES ('checkbox_from_model', '复选框(模型数据)(VARCHAR)');
INSERT INTO `web_fieldtypes` VALUES ('wysiwyg', '编辑器(TEXT)');
INSERT INTO `web_fieldtypes` VALUES ('wysiwyg_basic', '编辑器(简)(TEXT)');
INSERT INTO `web_fieldtypes` VALUES ('datetime', '日期时间(VARCHAR)');
INSERT INTO `web_fieldtypes` VALUES ('content', '内容模型调用(INT)');

-- ----------------------------
-- Table structure for `web_menus`
-- ----------------------------
DROP TABLE IF EXISTS `web_menus`;
CREATE TABLE `web_menus` (
  `menu_id` tinyint(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_name` varchar(20) NOT NULL,
  `method_name` varchar(30) NOT NULL,
  `menu_name` varchar(20) NOT NULL,
  `menu_level` tinyint(2) unsigned DEFAULT '0',
  `menu_parent` tinyint(10) unsigned DEFAULT '0',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_menus
-- ----------------------------
INSERT INTO `web_menus` VALUES ('1', 'system', 'home', '系统', '0', '0');
INSERT INTO `web_menus` VALUES ('2', 'system', 'home', '后台首页', '1', '1');
INSERT INTO `web_menus` VALUES ('3', 'system', 'home', '后台首页', '2', '2');
INSERT INTO `web_menus` VALUES ('4', 'setting', 'site', '系统设置', '1', '1');
INSERT INTO `web_menus` VALUES ('5', 'setting', 'site', '站点设置', '2', '4');
INSERT INTO `web_menus` VALUES ('6', 'setting', 'backend', '后台设置', '2', '4');
INSERT INTO `web_menus` VALUES ('7', 'system', 'password', '修改密码', '2', '4');
INSERT INTO `web_menus` VALUES ('8', 'system', 'cache', '更新缓存', '2', '4');
INSERT INTO `web_menus` VALUES ('9', 'model', 'view', '模型管理', '1', '1');
INSERT INTO `web_menus` VALUES ('10', 'model', 'view', '内容模型管理', '2', '9');
INSERT INTO `web_menus` VALUES ('11', 'category', 'view', '分类模型管理', '2', '9');
INSERT INTO `web_menus` VALUES ('12', 'plugin', 'view', '扩展管理', '1', '1');
INSERT INTO `web_menus` VALUES ('13', 'plugin', 'view', '插件管理', '2', '12');
INSERT INTO `web_menus` VALUES ('14', 'role', 'view', '权限管理', '1', '1');
INSERT INTO `web_menus` VALUES ('15', 'role', 'view', '用户组管理', '2', '14');
INSERT INTO `web_menus` VALUES ('16', 'user', 'view', '用户管理', '2', '14');
INSERT INTO `web_menus` VALUES ('17', 'content', 'view', '内容管理', '0', '0');
INSERT INTO `web_menus` VALUES ('18', 'content', 'view', '内容管理', '1', '17');
INSERT INTO `web_menus` VALUES ('19', 'category_content', 'view', '分类管理', '1', '17');
INSERT INTO `web_menus` VALUES ('20', 'module', 'run', '插件', '0', '0');
INSERT INTO `web_menus` VALUES ('21', 'database', 'index', '数据库管理', '1', '1');
INSERT INTO `web_menus` VALUES ('22', 'database', 'index', '数据库备份', '2', '21');
INSERT INTO `web_menus` VALUES ('23', 'database', 'recover', '数据库还原', '2', '21');
INSERT INTO `web_menus` VALUES ('24', 'database', 'optimize', '数据库优化', '2', '21');

-- ----------------------------
-- Table structure for `web_models`
-- ----------------------------
DROP TABLE IF EXISTS `web_models`;
CREATE TABLE `web_models` (
  `id` smallint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(50) NOT NULL,
  `perpage` varchar(2) NOT NULL DEFAULT '10',
  `hasattach` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `built_in` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `thumb_preferences` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_models
-- ----------------------------
INSERT INTO `web_models` VALUES ('6', 'article', '文章表', '20', '1', '0', '{\"enabled\":[],\"default\":\"original\"}');
INSERT INTO `web_models` VALUES ('7', 'link', '链接表', '20', '1', '0', '{\"enabled\":[],\"default\":\"original\"}');
INSERT INTO `web_models` VALUES ('8', 'msg_board', '留言板', '20', '1', '0', '{\"enabled\":[],\"default\":\"original\"}');
INSERT INTO `web_models` VALUES ('9', 'tempTags', '模板标签', '20', '1', '0', '{\"enabled\":[],\"default\":\"original\"}');
INSERT INTO `web_models` VALUES ('10', 'banner', 'banner图', '20', '1', '0', '{\"enabled\":[],\"default\":\"original\"}');

-- ----------------------------
-- Table structure for `web_model_fields`
-- ----------------------------
DROP TABLE IF EXISTS `web_model_fields`;
CREATE TABLE `web_model_fields` (
  `id` mediumint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(40) NOT NULL,
  `model` smallint(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(20) DEFAULT NULL,
  `length` smallint(10) unsigned DEFAULT NULL,
  `values` tinytext NOT NULL,
  `width` smallint(10) unsigned NOT NULL,
  `height` smallint(10) unsigned NOT NULL,
  `rules` tinytext NOT NULL,
  `ruledescription` tinytext NOT NULL,
  `searchable` tinyint(1) unsigned NOT NULL,
  `listable` tinyint(1) unsigned NOT NULL,
  `order` int(5) unsigned DEFAULT NULL,
  `editable` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`model`),
  KEY `model` (`model`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_model_fields
-- ----------------------------
INSERT INTO `web_model_fields` VALUES ('26', 'content', '内容', '6', 'wysiwyg', '0', '', '0', '0', 'required', '', '0', '0', '5', '1');
INSERT INTO `web_model_fields` VALUES ('25', 'focus', '焦点图', '6', 'file', '0', '', '0', '0', '', '', '0', '0', '4', '1');
INSERT INTO `web_model_fields` VALUES ('24', 'category', '栏目', '6', 'select_from_model', '0', 'category|category_name', '0', '0', 'required', '', '0', '1', '3', '1');
INSERT INTO `web_model_fields` VALUES ('21', 'title', '题目', '6', 'input', '0', '', '0', '0', 'required', '', '1', '1', '0', '1');
INSERT INTO `web_model_fields` VALUES ('22', 'author', '作者', '6', 'input', '0', '', '0', '0', 'required', '', '1', '1', '1', '1');
INSERT INTO `web_model_fields` VALUES ('23', 'source', '来源', '6', 'input', '0', '', '0', '0', 'required', '', '1', '1', '2', '1');
INSERT INTO `web_model_fields` VALUES ('34', 'add_time', '发布时间', '6', 'datetime', '0', '', '0', '0', 'required', '', '0', '1', '9', '1');
INSERT INTO `web_model_fields` VALUES ('27', 'hits', '点击量', '6', 'int', '0', '', '0', '0', '', '', '0', '1', '6', '1');
INSERT INTO `web_model_fields` VALUES ('28', 'ontop', '置顶', '6', 'int', '0', '', '0', '0', '', '', '0', '0', '7', '1');
INSERT INTO `web_model_fields` VALUES ('29', 'user', '发布人', '6', 'select_from_model', '0', 'user|username', '0', '0', 'required', '', '0', '1', '8', '1');
INSERT INTO `web_model_fields` VALUES ('30', 'link_name', '链接名', '7', 'input', '0', '', '0', '0', 'required', '', '1', '1', '0', '1');
INSERT INTO `web_model_fields` VALUES ('31', 'link_url', '链接地址', '7', 'input', '0', '', '0', '0', 'required', '', '1', '1', '1', '1');
INSERT INTO `web_model_fields` VALUES ('32', 'link_cate', '链接分类', '7', 'select_from_model', '0', 'link_cate|link_cate_name', '0', '0', 'required', '', '0', '1', '2', '1');
INSERT INTO `web_model_fields` VALUES ('33', 'site', '站点', '7', 'select_from_model', '0', 'site|site_name', '0', '0', 'required', '', '0', '1', '3', '1');
INSERT INTO `web_model_fields` VALUES ('35', 'title', '标题', '8', 'input', '0', '', '0', '0', 'required', '', '1', '1', '0', '1');
INSERT INTO `web_model_fields` VALUES ('36', 'content', '内容', '8', 'textarea', '0', '', '0', '0', 'required', '', '0', '0', '1', '1');
INSERT INTO `web_model_fields` VALUES ('37', 'reply', '回复', '8', 'textarea', '0', '', '0', '0', '', '', '0', '0', '2', '1');
INSERT INTO `web_model_fields` VALUES ('38', 'add_time', '添加时间', '8', 'datetime', '0', '', '0', '0', 'required', '', '0', '1', '3', '1');
INSERT INTO `web_model_fields` VALUES ('39', 'is_reply', '是否回复', '8', 'radio', '0', '0=否|1=是', '0', '0', 'required', '', '0', '1', '4', '1');
INSERT INTO `web_model_fields` VALUES ('40', 'site', '站点', '8', 'select_from_model', '0', 'site|site_url', '0', '0', 'required', '', '0', '1', '6', '1');
INSERT INTO `web_model_fields` VALUES ('41', 'reply_time', '回复时间', '8', 'datetime', '0', '', '0', '0', '', '', '0', '1', '5', '1');
INSERT INTO `web_model_fields` VALUES ('42', 'site', '站点', '9', 'select_from_model', '0', 'site|site_name', '0', '0', 'required', '', '0', '1', '0', '1');
INSERT INTO `web_model_fields` VALUES ('43', 'tags', '定义名', '9', 'input', '0', '', '0', '0', 'required', '', '0', '1', '1', '1');
INSERT INTO `web_model_fields` VALUES ('44', 'article_cate', '文章分类', '9', 'select_from_model', '0', 'category|category_name', '0', '0', 'required', '', '0', '1', '2', '1');
INSERT INTO `web_model_fields` VALUES ('45', 'max_length', '最大显示条数', '9', 'input', '0', '', '0', '0', 'required', '', '0', '1', '3', '1');
INSERT INTO `web_model_fields` VALUES ('46', 'config_type', 'config', '9', 'select_from_model', '0', 'config_cate|config_cate_name', '0', '0', 'required', '', '0', '1', '4', '1');
INSERT INTO `web_model_fields` VALUES ('47', 'title', 'banner标题', '10', 'input', '0', '', '0', '0', 'required', '', '0', '1', '0', '1');
INSERT INTO `web_model_fields` VALUES ('48', 'url', '链接', '10', 'input', '0', '', '0', '0', 'required', '', '0', '1', '1', '1');
INSERT INTO `web_model_fields` VALUES ('49', 'banner', '图片', '10', 'file', '0', '', '0', '0', '', '', '0', '0', '2', '1');
INSERT INTO `web_model_fields` VALUES ('50', 'site', '站点', '10', 'select_from_model', '0', 'site|site_name', '0', '0', 'required', '', '0', '1', '3', '1');
INSERT INTO `web_model_fields` VALUES ('51', 'email', '邮箱', '8', 'input', '0', '', '0', '0', 'required', '', '1', '1', '7', '1');

-- ----------------------------
-- Table structure for `web_plugins`
-- ----------------------------
DROP TABLE IF EXISTS `web_plugins`;
CREATE TABLE `web_plugins` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `version` varchar(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `author` varchar(20) NOT NULL,
  `link` varchar(100) NOT NULL,
  `copyrights` varchar(100) NOT NULL,
  `access` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_plugins
-- ----------------------------

-- ----------------------------
-- Table structure for `web_rights`
-- ----------------------------
DROP TABLE IF EXISTS `web_rights`;
CREATE TABLE `web_rights` (
  `right_id` tinyint(10) unsigned NOT NULL AUTO_INCREMENT,
  `right_name` varchar(30) DEFAULT NULL,
  `right_class` varchar(30) DEFAULT NULL,
  `right_method` varchar(30) DEFAULT NULL,
  `right_detail` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`right_id`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_rights
-- ----------------------------
INSERT INTO `web_rights` VALUES ('1', '密码修改', 'system', 'password', null);
INSERT INTO `web_rights` VALUES ('2', '更新缓存', 'system', 'cache', null);
INSERT INTO `web_rights` VALUES ('3', ' 站点设置', 'setting', 'site', null);
INSERT INTO `web_rights` VALUES ('4', '后台设置', 'setting', 'backend', null);
INSERT INTO `web_rights` VALUES ('5', '插件管理[列表]', 'plugin', 'view', null);
INSERT INTO `web_rights` VALUES ('6', '添加插件', 'plugin', 'add', null);
INSERT INTO `web_rights` VALUES ('7', '修改插件', 'plugin', 'edit', null);
INSERT INTO `web_rights` VALUES ('8', '卸载插件', 'plugin', 'del', null);
INSERT INTO `web_rights` VALUES ('9', '导出插件', 'plugin', 'export', null);
INSERT INTO `web_rights` VALUES ('10', '导入插件', 'plugin', 'import', null);
INSERT INTO `web_rights` VALUES ('11', '激活插件', 'plugin', 'active', null);
INSERT INTO `web_rights` VALUES ('12', '禁用插件', 'plugin', 'deactive', null);
INSERT INTO `web_rights` VALUES ('13', '运行插件', 'module', 'run', null);
INSERT INTO `web_rights` VALUES ('14', '内容模型管理[列表]', 'model', 'view', null);
INSERT INTO `web_rights` VALUES ('15', '添加内容模型', 'model', 'add', null);
INSERT INTO `web_rights` VALUES ('16', '修改内容模型', 'model', 'edit', null);
INSERT INTO `web_rights` VALUES ('17', '删除内容模型', 'model', 'del', null);
INSERT INTO `web_rights` VALUES ('18', '内容模型字段管理[列表]', 'model', 'fields', null);
INSERT INTO `web_rights` VALUES ('19', '添加内容模型字段', 'model', 'add_filed', null);
INSERT INTO `web_rights` VALUES ('20', '修改内容模型字段', 'model', 'edit_field', null);
INSERT INTO `web_rights` VALUES ('21', '删除内容模型字段', 'model', 'del_field', null);
INSERT INTO `web_rights` VALUES ('22', '分类模型管理[列表]', 'category', 'view', null);
INSERT INTO `web_rights` VALUES ('23', '添加分类模型', 'category', 'add', null);
INSERT INTO `web_rights` VALUES ('24', '修改分类模型', 'category', 'edit', null);
INSERT INTO `web_rights` VALUES ('25', '删除分类模型', 'category', 'del', null);
INSERT INTO `web_rights` VALUES ('26', '分类模型字段管理[列表]', 'category', 'fields', null);
INSERT INTO `web_rights` VALUES ('27', '添加分类模型字段', 'category', 'add_filed', null);
INSERT INTO `web_rights` VALUES ('28', '修改分类模型字段', 'category', 'edit_field', null);
INSERT INTO `web_rights` VALUES ('29', '删除分类模型字段', 'category', 'del_field', null);
INSERT INTO `web_rights` VALUES ('30', '内容管理[列表]', 'content', 'view', null);
INSERT INTO `web_rights` VALUES ('31', '添加内容[表单]', 'content', 'form', 'add');
INSERT INTO `web_rights` VALUES ('32', '修改内容[表单]', 'content', 'form', 'edit');
INSERT INTO `web_rights` VALUES ('33', '添加内容[动作]', 'content', 'save', 'add');
INSERT INTO `web_rights` VALUES ('34', '修改内容[动作]', 'content', 'save', 'edit');
INSERT INTO `web_rights` VALUES ('35', '删除内容', 'content', 'del', null);
INSERT INTO `web_rights` VALUES ('36', '分类管理[列表]', 'category_content', 'view', null);
INSERT INTO `web_rights` VALUES ('37', '添加分类[表单]', 'category_content', 'form', 'add');
INSERT INTO `web_rights` VALUES ('38', '修改分类[表单]', 'category_content', 'form', 'edit');
INSERT INTO `web_rights` VALUES ('39', '添加分类[动作]', 'category_content', 'save', 'add');
INSERT INTO `web_rights` VALUES ('40', '修改分类[动作]', 'category_content', 'save', 'edit');
INSERT INTO `web_rights` VALUES ('41', '删除分类', 'category_content', 'del', null);
INSERT INTO `web_rights` VALUES ('42', '用户组管理[列表]', 'role', 'view', null);
INSERT INTO `web_rights` VALUES ('43', '添加用户组', 'role', 'add', null);
INSERT INTO `web_rights` VALUES ('44', '修改用户组', 'role', 'edit', null);
INSERT INTO `web_rights` VALUES ('45', '删除用户组', 'role', 'del', null);
INSERT INTO `web_rights` VALUES ('46', '用户管理[列表]', 'user', 'view', null);
INSERT INTO `web_rights` VALUES ('47', '添加用户', 'user', 'add', null);
INSERT INTO `web_rights` VALUES ('48', '修改用户', 'user', 'edit', null);
INSERT INTO `web_rights` VALUES ('49', '删除用户', 'user', 'del', null);
INSERT INTO `web_rights` VALUES ('50', '数据库备份', 'database', 'index', null);
INSERT INTO `web_rights` VALUES ('51', '数据库还原', 'database', 'recover', null);
INSERT INTO `web_rights` VALUES ('52', '数据库优化', 'database', 'optimize', null);

-- ----------------------------
-- Table structure for `web_roles`
-- ----------------------------
DROP TABLE IF EXISTS `web_roles`;
CREATE TABLE `web_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `rights` varchar(255) NOT NULL,
  `models` varchar(255) NOT NULL,
  `category_models` varchar(255) NOT NULL,
  `plugins` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_roles
-- ----------------------------
INSERT INTO `web_roles` VALUES ('1', 'root', '', '', '', '');

-- ----------------------------
-- Table structure for `web_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `web_sessions`;
CREATE TABLE `web_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_sessions
-- ----------------------------
INSERT INTO `web_sessions` VALUES ('3888a198af19261bb82dff1df9c2f11b', '220.177.100.106', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 UBrowser/6.2.2850.262 Sa', '1505967022', '');
INSERT INTO `web_sessions` VALUES ('43535c2e89ab07ae85beba9e48f3d74a', '101.226.85.67', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '1494295654', '');
INSERT INTO `web_sessions` VALUES ('8715a464b26eb545a9ffc3f65130603f', '220.175.138.26', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko', '1494295645', '');
INSERT INTO `web_sessions` VALUES ('00fef0b470cb380290aaa5b60d6ffa96', '220.177.100.106', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.104 Safari/537.36 Core/1.53.', '1493114819', '');
INSERT INTO `web_sessions` VALUES ('b7519df7de173eb2792304b919fe6d52', '220.177.100.106', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.104 Safari/537.36 Core/1.53', '1493107400', 'a:3:{s:9:\"user_data\";s:0:\"\";s:20:\"throttles_superadmin\";i:2;s:3:\"uid\";s:1:\"1\";}');
INSERT INTO `web_sessions` VALUES ('c97663a65c4939d8362ab86445dbc125', '101.226.33.238', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.104 Safari/537.36 Core/1.53.', '1493107410', '');
INSERT INTO `web_sessions` VALUES ('452ae4a96c150c4a53a1b7ff24ccae4a', '180.153.81.212', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) 			Chrome/55.0.2883.95 Safari/537', '1493107412', '');
INSERT INTO `web_sessions` VALUES ('873b5a19a13d81d4cec0ce6ec5afd50d', '101.226.66.174', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.104 Safari/537.36 Core/1.53.', '1493107511', '');
INSERT INTO `web_sessions` VALUES ('0def39ffae543f172475968abb92fb64', '101.226.102.97', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '1493107513', '');
INSERT INTO `web_sessions` VALUES ('3e868df0bed9e48ac72b96364d667796', '180.163.1.46', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '1493107522', '');
INSERT INTO `web_sessions` VALUES ('590dc9617c3e51e779779219e5d2e091', '180.163.2.119', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '1493107524', '');
INSERT INTO `web_sessions` VALUES ('8c53f7374c33be3026ad091037f89daf', '101.226.68.213', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) 			Chrome/55.0.2883.95 Safari/537', '1493107524', '');
INSERT INTO `web_sessions` VALUES ('2fb527f8ddbb179d5cf3ea25c60143b0', '101.226.33.205', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Medi', '1493107532', '');
INSERT INTO `web_sessions` VALUES ('dd72799eadea5acb7ccb87782dfccfd4', '101.226.89.116', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.104 Safari/537.36 Core/1.53.', '1493107535', '');
INSERT INTO `web_sessions` VALUES ('d1b851717f55e5e6505ec51e6315ab8e', '140.207.185.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '1493107535', '');
INSERT INTO `web_sessions` VALUES ('51397da2eb13859b9bb35f787326e435', '101.226.33.206', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36', '1493107538', '');
INSERT INTO `web_sessions` VALUES ('2fd603851e1543b4485e265a40f2304e', '101.226.66.191', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36', '1493107548', '');

-- ----------------------------
-- Table structure for `web_site_settings`
-- ----------------------------
DROP TABLE IF EXISTS `web_site_settings`;
CREATE TABLE `web_site_settings` (
  `site_name` varchar(50) DEFAULT NULL,
  `site_domain` varchar(50) DEFAULT NULL,
  `site_logo` varchar(50) DEFAULT NULL,
  `site_icp` varchar(50) DEFAULT NULL,
  `site_terms` text,
  `site_stats` varchar(200) DEFAULT NULL,
  `site_footer` varchar(500) DEFAULT NULL,
  `site_status` tinyint(1) DEFAULT '1',
  `site_close_reason` varchar(200) DEFAULT NULL,
  `site_keyword` varchar(200) DEFAULT NULL,
  `site_description` varchar(200) DEFAULT NULL,
  `site_theme` varchar(20) DEFAULT NULL,
  `attachment_url` varchar(50) DEFAULT NULL,
  `attachment_dir` varchar(20) DEFAULT NULL,
  `attachment_type` varchar(50) DEFAULT NULL,
  `attachment_maxupload` varchar(20) DEFAULT NULL,
  `thumbs_preferences` varchar(500) DEFAULT '[]'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_site_settings
-- ----------------------------
INSERT INTO `web_site_settings` VALUES ('景德镇陶瓷大学站群管理系统', 'http://www.dilicms.com/', 'images/logo.png', '', '', '', '', '1', '网站维护升级中......', '', '', 'default', null, 'attachments', '*.jpg;*.gif;*.png;*.doc', '2097152', '[]');

-- ----------------------------
-- Table structure for `web_throttles`
-- ----------------------------
DROP TABLE IF EXISTS `web_throttles`;
CREATE TABLE `web_throttles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_throttles
-- ----------------------------

-- ----------------------------
-- Table structure for `web_u_c_category`
-- ----------------------------
DROP TABLE IF EXISTS `web_u_c_category`;
CREATE TABLE `web_u_c_category` (
  `classid` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` int(5) unsigned NOT NULL DEFAULT '0',
  `level` int(2) unsigned NOT NULL DEFAULT '1',
  `path` varchar(50) DEFAULT '',
  `category_name` varchar(100) NOT NULL DEFAULT '',
  `site` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`classid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_u_c_category
-- ----------------------------
INSERT INTO `web_u_c_category` VALUES ('1', '0', '1', '{0}', '学院概况', '1');
INSERT INTO `web_u_c_category` VALUES ('2', '1', '2', '{0},{1}', '学院简介', '1');
INSERT INTO `web_u_c_category` VALUES ('3', '1', '2', '{0},{1}', '现任领导', '1');
INSERT INTO `web_u_c_category` VALUES ('4', '0', '1', '{0}', '新闻通知', '1');
INSERT INTO `web_u_c_category` VALUES ('5', '4', '2', '{0},{4}', '本系新闻', '1');
INSERT INTO `web_u_c_category` VALUES ('6', '4', '2', '{0},{4}', '通知公告', '1');
INSERT INTO `web_u_c_category` VALUES ('7', '0', '1', '', '工作动态', '1');
INSERT INTO `web_u_c_category` VALUES ('9', '7', '2', '', '学生工作', '1');

-- ----------------------------
-- Table structure for `web_u_c_config_cate`
-- ----------------------------
DROP TABLE IF EXISTS `web_u_c_config_cate`;
CREATE TABLE `web_u_c_config_cate` (
  `classid` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` int(5) unsigned NOT NULL DEFAULT '0',
  `level` int(2) unsigned NOT NULL DEFAULT '1',
  `path` varchar(50) DEFAULT '',
  `config_cate_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_u_c_config_cate
-- ----------------------------
INSERT INTO `web_u_c_config_cate` VALUES ('1', '0', '1', '{0}', 'config_index');
INSERT INTO `web_u_c_config_cate` VALUES ('2', '0', '1', '{0}', 'config_list');

-- ----------------------------
-- Table structure for `web_u_c_link_cate`
-- ----------------------------
DROP TABLE IF EXISTS `web_u_c_link_cate`;
CREATE TABLE `web_u_c_link_cate` (
  `classid` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` int(5) unsigned NOT NULL DEFAULT '0',
  `level` int(2) unsigned NOT NULL DEFAULT '1',
  `path` varchar(50) DEFAULT '',
  `link_cate_name` varchar(100) NOT NULL DEFAULT '',
  `site` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`classid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_u_c_link_cate
-- ----------------------------
INSERT INTO `web_u_c_link_cate` VALUES ('1', '0', '1', '{0}', '快速访问', '1');
INSERT INTO `web_u_c_link_cate` VALUES ('2', '0', '1', '{0}', '合作单位', '1');
INSERT INTO `web_u_c_link_cate` VALUES ('3', '0', '1', '{0}', '友情链接', '1');

-- ----------------------------
-- Table structure for `web_u_c_site`
-- ----------------------------
DROP TABLE IF EXISTS `web_u_c_site`;
CREATE TABLE `web_u_c_site` (
  `classid` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` int(5) unsigned NOT NULL DEFAULT '0',
  `level` int(2) unsigned NOT NULL DEFAULT '1',
  `path` varchar(50) DEFAULT '',
  `site_name` varchar(100) NOT NULL DEFAULT '',
  `site_url` varchar(100) NOT NULL DEFAULT '',
  `site_logo` varchar(100) NOT NULL DEFAULT '',
  `site_copy` text NOT NULL,
  `temp` int(10) NOT NULL DEFAULT '0',
  `user` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`classid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_u_c_site
-- ----------------------------
INSERT INTO `web_u_c_site` VALUES ('1', '0', '1', '{0}', '信息工程学院', 'xxgcxy', '/2016/09/1474938953dfc1ac01852bc2c4.png', '<div style=\"text-align:center;\">\r\n	<span style=\"font-family:宋体, Arial;font-size:14px;\">Copyright &copy; 2016 <a href=\"http://mia.pasp.cn/\" target=\"_blank\">景德镇陶瓷大学移动互联协会.</a> </span><span style=\"font-family:宋体, Arial;font-size:14px;\"></span> \r\n</div>\r\n<div style=\"text-align:center;\">\r\n	<span style=\"font-family:宋体, Arial;font-size:14px;\">地址：江西省景德镇市浮梁县湘湖镇　</span><span style=\"font-family:宋体, Arial;font-size:14px;\"></span><span style=\"font-family:宋体, Arial;font-size:14px;\">邮政编码：330043　</span> \r\n</div>', '2', '1');
INSERT INTO `web_u_c_site` VALUES ('2', '0', '1', '{0}', '机械电子工程学院', 'jdxy', '/2016/09/1474938800861e3690d4f4948d.png', '<span style=\"font-family:宋体, Arial;font-size:14px;line-height:20px;\">Copyright &copy; 2016 景德镇陶瓷大学移动互联协会. All Rights Reserved</span><br />\r\n<span style=\"font-family:宋体, Arial;font-size:14px;line-height:20px;\">地址：江西省景德镇市浮梁县湘湖镇　　邮政编码：330043　</span>', '1', '2');
INSERT INTO `web_u_c_site` VALUES ('3', '0', '1', '', '体育教育学院', 'tyxy', '//uploads/148342567974635885.png', '<p>Copyright © 2016 移动互联协会</p><p>地址：江西省景德镇市浮梁县湘湖镇　邮政编码：330043　</p><p></p>', '2', '3');

-- ----------------------------
-- Table structure for `web_u_c_templates`
-- ----------------------------
DROP TABLE IF EXISTS `web_u_c_templates`;
CREATE TABLE `web_u_c_templates` (
  `classid` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` int(5) unsigned NOT NULL DEFAULT '0',
  `level` int(2) unsigned NOT NULL DEFAULT '1',
  `path` varchar(50) DEFAULT '',
  `template_name` varchar(100) NOT NULL DEFAULT '',
  `index` text NOT NULL,
  `common_top` text NOT NULL,
  `common_foot` text NOT NULL,
  `list` text NOT NULL,
  `content` text CHARACTER SET utf8mb4 NOT NULL,
  `msg_board` text CHARACTER SET utf8mb4 NOT NULL,
  `search` text NOT NULL,
  PRIMARY KEY (`classid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_u_c_templates
-- ----------------------------
INSERT INTO `web_u_c_templates` VALUES ('1', '0', '1', '{0}', '院校模板', '<div class=\"banner\" id=\"banner\"></div>\r\n		<script type=\"text/javascript\">\r\n				var bannerSource=[\r\n						@@Banner@@\r\n				];\r\n				var bannerObj=$(\'#banner\'),tmpBc=\"\",bannerCount=0,bannerLen=bannerSource.length;\r\n				for(var bI=0;bI<bannerLen;bI++){\r\n						tmpBc+=\'<a href=\"\'+bannerSource[bI].url+\'\" target=\"blank\"><img src=\"\'+bannerSource[bI].pic+\'\" alt=\"\'+bannerSource[bI].txt+\'\" title=\"\'+bannerSource[bI].txt+\'\" /></a>\';\r\n				}\r\n				bannerObj.html(tmpBc);\r\n				var bannerImgObj=$(\'#banner img\');\r\n				var bannerShow=function(index){\r\n						bannerImgObj.not(\":eq(\"+index+\")\").hide(\'fast\');\r\n						bannerImgObj.eq(index).fadeIn(\'fast\');\r\n				}\r\n				var bannerRun=function(){\r\n						if(bannerCount==bannerLen){\r\n								bannerCount=0;\r\n						};\r\n						bannerShow(bannerCount);\r\n						bannerCount++;\r\n						setTimeout(bannerRun,10000);\r\n				}\r\n				bannerRun();\r\n		</script>\r\n		<div class=\"main clr\">\r\n				<div class=\"focus\">\r\n						<!--焦点图容器-开始-->\r\n						<div id=\"focus\">\r\n								<div id=\"focuspic\"></div>\r\n								<div id=\"focustitle\"></div>\r\n								<div id=\"focusnav\"></div>\r\n						</div>\r\n						<!--焦点图容器-结束-->\r\n						<script type=\"text/javascript\">\r\n						//定义焦点图的资源\r\n						var picArray=[\r\n								@@焦点图@@\r\n						];var len = picArray.length;\r\n						var titleArray=[@@焦点图标题@@];\r\n						var linkArray=[@@焦点图链接@@];\r\n\r\n						//设定焦点图参数\r\n						var para={\r\n								focusAreaBox:\"focus\",//焦点图大容器ID\r\n								focusNavBox:\"focusnav\",//焦点图数字导航容器ID\r\n								focusPicBox:\"focuspic\",//焦点图图片容器ID\r\n								//focusTitleBox:\"focustitle\",//焦点图标题容器ID\r\n								focusPics:picArray,//焦点图图片数组\r\n								focusTitle:titleArray,//焦点图标题数组\r\n								focusLinks:linkArray,//焦点图链接数组\r\n								focusNums:len ,//需要显示的焦点图个数\r\n								focusSpeed:5000,//焦点图自动切换的时间（单位毫秒）\r\n								focusWidth:\"365px\",//焦点图宽度\r\n								focusHeight:\"300px\"//焦点图高度\r\n						};\r\n						//建立焦点图对象\r\n						var focusObj=new Focus(para)\r\n						//运行焦点图\r\n						focusObj.run();\r\n				</script>\r\n				</div>\r\n				<div class=\"newsMoudle\">\r\n						<div class=\"newNav clr\">\r\n								<div class=\"tab current\">本系新闻</div>\r\n								<div class=\"tab\">通知公告</div>\r\n								<div id=\"dTime\"></div>\r\n						</div>\r\n						<ul class=\"newsUl news\" id=\"news\">\r\n@@新闻@@\r\n								<!--li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]我院受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li-->\r\n						</ul>\r\n						<ul class=\"newsUl notice\" id=\"notice\">\r\n@@通知@@\r\n								<!--li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li>\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">[图]通知受邀参加威漫网“爱无疆界”公益漫画征集活动启动仪式</span><span class=\"newsTime\">[2013-12-12]</span></a></li-->\r\n						</ul>\r\n				</div>\r\n		</div>', '<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\"gb2312\">\r\n<title>@@网站名称@@</title>\r\n<link href=\"../templates/temp1/style/global.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"../templates/temp1/style/index.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"../templates/temp1/style/subpage.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<script type=\"text/javascript\" src=\"../templates/temp1/script/jquery.js\"></script>\r\n<script type=\"text/javascript\" src=\"../templates/temp1/script/jQuery.imgFloat.js\"></script>\r\n<script type=\"text/javascript\" src=\"../templates/temp1/script/jQuery.focus.1.0.min.js\"></script>\r\n<script type=\"text/javascript\" src=\"../templates/temp1/script/ctd.js\"></script>\r\n<style type=\"text/css\">\r\n/*漂浮图片start*/\r\n.imgFloat{width:250px;height:100px;position:absolute;background-image:url(template/images/imgFloat.jpg); z-index:999;}\r\n#java{background-position:0 -100px;}\r\n/*漂浮图片end*/\r\n</style>\r\n</head>\r\n<body>\r\n<!--漂浮图片start-->\r\n<!--div id=\"iphone\" class=\"imgFloat\" title=\"iPhone手机开发工程师方向·深圳软件行业协会订单班\"></div>\r\n<div id=\"java\" class=\"imgFloat\" title=\"Java软件外包方向·中软国际定向班\"></div-->\r\n<script type=\"text/javascript\">\r\n/*\r\n$(\"#iphone\").imgFloat({speed:40,xPos:10,yPos:1000});\r\n$(\"#iphone\").click(function (){window.open(\"#\");});\r\n$(\"#java\").imgFloat({speed:40,xPos:10,yPos:10});\r\n$(\"#java\").click(function (){window.open(\"#\");});\r\n*/\r\n</script>\r\n<!--漂浮图片end-->\r\n<div class=\"topBg\"></div>\r\n<div class=\"warp\">\r\n		<div class=\"top clr\">\r\n				<div id=\"logo\" title=\"返回首页\"><img src = \"../attachments/@@Logo@@\"></div>\r\n				<div class=\"searchArea\"> <a href=\"@@首页链接@@\">首页</a> <a href=\"#\" onclick=\"SetHome(this,window.location)\">设为首页</a> <a  href=\"#\" onclick=\"AddFavorite(window.location,document.title)\" >收藏本站</a> <a href=\"#\"><span class=\"enEditon\">English</span></a>\r\n						<form action=\"@@搜索@@\" method=\"post\">\r\n								<input id=\"keywords\" type=\"text\" name=\"keywords\" placeholder=\"请输入关键字\" speech x-webkit-speech class=\"inputArea\" />\r\n								<input name=\"site\" type=\"hidden\" value=\"@@站点@@\" />\r\n                                                                <input type=\"submit\" value=\"搜索\" />\r\n						</form>\r\n				</div>\r\n		</div>\r\n		<div class=\"nav\">\r\n				<div class=\"navBg navLeft\"></div>\r\n				<ul class=\"navUl clr\">\r\n						<li class=\"navLi\"><a href=\"@@Site_index@@\">首　　页</a></li>\r\n						@@一级栏目@@\r\n				</ul>\r\n				<div class=\"navBg navRight\"></div>\r\n		</div>\r\n		<div class=\"subNav\">\r\n				<div id=\"subNavItems\">\r\n						<div class=\"subNavItem\">欢迎来到@@网站名称@@^_^</div>\r\n						@@二级栏目@@\r\n				</div>\r\n		</div>', '<div class=\"footNav\">\r\n				<div class=\"footNavBg footNavLeft\"></div>\r\n				<div class=\"quickLinks\"> 快速访问： @@链接@@...... </div>\r\n				<div class=\"cooperation\">合作单位：\r\n						<select name=\"copr\">\r\n								@@单位@@\r\n						</select>\r\n				</div>\r\n				<div class=\"footNavBg footNavRight\"></div>\r\n		</div>\r\n		<div class=\"copy\">@@copyRight@@</div>\r\n</div>\r\n</body>\r\n<script>\r\n		function AddFavorite(sURL, sTitle){\r\n		    try{\r\n		        window.external.addFavorite(sURL, sTitle);\r\n		    }\r\n		    catch (e){\r\n		        try{\r\n		            window.sidebar.addPanel(sTitle, sURL, \"\");\r\n		        }\r\n		        catch (e){\r\n		            alert(\"加入收藏失败，请使用Ctrl+D进行添加\");\r\n		        }\r\n		    }\r\n		}\r\n		//设为首页 <a onclick=\"SetHome(this,window.location)\">设为首页</a>\r\n		function SetHome(obj,vrl){\r\n	        try{\r\n	            obj.style.behavior=\'url(#default#homepage)\';obj.setHomePage(vrl);\r\n	        }\r\n	        catch(e){\r\n	            if(window.netscape) {\r\n	                try {\r\n	                    netscape.security.PrivilegeManager.enablePrivilege(\"UniversalXPConnect\");\r\n	                }\r\n	                catch (e) {\r\n	                    alert(\"此操作被浏览器拒绝！\\n请在浏览器地址栏输入“about:config”并回车\\n然后将 [signed.applets.codebase_principal_support]的值设置为\'true\',双击即可。\");\r\n	                }\r\n	                var prefs = Components.classes[\'@mozilla.org/preferences-service;1\'].getService(Components.interfaces.nsIPrefBranch);\r\n	                 prefs.setCharPref(\'browser.startup.homepage\',vrl);\r\n	            }\r\n	        }\r\n		}\r\n	</script>\r\n</html>', '<div class=\"mainList clr\">\r\n				<div class=\"subPageMenu\">\r\n						<div class=\"subPageMenuTit\">\r\n								<span class=\"cnTxt\">@@栏目名1@@</span>\r\n						</div>\r\n						<div class=\"subPageMenuList\">\r\n								<ul class=\"subPageMenuUl\">\r\n\r\n										<!--li class=\"subPageMenuLi\"><a href=\"#\">本系概括</a></li>\r\n										<li class=\"subPageMenuLi\"><a href=\"#\">现任领导</a></li>\r\n										<li class=\"subPageMenuLi\"><a href=\"#\">所获荣誉</a></li>\r\n										<li class=\"subPageMenuLi\"><a href=\"#\">校企合作</a></li>\r\n										<li class=\"subPageMenuLi\"><a href=\"#\">学生获奖</a></li>\r\n										<li class=\"subPageMenuLi\"><a href=\"#\">优秀毕业生</a></li-->\r\n                                                                              @@侧边导航@@\r\n								</ul>\r\n						</div>\r\n						<div class=\"links\">\r\n								<select name=\"links\">\r\n										@@友情链接@@\r\n								</select>\r\n						</div>\r\n				</div>\r\n				<div class=\"subPageContentArea\">\r\n						<div class=\"subPagePath\">您现在所在的位置是：<a href=\"@@首页链接@@\">首页</a> > <a href=\"@@栏目链接@@\">@@栏目名1@@</a> > @@栏目名2@@</div>\r\n						<div class=\"subPageContent\"><ul>@@列表页@@</ul></div>\r\n                                               <div class=\"pageDown\">@@翻页@@</div>\r\n				</div>\r\n		</div>', '<div class=\"mainList clr\">\r\n				<div class=\"subPageMenu\">\r\n						<div class=\"subPageMenuTit\">\r\n								<span class=\"cnTxt\">@@栏目名1@@</span>\r\n						</div>\r\n						<div class=\"subPageMenuList\">\r\n								<ul class=\"subPageMenuUl\">\r\n										<!--li class=\"subPageMenuLi\"><a href=\"#\">本系概括</a></li>\r\n										<li class=\"subPageMenuLi\"><a href=\"#\">现任领导</a></li>\r\n										<li class=\"subPageMenuLi\"><a href=\"#\">所获荣誉</a></li>\r\n										<li class=\"subPageMenuLi\"><a href=\"#\">校企合作</a></li>\r\n										<li class=\"subPageMenuLi\"><a href=\"#\">学生获奖</a></li>\r\n										<li class=\"subPageMenuLi\"><a href=\"#\">优秀毕业生</a></li-->\r\n                                                                        @@侧边导航@@\r\n								</ul>\r\n						</div>\r\n						<div class=\"links\">\r\n								<select name=\"links\">\r\n										@@友情链接@@\r\n								</select>\r\n						</div>\r\n				</div>\r\n				<div class=\"subPageContentArea\">\r\n						<div class=\"subPagePath\">您现在所在的位置是：<a href=\"@@首页链接@@\">首页</a> > <a href=\"@@栏目链接@@\">@@栏目名1@@</a> > @@栏目名2@@</div>\r\n						<div class=\"subPageContent\"><p style=\"text-align:center;font-size:18px;\">@@文章标题@@</p><p style=\"text-align:center;font-size:12px;\">@@文章详细@@</p>@@文章内容@@</div>\r\n				</div>\r\n		</div>', '<div class=\'mainList clr\'>\r\n				<div class=\'subPageMenu\'>\r\n						<div class=\'subPageMenuTit\'>\r\n								<span class=\'cnTxt\'>留言</span>\r\n								<span class=\'enTxt\'>Message</span>\r\n						</div>\r\n				</div>\r\n				<div class=\'subPageContentArea\'>\r\n						<div class=\'subPagePath\'><span>请写下您的疑问</span></div>\r\n						<div class=\'subPageContent\'>\r\n							<form action=\'@@留言@@\' method=\'post\' class=\'msg clr\'>\r\n								<p>标&nbsp;&nbsp;题:&nbsp;<input type=\'text\' name=\'title\' id=\'title\' value=\'\' /></p>\r\n								<p>e-mail&nbsp;:&nbsp;<input type=\'email\' name=\'lxfs\' id=\'lxfs\' value=\'\' /></p>\r\n								<span>内&nbsp;&nbsp;容:</span><textarea cols=\'40\' rows=\'10\' style=\"margin-left:20px\"></textarea>\r\n								<p style=\'text-align:center;\'><input type=\'submit\' value=\'提交\' /></p>\r\n							</form>\r\n						</div>\r\n				</div>\r\n		</div>', '<div class=\"mainList clr\">\r\n				<div class=\"subPageMenu\">\r\n						<div class=\"subPageMenuTit\">\r\n								<span class=\"cnTxt\">搜索</span>\r\n								<span class=\"enTxt\">Search</span>\r\n						</div>\r\n				</div>\r\n				<div class=\"subPageContentArea\">\r\n						<div class=\"subPagePath\">您现在搜索的关键字是：<a href=\"#\">@@关键字@@</a> <span>共有@@数量@@条</span></div>\r\n						<div class=\"subPageContent\">\r\n							<ul>\r\n<!--\r\n								<li class=\"newsLi clr\"><a href=\"#\"><span class=\"newsTxt\">测试base_url1</span><span class=\"newsTime\">2016-09-22</span></a></li>\r\n-->\r\n                                                                     @@搜索列表@@\r\n							</ul>\r\n						</div>\r\n                                     <div class=\"pageDown\">@@翻页@@</div>\r\n				</div>\r\n		</div>');
INSERT INTO `web_u_c_templates` VALUES ('2', '0', '1', '{0}', '院校模板1', '<!--中间-->\r\n	<div class=\"content\">\r\n		<!--图片轮播区域-->\r\n		<div id=\"rotator\">\r\n		<script type=text/javascript>\r\n			jQuery(function($) {$(document).ready(function() {\r\n				$(\'#rotator\').crossSlide(\r\n					{sleep: 2, fade: 1, debug: true},\r\n					[\r\n						@@Banner@@\r\n					]\r\n				);\r\n				});\r\n			});\r\n		</script>\r\n		</div>\r\n		<!--图片轮播区域-->\r\n		<div class=\"box\">\r\n			<!--中间上面-->\r\n			<div class=\"box-1 clear\">\r\n				<!--通知公告-->\r\n				<div class=\"Inform\">\r\n					<div class=\"Ititle clear\">\r\n						<span class=\"notice fl\">通知公告</span>\r\n						<span class=\"more fr\"><a href=\"#\"><img src=\"../templates/temp2/img/more.gif\" /></a></span>\r\n					</div>\r\n					<div class=\"Icontent\">\r\n						<ul>\r\n@@新闻@@\r\n						</ul>\r\n					</div>\r\n				</div>\r\n				<!--通知公告-->\r\n				<!--图片新闻-->\r\n				<div class=\"flash\">\r\n					<div class=\"mov\">\r\n						<!--焦点图容器-开始-->\r\n						<div id=\"focus\">\r\n								<div id=\"focuspic\"></div>\r\n								<div id=\"focustitle\"></div>\r\n								<div id=\"focusnav\"></div>\r\n						</div>\r\n						<!--焦点图容器-结束-->\r\n						<script type=\"text/javascript\">\r\n						//定义焦点图的资源\r\n						var picArray=[\r\n								@@焦点图@@\r\n						];\r\nvar len = picArray.length;\r\n						var titleArray=[@@焦点图标题@@];\r\n						var linkArray=[@@焦点图链接@@];\r\n						//设定焦点图参数\r\n						var para={\r\n								focusAreaBox:\"focus\",//焦点图大容器ID\r\n								focusNavBox:\"focusnav\",//焦点图数字导航容器ID\r\n								focusPicBox:\"focuspic\",//焦点图图片容器ID\r\n								//focusTitleBox:\"focustitle\",//焦点图标题容器ID\r\n								focusPics:picArray,//焦点图图片数组\r\n								focusTitle:titleArray,//焦点图标题数组\r\n								focusLinks:linkArray,//焦点图链接数组\r\n								focusNums:len ,//需要显示的焦点图个数\r\n								focusSpeed:5000,//焦点图自动切换的时间（单位毫秒）\r\n								focusWidth:\"375px\",//焦点图宽度\r\n								focusHeight:\"330px\"//焦点图高度\r\n						};\r\n						//建立焦点图对象\r\n						var focusObj=new Focus(para)\r\n						//运行焦点图\r\n						focusObj.run();\r\n					</script>\r\n					</div>\r\n				</div>\r\n				<!--图片新闻-->\r\n				<!--新闻动态-->\r\n				<div class=\"iNews\">\r\n					<div class=\"Ntitle\">\r\n						<span class=\"notice fl\">新闻动态</span>\r\n						<span class=\"more fr\"><a href=\"#\"><img src=\"../templates/temp2/img/more.gif\" /></a></span>\r\n					</div>\r\n					<div class=\"Ncontent\">\r\n						<ul>\r\n@@通知@@\r\n              			</ul>\r\n					</div>\r\n				</div>\r\n				<!--新闻动态-->\r\n			</div>\r\n			<!--中间上面-->\r\n		</div>\r\n	</div>', '<!DOCTYPE html>\r\n<html>\r\n	<head>\r\n		<meta charset=\"utf-8\" />\r\n		<title>@@网站名称@@</title>\r\n		<!--中部-->\r\n		<link rel=\"stylesheet\" href=\"../templates/temp2/css/base_index.css\" />\r\n		<!--底部-->\r\n		<script type=\"text/javascript\" src=\"../templates/temp2/js/main.js\" ></script>\r\n		<script type=\"text/javascript\" src=\"../templates/temp2/js/jquery.cross-slide.js\" ></script>\r\n		<script type=\"text/javascript\" src=\"../templates/temp2/js/jQuery.focus.1.0.min.js\" ></script>\r\n		<style type=\"text/css\">\r\n			#rotator{\r\n				width: 1024px;\r\n				height:200px;\r\n				margin:10px auto;\r\n				z-index:-1;\r\n			}\r\n		</style>\r\n	</head>\r\n	<body>\r\n	<!--top start-->\r\n	<div class=\"top clr\">\r\n<div class=\"logo fl\">\r\n		<img src=\"../attachments/@@Logo@@\" height=\"100\">\r\n</div>\r\n<div class=\"searchArea fr\"> <a href=\"@@首页链接@@\">首页</a> <a href=\"#\" onclick=\"SetHome(this,window.location)\">设为首页</a> <a  href=\"#\" onclick=\"AddFavorite(window.location,document.title)\" >收藏本站</a> \r\n						<form action=\"@@搜索@@\" method=\"post\">\r\n								<input id=\"keywords\" type=\"text\" name=\"keywords\" placeholder=\"请输入关键字\" speech x-webkit-speech class=\"inputArea\" />\r\n								<input name=\"site\" type=\"hidden\" value=\"@@站点@@\" />\r\n                                                                <input type=\"submit\" value=\"搜索\" />\r\n						</form>\r\n				</div>\r\n	</div>\r\n	<div id=\"nav\" class=\"nav\">\r\n	    <ul class=\"sf-menu\">\r\n<li style=\"border-top-left-radius:10px;\"><a href=\"@@首页链接@@\" id=\"a1\"><span class=\"arrow\">首页</span></a></li>\r\n@@一级栏目@@\r\n		</ul>\r\n	</div>\r\n	<!--导航条区域-->\r\n	<script type=\"text/javascript\">    \r\n	    $(\"#nav .sf-menu\").supersubs().superfish();   \r\n	</script>\r\n	<!--top end-->', '<!--bottom start-->\r\n	<div class=\"links\">\r\n			<div class=\"lt clr\">\r\n				<span class=\"ft fl\">友情链接</span>\r\n			</div>\r\n			<div class=\"lcon\">\r\n			</div>\r\n		</div>\r\n		<div class=\"foot\">\r\n@@copyRight@@\r\n		</div>\r\n	<!--bottom end-->\r\n	</body>\r\n<script>\r\n		function AddFavorite(sURL, sTitle){\r\n		    try{\r\n		        window.external.addFavorite(sURL, sTitle);\r\n		    }\r\n		    catch (e){\r\n		        try{\r\n		            window.sidebar.addPanel(sTitle, sURL, \"\");\r\n		        }\r\n		        catch (e){\r\n		            alert(\"加入收藏失败，请使用Ctrl+D进行添加\");\r\n		        }\r\n		    }\r\n		}\r\n		//设为首页 <a onclick=\"SetHome(this,window.location)\">设为首页</a>\r\n		function SetHome(obj,vrl){\r\n	        try{\r\n	            obj.style.behavior=\'url(#default#homepage)\';obj.setHomePage(vrl);\r\n	        }\r\n	        catch(e){\r\n	            if(window.netscape) {\r\n	                try {\r\n	                    netscape.security.PrivilegeManager.enablePrivilege(\"UniversalXPConnect\");\r\n	                }\r\n	                catch (e) {\r\n	                    alert(\"此操作被浏览器拒绝！\\n请在浏览器地址栏输入“about:config”并回车\\n然后将 [signed.applets.codebase_principal_support]的值设置为\'true\',双击即可。\");\r\n	                }\r\n	                var prefs = Components.classes[\'@mozilla.org/preferences-service;1\'].getService(Components.interfaces.nsIPrefBranch);\r\n	                 prefs.setCharPref(\'browser.startup.homepage\',vrl);\r\n	            }\r\n	        }\r\n		}\r\n	</script>\r\n</html>', '<div class=\"box clr\">\r\n		<!--left-->\r\n		<div class=\"left\">\r\n			<div id=\"lnavtop\"></div>\r\n			<div class=\"navleft\">\r\n				<div id=\"navtitle\">@@栏目名1@@</div>\r\n	            <div class=\"navbody\">\r\n	            	<ul>\r\n@@侧边导航@@\r\n	                </ul>\r\n	            </div>\r\n			</div>\r\n			<div id=\"lnavbottom\">\r\n			</div>\r\n		</div>\r\n		<!--left-->\r\n		<!--right-->\r\n		<div class=\"right\">\r\n			<div class=\"rtitle\">\r\n				<ul>\r\n	            	<li style=\"width:100px; height:38px; text-align:center;	background-image:url(../templates/temp2/img/dt-5.jpg);\">@@栏目名2@@</li>\r\n	                <li style=\"width:614px; height:37px; text-align:right; background-image:url(../templates/temp2/img/dt-6.jpg); padding:0 10px 0 0\">当前位置：<a href=\"@@首页链接@@\">网站首页</a> >> <a href=\'@@栏目链接@@\'>@@栏目名1@@</a> </li>\r\n            	</ul>\r\n			</div>\r\n			<div class=\"rbody\">\r\n				<div class=\"rbodyin\">\r\n					<ul>\r\n	                	<li style=\"font-size:16px; font-weight:bold;text-align:center;\"></li>\r\n	                    <li><hr /></li>\r\n	                    @@列表页@@\r\n	                    <li><hr /></li>\r\n	                    <li style=\"margin-top:5px;\"> @@翻页@@</li>\r\n                	</ul>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<!--right-->\r\n	</div>', '<div class=\"box clr\">\r\n		<!--left-->\r\n		<div class=\"left\">\r\n			<div id=\"lnavtop\"></div>\r\n			<div class=\"navleft\">\r\n				<div id=\"navtitle\">@@栏目名1@@</div>\r\n	            <div class=\"navbody\">\r\n	            	<ul>\r\n@@侧边导航@@\r\n	                </ul>\r\n	            </div>\r\n			</div>\r\n			<div id=\"lnavbottom\">\r\n			</div>\r\n		</div>\r\n		<!--left-->\r\n		<!--right-->\r\n		<div class=\"right\">\r\n			<div class=\"rtitle\">\r\n				<ul>\r\n	            	<li style=\"width:100px; height:38px; text-align:center;	background-image:url(../templates/temp2/img/dt-5.jpg);\">@@栏目名2@@</li>\r\n	                <li style=\"width:614px; height:37px; text-align:right; background-image:url(../templates/temp2/img/dt-6.jpg); padding:0 10px 0 0\">当前位置：<a href=\"@@首页链接@@\">网站首页</a> >> <a href=\'@@栏目链接@@\'>@@栏目名1@@</a> </li>\r\n            	</ul>\r\n			</div>\r\n			<div class=\"rbody\">\r\n				<div class=\"rbodyin\">\r\n					<ul>\r\n	                	<li style=\"font-size:16px; font-weight:bold;text-align:center;\">@@文章标题@@</li>\r\n	                    <li style=\"font-size:12px; color:#999; margin:10px auto; text-align:center;\"> @@文章详细@@</li>\r\n	                    <li><hr style=\"width:715px; border-style:dashed; color:#999; margin:5px auto;\" /></li>\r\n	                    <div class=\"contents\">\r\n@@文章内容@@\r\n	                    </div>\r\n	                    <li><hr style=\"width:715px; border-style:dashed; color:#999; margin:5px auto;\" /></li>\r\n                	</ul>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<!--right-->\r\n	</div>', '<div class=\"box clr\">\r\n		<!--left-->\r\n		<div class=\"left\">\r\n			<div id=\"lnavtop\"></div>\r\n			<div class=\"navleft\">\r\n				<div id=\"navtitle\">留言</div>\r\n	            <div class=\"navbody\">\r\n	            	<ul>\r\n	            		<li>在这里可以写下您的意见</li>\r\n	                </ul>\r\n	            </div>\r\n			</div>\r\n			<div id=\"lnavbottom\">\r\n			</div>\r\n		</div>\r\n		<!--left-->\r\n		<!--right-->\r\n		<div class=\"right\">\r\n			<div class=\"rtitle\">\r\n				<ul>\r\n	            	<li style=\"width:100px; height:38px; text-align:center;	background-image:url(../templates/temp2/img/dt-5.jpg);\">留言</li>\r\n	                <li style=\"width:614px; height:37px; text-align:right; background-image:url(../templates/temp2/img/dt-6.jpg); padding:0 10px 0 0\">当前位置：<a href=\"index.asp\">网站首页</a> >> <a href=\'newslist.asp?lm=1\'>留言</a> </li>\r\n            	</ul>\r\n			</div>\r\n			<div class=\"rbody\">\r\n				<div class=\"rbodyin\">\r\n					<ul>\r\n	                	<li style=\"font-size:16px; font-weight:bold;text-align:center;\">留言</li>\r\n	                    <li><hr /></li>\r\n							<div class=\'subPageContent\'>\r\n								<form action=\'@@留言@@\' method=\'post\' class=\'msg clr\'>\r\n									<p>标&nbsp;&nbsp;题:&nbsp;&nbsp;&nbsp;<input type=\'text\' name=\'title\' id=\'title\' value=\'\' /></p>\r\n									<input type=\"hidden\" name=\"site\" value=\"@@站点ID@@\">\r\n									<input type=\"hidden\" name=\"site_url\" value=\"@@站点URL@@\">\r\n									<p>e-mail&nbsp;:&nbsp;<input type=\'email\' name=\'lxfs\' id=\'lxfs\' value=\'\' /></p>\r\n									<p><div class=\"ta clr\"><span>内容:</span><textarea cols=\'40\' rows=\'10\' name=\"content\"  style=\"margin-left:20px\"></textarea></div></p>\r\n									<p><input type=\'submit\' value=\'提交\' /></p>\r\n								</form>\r\n							</div>\r\n	                    <li><hr /></li>\r\n                	</ul>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<!--right-->\r\n	</div>', '<!--top end-->\r\n	<div class=\"box clr\">\r\n		<!--left-->\r\n		<div class=\"left\">\r\n			<div id=\"lnavtop\"></div>\r\n			<div class=\"navleft\">\r\n				<div id=\"navtitle\">搜索</div>\r\n	            <div class=\"navbody\">\r\n	            	<ul>\r\n	                	<li>您搜索的(@@关键字@@)</li>\r\n						<li>结果共有@@数量@@条</li>\r\n	                </ul>\r\n	            </div>\r\n			</div>\r\n			<div id=\"lnavbottom\">\r\n			</div>\r\n		</div>\r\n		<!--left-->\r\n		<!--right-->\r\n		<div class=\"right\">\r\n			<div class=\"rtitle\">\r\n				<ul>\r\n	            	<li style=\"width:100px; height:38px; text-align:center;	background-image:url(../templates/temp2/img/dt-5.jpg);\">搜索</li>\r\n	                <li style=\"width:614px; height:37px; text-align:right; background-image:url(../templates/temp2/img/dt-6.jpg); padding:0 10px 0 0\">当前位置：<a href=\"@@首页链接@@\">网站首页</a> >> <a href=\'#\'>搜索</a> </li>\r\n            	</ul>\r\n			</div>\r\n			<div class=\"rbody\">\r\n				<div class=\"rbodyin\">\r\n					<ul>\r\n	                	<li style=\"font-size:16px; font-weight:bold;text-align:center;\"></li>\r\n	                    <li><hr /></li>\r\n@@搜索列表@@\r\n	                    <li><hr /></li>\r\n	                    <li style=\"margin-top:5px;\"> @@翻页@@</li>\r\n                	</ul>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<!--right-->\r\n	</div>');

-- ----------------------------
-- Table structure for `web_u_c_user`
-- ----------------------------
DROP TABLE IF EXISTS `web_u_c_user`;
CREATE TABLE `web_u_c_user` (
  `classid` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` int(5) unsigned DEFAULT '0',
  `level` int(2) unsigned DEFAULT '1',
  `path` varchar(50) DEFAULT '',
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `add_time` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_u_c_user
-- ----------------------------
INSERT INTO `web_u_c_user` VALUES ('1', '0', '1', '', 'xxgcxy', 'EBrVhdHmg65GsN9ziZxVH3eQM6X0P8nMqWbR9xd1SC+UYkuizms52Um6DOPsRsm5H90e4CTjYK8jsqq1aJKygw==', '2016-10-14');
INSERT INTO `web_u_c_user` VALUES ('2', '0', '1', '', 'jdxy', 'TtaXcK19sx4IvtRWFuAFhVboM7lMq0/dPQC1dFB+/qU4Tsujxzk+br0M+VS+/h3vE83U1MjMEFr2nXntyxUOow==', '2016-10-14');
INSERT INTO `web_u_c_user` VALUES ('3', '0', '1', '', 'rexy', '9D6pxNCKsOQsUFG7Y/Q51o+c9GbZPBN0JM6RoTYFqTclhoMG1TIzNqpswB82Zej1sSZQkDCLpI7UUICSOR0cCg==', '2016-10-18');

-- ----------------------------
-- Table structure for `web_u_m_article`
-- ----------------------------
DROP TABLE IF EXISTS `web_u_m_article`;
CREATE TABLE `web_u_m_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `create_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `update_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `author` varchar(100) NOT NULL DEFAULT '',
  `source` varchar(100) NOT NULL DEFAULT '',
  `category` int(10) NOT NULL DEFAULT '0',
  `focus` varchar(100) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `hits` int(10) NOT NULL DEFAULT '0',
  `ontop` int(10) NOT NULL DEFAULT '0',
  `user` int(10) NOT NULL DEFAULT '0',
  `add_time` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_u_m_article
-- ----------------------------
INSERT INTO `web_u_m_article` VALUES ('1', '1473303300', '1473316494', '1', '1', '测试发文', '测试', '测试', '2', '', '测试测试', '0', '0', '1', '2016-09-08 14:34:52');
INSERT INTO `web_u_m_article` VALUES ('2', '1473318476', '1474248036', '1', '1', '测试2', '测试2', '测试2', '2', '/2016/09/1474245354880d269d838a1c8f.jpg', '测试2\r\n<p class=\"MsoNormal\" style=\"text-indent:24.0pt;\">\r\n	<span style=\"font-size:12.0pt;line-height:115%;font-family:宋体;\">这次亲身体验让我有了深刻感触，这不仅是一次实践，还是一次人生经历，是一生宝贵的财富。在今后我要参加更多的社会实践，磨练自己的同时让自己认识的更多，使自己未踏入社会就已体会社会更多方<span style=\"font-family:宋体;font-size:16px;\">面 。</span><img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCADIARsDAREAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDw+uwyFPSqAhbrSQ0RnrTKHqOKaJYqrzTEWo0GKsgkCZ7U7CbNvRYMuCa6aEdTjrysj1HwnaZIOOAK6ajsjy73kdp9nxH07VyNo2toc5rdk0iEKKG9Dmmjkv7OCXIBHNeXVdmZym3oaX2EbeBXI5GfKVpIdvBFCZOxTcYfrxVpjdy5GdsYOeaTI6mjpE5jkJPWkzoouzO30e54TLcHvVJnq0paHVWs+1fvA/jQdcZIfJPJ1BAFA3IyNTviqFQwJPvSc0jGc7HE6n50rsyLj3zUOSepySXNscTrUksLEuTkVpBJkRh3ObuLsuxLcVso9ioxK7xtKu5RxW0EaKXKVTbP5gznrVOSsU6isaI052jHWuOdRJmMaqbM29sGQE4q4VLnbTqGHdR7TXUnc7YspuuaTRQzZU8oxQtOwiKTAqGNEDHGahlDPwosM3gK3MxGpgQN1oKQ09aEBIo4qhEiDmmkJlyFc4xVmbLCR8imiJPQ6TRYQGQY78120VY8+vLRs9Y8KxhYRgc0V2cdJanVCLKc8n0FcdzpsZ2p2zwozyxOqjnkYovfYynGy2OHmO+9JA4zXn1VqcW7JnkCqefyrl5GVZIoOjS881agZ2GLZFuxq+WwMtfZCFVafKibNMsxWZRdyipdkaJNal60neIgLnHpWTNoVWjcsbq5dsKGwTU8x1wm2b8UF26Y5INS5PodihJoibRppm+fPWs7sXsbj20JEjII5ouaexSR53430uOCNnx3rSEmmcWIXJqjzO5RM/LiuqLZjCT6l6wjUpg4rqjsZVptDp4UEoI6ipauiKc5NWZaSZVix6Vx1IGUb8xlajOrqwAFFOOp6dFNbnK3y/Nmu+Gx6kNimVFXY1I2GKljRC5xmpbArucms2xoiakyhMe9IDdrczGPTAhbrQUhF5NNAyaqJHxjmqQmaEA45qjNssIOfamjOR0GkS/vVArqps4a60PVfC8m21LkEqozx3p1jio6vQ1m1e6E/l2gC8DLd8+lcUmke3QwfMryLlp4ivoZDDqES3MBGGRx1rJxi9VodksErXRi+K/D8Yh/tnRAzWLf62HqYT/hWU1ffc8LFYT2bcoo5ZSJWCjqa57nnM1raxLADFFzVQuaEWnH+5mk5Ir2ZImmFpMkUudE+yNOLTAYsEdqlyuaqmiOPRxv6VzSkwjSVzo9K01EUblqondShY6W2hVVGQKZ2wLKxRntSsaJJlO+iUI2MUmRJaHj/wATY3aElAcDOcUU9ZHl4r4keI3cjrKRz1716cYDjBNEtpeuDjNdMY6EzpIvy3WY855pW6GSppOwxLgmNsnrXNUWoez1K8nMZOcislozrpow7s5f2rsprQ74bFRutMsY4zSKKsg61DQFV6zaGMpFi4oA3K2MxjVQETUACdaaAk60xE8Iq0JmhAvFMzbJ9pFMhm94ctmklU4rpoq+pwYqdlY9l8OWJjsQTwdwPT6moxE9SMBTuxb63aN1uBzgjFefOR9dh0kuVmncQedbBsAnrkDmslJhGydh3hu7NrNiUb4ZBslTHBHTpVN3McVh1LoZOq+Gk0zVXWH5rdzvjP8AsntXFVbiz5Sth/ZzNSysUwMAUua5cY3ND7IqjpSbLcUhiQDfQtCOUtLHtWhstInt4BxxUWNIRNRIQoGKs6VGxZjfHFI0RZjPvUmkWJLHvFS2Nq5xni7REuYJAR1FZ35ZHLXoKSPnrxloxsZnbGBnNerh6vMjjp3i+VnJ+aEXI610OokbOLe4+K5MvGfzqVXRDjy7mnDFmIA1jUlrclK70LEkGyDOMgisFK7N4s5y8A3nFehT2OuGxScVTNBjdKTGitIahgVX61ky1sMxSFsIc0DubYrcgY1MCN6AHRiqsDHgc00hFu3TpVEyZqW0WRk1SMpMuR25cgYp2MpSPQ/BWlbihZf0rpT5YnlVW5zPWYLYRaeoUDr/AENcFSV2ejg42ZUmt1ltGUAZ9jWD3PfpSs9R1ggNqVJ2svAFZPc1k9TLgJi1NR/A7HK9s+tFzrceaHodVfwfa9Ihf+OFiufasqy5kfNY+lrczbbCYGea5UzzIOxYeb5afMU5DI23NmjmJNCCPetVuaxVy9HEAOlM6IxLkYDIM9qDVEcmEJNS0GxJDIpXg0MqLLCOMVmapmdq+14yPWsZPUJ2aPnb4v3CLd/Z4iN3U4r0cJF2bPNdvaWPK2U1tNmraGcr0rJkPU1LW9ZUB69j9apLm0IirOxZk1HMOPahU7M3UTFuJNzE12w0R0xVisxBqmUQyEAUnoUinK1ZSY0QE81A7kiD5c0WE2N20ijVPStyCNiAaBjCc0xkkfSrRLJ4lyapE3NC2TkUGbZ0Gn2+9elaJGE3qbNlZbpF471VjnqTsj03w7CsESADmnN3OVKzO7tE32yg9NwB/UVxVNzuwz1K8kJTKgcdKzZ7dOVylaP5V46HoQKykdLV43KOr2u26FxEDuVgWHrUHZh5+7ys6ixkWTS7wrjbsDD0pPY8bMY8sdTCY85rhaPnWRtNjvUu5HMWbSQE5qosuLNy1IVK2R2QLCzDFM0UgS52nmgakYHirU7mG1k+yLlz0PpRa5z4mpJR9080j+IGt6fcFbiKNlB54PNWorZnEq1aOsWejaB4wg1O0VziOQjJXPSuSo3F2sd9DFKa97RkHibxJBYWEs0kg4BxWdKm5ysjSrXUVofOHiDVTqupzTvyXbj6V7UbQjyo5YprVmTIoHaoY7tkMqADik0VBsfaW82C+0iP1PehRCUlsRXg5G01rE6KWxSYmtkdKIJH20NlIgkkqGxld2rNsYzNIBwf5cVSAkB4FUM0SeKskhbqaCkNpoTJ4qtCZchTpVEPU0LQZYCmiJHX6TCCoFbLY46kjpLGFVYHHeg5pHYaTIF20NXIZ2+mShrf9RXHVR1UJalq/hypZa57ntUZHMyyFNTQjv8AKaUz0oRvBmjeKXh3/KSBz9KyCjo7GrpSh9GugV58r+lFjzcx1bRx7XGa5Wj5xtEMktQ0Zt2J7S5w4pFRmdBbXQZBg9q1TOuM9CUTYptlqRDLM3QUXE2zC1fUFhUiQg/WqjFy2OepOx5/4huIJwfKRc9yK6oUe5y25mcv5c6ZkgnKY6BWxiuhUk1Zg1G9mjH1vUdRvFEE0zui9qx9iofCjrp8q3MRY2Q8g5qXdGjkmTiNpsKi5JqknLYx5lHc6LRvDybRJdfMx6LT5LHNPFOTtHYNbiSJdigKo7CpsaUEcnOu6TitEerTVivLBhc1tFG0XdmbcrjNTI0RQc4NYyZSGZzUXKAnFNBYQnA4pjJ1+6Oa0RBonpViIm60FIAtUtBMtQLxVoiTLiDAqiUaFgPnWmjOZ2WjkBQa1WxxVDdilAxQYt6G9pMhkdRVdDBy1PQ9FQ+WM9K4qrOqhsbar5kIU9du0/UcVyvRnsUZaHE6mphv+eNrc570p7I9uj70TVLZg7DOB9c1mTBWZ0WgRb7OVQPvgrj8KctDy8VrJo81uVeC4kicYZDg1zS0dj5iV4uxBuLsAOaybM73NSztywzSubQgaltE69OlUjojFlxY3duaLmqVyHU90NuzDihPUJJpHjXi7V5TftGHJI5PtXoQtFHLFc2rOVnvZXzlj+FVzsvkRRn1CSJcL1o+suCGqSZoeH5UuCTc43E967MM1NXkc2JTj8Jr3Gm2sxJj25FbzpQZwQrTT1IbDTo47jnFKnCMWFetNx0N2PZAfmPy1VSlF7HJTqtPUydcgSVGZea5ZUbHdQxFnY4yWPbPhlxUclj3YVPduhbiAGPC8mrjFkxxGphX8RTPFTI7YVFIx5hzWEjdMiAxUpFAaoEyMnmkMeH461QrGxWqIIz1pjFUU0JlyAcVoiGWFpko0LLgg1SImdRpsuAADWi2OWaNu3JcgChnNLRHa+GLQswYinN2Rzxjdnpmk2+yMV51WV2ejRjZFp18u529BJ8y/wC8Ov6fyrK90dtJ2djn/FNgZYzdQjJTBcD0qd1Y9jB1bPlfUzbWYXNxFHuICncf6VB3OHJBysd5pCeVCuMY60pngVHeTOF8YWQTX7gp918P+YrlrOzPDxVO1VmTFa4bpXO5HOoG3pqqF2t1oUzop2WhtQRKR2rZM6YotxRKoPApo2ikc144vo7LT5Hboqk1cNZHNiZ2VkfPsxn1G9lkC7nclj7V1cxlFKKsUr23ktx86kZpSkUpJmYyguC1RFa3Zb2NOFY1QFSAfavRi0c077FqGfaPvn86tz8zFw8izb3gQ7t3NEZmM6Ny0+pLKm0sCK09o2crw7TKS3QDFGJIqHMqVBx95GfqEAc7l61jKTuejQqLlKcTYYBvxropeYp6O5U1i3BQsooqx6o6MNUadmcpOvzkVxnrwdyuVwaLFojc4pFkeeaQwz9aLhY2ya2RkM70xkg61SEy3HwBWhBOtMkuWzYIxTRLOm0lCwFao5KjsdVpVuzyrkd6ZyTd9D1TwzZARpxXPWmVSgd1ZQAIOK8+cj0qcR2pW4e0bGRInzIw7EVMJamj01Mi1mS7hLKBhvldPQ9xVyjys6aczk9TtH0e5M0cZa3d92euD6Gs5aO572HqrER5W9Ud7okwnsYJRwGUGiR4NWPLNov3enWt6MzxKzevesX2ZzzpRn8SMS68JQMWNtKyZ6K3IrGVGL20OaWCX2WZFxod9anJj3DplDmsnQktjnlh5w6C2skiAh8gjsaiLa0Y4NrRlkTMcVXMzRNnN+NNIn1WyZYvyojNxdzDEUpys0YngvwV9lR2u0DO/XI6CqdRy0RpQouWszF+JvhhooPMtY8heeBW1Kf2ZGVSk6U79DyCaNkYhgQfeuhOzLWxCHYHAJraMh2FMrgcGm5sOVDDcuMjcaamxezTGC7cHqafOwdFWLMF0WcZNCk2zCdLQvTTjYpJ46GtVa5zQgyBo1flOtdMY3Whpr1ILoFoSrdqcttSqTtI5W+TbIcVxzVmezSldFQ8ipRsQuMmhlJjCtTYoTYaLBc12PNbIlDR1pgyWPkirRLLiDitCCZelBKLmnrvkH1qo7kVHY9C8PWW+MZFannzd3Y7TR9PxKpx3rNyJcT0rRIAkajFcdSV2dFONjqLYfIMVxyZ200F4wED7jgYpR3HPY83j1N9P1aUbgYpG5x09jXe4cyIpy0OljaO/gkhmAdJOlc8o2OmFaUWnE07QLZwxxDoBUPUiUnJ3ZfhulbvWbiTzFgTA1Nirj/MBpWC5XurG3ugS6AMf4hwaTSejIlTjIyLnS3gJZBuSsJUexi6XKRRRjGMcVkolRRYjiVRwAK0SSNYlDVrGK7hZXAOR3qZLqKcFLRnjvi3wWplkeFPfinDENaM8ypTlTemx5Lqtm9jdNHIMYOK7oSurlwd0Ui2a0RViF2xQWiIsKBgspU8U0S4XHS3hMe3NXzERo2dxLHUGRwrmtqVW2jCrh7q6NZ5UmjGCM113Ukcai4swdSg5JArCpA9ChMxiMEiudaHeiJutJ6lIQHnFAMk2g0CuWzWhQA0xb6FiHnFWiWW0qzMmVSR7Uyb2NfSYDvUkcGriY1JnqnhmNBCuRVTZxR1udvpioCDXPJmq2Ot06QADmsJI0i7G/bSZXrXNJHTBlfWJwlq+CM44zVUo6iqy0PI9VuM3LEjY2e1d9jOmzrPC10XjjEhIK4IPY1hURrc6O9ckbsEcVjFDuVo7wxjJzjPX0qnEi5OdSWM4LLzyPmqeS5VxZNetbaLzLi4WNR3JpezuXBSm7R1Ktv490CSdYl1O28xuilwDSdFnT9WrfynV2twlxEHjYMp7isJRaMttGVL2zwDJEPcgVDjfYzlG2qMx7gKcE4xWF7aApFG8vAqk54FZydglI4zWNajiLbyD9aws5ann1q3KzxLxvOt1qTugGDXo0X7qRlQlzanKsSK6LnVYhduaLlpELtiqGkQNId1LnVy+UC2RVXJsQSA9aNjRDob54jgkkVrCty7kyoqWpca7WaP3ro9spIxVLlZlTgbyRWTdzsjsVmrNmqIzkGhBYeH4qrisaARmI2gmrSE2kb2meEdX1BUe3tJGR+jAcV0xw03q9DmnjKcdDtdN+FGrSRAzhI3ByMt1rVU6aWstTlljG37sTsNP+EVqYUNzdMsoHOBwaJTprZGftqsutjQj+FOmrtXznZB19aXtYW+ETlUfU1NP+HemWqmJiXjzuUnqPUVHtlbREcspO7Zs2/hS0t02wyMAOnt7Vm6t+hap26kp0ySHIhcN3qbpjaa2L1j58W3eufepkkNNo2bW+AXDZBrGULmsKhmeIdWg+zsjfNV04NahKXNoedu6SXJDIzIT0BGa6bBHQ7Tw9aLHEjW0hx6MP51hN9zRSudTEjmPDAVgzTcjeD2X8qaZLRSutOjuIirKR6Mp6VSkCPI/irpd3p1ossT3DqzfN82RtxTlK60Pby1p3R5n4c1K70fxJDe/ZRdwYZfLf7hVlwfbIznB4yKUW07noVqftFyn0Z8HNVnm0yWGYMsSNiIM2SBjpU14dTycbFKSfXqenRyAiuNo47nP+LLJ0spLu0HzJ8zqPTuazqQ5lpuc9ZOMeZHk2peKowSqv8ASsPZNnA8VfY4zV9W+0FiWwPWtFSsjnd5O7OQv9szs2SxrSCszWm+UxbgEZzW6OyOpT2lmwKaNG7If5R4yKUnYhyFe03ITkVk2mQqupRdSpxWsDa9xjDK1bKTKci/MazubxYgOO+KpMGI571vFlIruaTLADIpoTGnrTKufSGg+CdK09VaSNZ5R1Y9D+FewlGHwo+eqV51Hqzt7FFtogkCqigcKtZyfNuQWlu2B659qnlQ9SQX5HXilyIdyT7cAKXIUmC36lsZNJwKTJDfqAMnFT7Mq4wXbE4U5o5Srjlu2UjcdtS4opNiz6xbCPEu7eO6io9m0NpHI69q6TttQcDvmrjGwJFHTMzyZwCBTKud7oLYQAkisahUWdTb4AAJzXOzVC3XCErSiNla0dWzkHcPWqkhJk15ZW99bGC7hSVD/CwzU3tsawnKD5ouxhweBtCWQP8AYYsg5+ZQear2sktDr+vVWrNm5ZaNa2IH2NFhA52qMA1DqOW5zzqOW7NOCUhsHg+9ZyiQmWQ6uu1gCD1zUNWKufMnxI0CTQvFN5H5cgtpH32ufusp5Jz7E4xQ43948WpT9nNw/qxyE0T9ZMg+/H6VLi+okyqwXn+dPlKKt1EGHGKpM1hKxS8oIecVfMattkFxIAuKxqbBGOpAknI5pU0VyEVyRnIrqjEuKZVY8VMkWkU5fvGs7G0SPOOtUirDWORWkSkiF+lUzQFbFCJYhOTVXHZn0vZ3aydZhz2zzXutHzNmbcMgKfJIzD0zWbQxwmO7DsVPbdSaKSuTPIRj5seh7GpHYieRu8gB9DxTGOjuGA4CtSaKRILnJAkXHuKmxSLCz7cBT83uKzauUh00jPHnGfcDipNLmPeiSQYTLfjTGZstiyAySR4Hf5qQ7jtPmhiuMlWT0Pl5xRYGdxpExbHlAsO7NwKyktNRpnTWcgXAbbmuaSNYsvyBJEODz7VC0L0KojmVsBvl9PWquiTQt+nzYrNlkxOABSSKuOJ+XJIpAUpJgsgxzx2q1Em5dgkVk9zWckWmcX8YNPa98JT3EfFzZfvkI4JXowB+nPHpTguhzYyHNBPsfNbuMknrRyHFYpTXAXp1q+UtQuVWuTUuJqoFK4uDz0ArO1jaECpvLnmkoSkW42FYhV4rohTSBIrTSc8GtGrFpD7ZRJwamMbszqOxJcWny5Aq3TJhVMyeEqaycLHTCdysxI4pLRmyQxulUyiMnipQIZmqHY90tbhIiHlBX3ByK+hufPuN9Eb9vqMflhoJdpAz0/mal6mfK0Xl1RWUrJgE+nIqbWKSLNrfQyYQSMr9s8g1DTHYtPEXXMb/AIChMCKF2QlHRt3ZlU/rQ0BFNKwl2HJ/GlYpF6AhYhtXaD1wazaKNBJEdMKB065rOxVzD1DfHISm78B/WqQ7lvT83AVZAD2OTkVLVgRflt5CQkR2jrhRipGWtOt54xuWdlYduoxUuw0asEt9uxKyEHphcGs3GNi02XLW+lHEyOmP1/GolBdClIvrdKcFcN6DrWfKXcvWs+8Ek8j8qhxKTJ/NDHrU2GQ303lwHDc9hmqitRN2MRpHQeY9wiknLE9AO4FbWvpYg0tOuTsYlw5B6jis5RKTPPvi34ruNMt72w8tmFzAqxEDoDncx+mMYpxjpcmtKLjydX+R4HJdNLnLE59RSWpzqmjPuCc03Y1ikVWY1m1cuyIGBkbB6U40+Y0+FDJj5YrbkSQblV581Fy1EgZ9xp3LsWLdtjAg0k7GU1dGokwZea2UjkdNp3KtzGrjiplqaQk0ZNzFtzismjtpyuVG9Kls1uMNCBCUyj2KCHP8ePocV7iR4smWrcPHMFDAgfdI4NUiHZovNK2AynBFDJSL1pIp2t5vT0pCZtxTJIAokb8RmlYkknby4siQMfxBFJagLaTCcfvCAw5yvJpSQ9iQP5ZJLl/RRx+tQylqaWktI/3lVc/jWUkUhdTt3Zg0SFj6A8U4sLlCEyoy53RL0yxGT9MUNDRqWkyROWYk7uOT2qGijTgaQDfGAOnA71DsM0JnaNVdlBHVvf6VmlfQonSTzJkXcNrLnB7Cpa0KTuRtE0TEwEhT27UXvuMWK7lhKjGBn5s0uVME7Ftb9QoO7OajkKuZuo3sjqSHGwfrWsIohsox3ReCTzgCFG2MEccdTV21Fc149RhhiLSOuzgE8Y6d6zcG2NzUVqePfFvW4L3UdkDkKihQT91xWko8kLM5r+0qc3Q8vlmXcTjB9q5GzfUpTXAJ61m59C1FsdEyMua3hqhtWIZZVRqtySLSuihdS7+lZync0irFNqzRqhhbFaIdiRZDipbJ5SRbgqetNSIdO5KLnNPmIdMiuH3rxSbLgrGfIOahm6IzTRQlMD1aBhsxyB2zyBXrxPLki5azru2SEc+pNWpdGZyj1RZmlEbLvbIPAaqcrExVya21HcrLuTKnp3/A0lJPQbgaVldsSSj898pyKpamTVi41xJKCN3I7kdfwpi2Etrpoz843jPZmH5CpaGX4bmO4lCCIvI3ZhgCoYK50+mw4iwwCr6DgVjLfQoW8iYjERyx52+o+tKL7jMJn/fk3By69AGyBVtBcuW1whkTAyMYC9MVLQ0btpIvmorOVReSM1nK5aZrGWO7eOGFz3yfp2rGzjqyr3J7aDypGA5BI5NTJ3RSVixJCVBAJ9alO5QskAmtvu/OCDihOzC10YHiCzdQHtZPLYckHuPQ1rB9GRJHn2q+N49Kla3vVkaRPvAfmfzq5WRMby2M2bx9Is+Y4QGMaDax4Xcc/wBf0p2XUGm9ijq/iG5lSUmQ75sMuDwrbQRj8mH41ppFGNuZ6nI6hffbo97nA6MP7h/+JP8A9btXLVqKaKhBwdv6/wCHMSUlcg9q5PI6I6mdLuZ+AajkbdzdEod0Toa3V0gaKU0rFjkmsJNtmkUQl89aLsrlDOapSBaDG61opXGJ0oYCGkgE3GgY4PwaYiCSk9y0iOmMKBnpkMgVCFAIPfOK9KLstDzmrl63lAXAmBP90CtFLzM2vISQxvHncUJ9+9Dsw1RX2gsRuy/YnikUXLOJnU5lLegBrSKuZyl2RP8AaTENkhUKPbrVc1tyeS+qLEV+ExiQ7T23bf6UXFyM3NJvgW3RrnjqxxQ1chq2502n3xZiZWBHbPA/AVEo9hGhNNKyhlbJIzgcCoSC5hXlxIkhIQE98Hr+VXYYyDVUeeJSvlt+ZJqXEpGzatyGIy7HHJxxUtBdHRW9wrBREFUrjJU/d9Kwce5aNy3lDx43guOD7+9YtWNEy3DlY8OM+lZvyLQpACYHA9qEDMzULb7RweQK0i7GbVz5/wDizYFfFkURDKkhUHI4K9yD9M1tKPMo2IpS5ebyOVjcXE80nQNKn8yardtm6hay9S3qMv7g4OCu0/Tt/T9aVRuxkqepgz7vP3xdGHI7VwTjJT5kaRhdWkIYHfGBj29KtQuaxg0Tw2IP3hW8YJGnKLPZAA03BBymRe2oXOBWUqaLSMiTKtisJRsVYQNU2CwbqaCwoamFhDQIb0oGIaopIjepGhlUUOHSgR3VvID8x7etdkX1OOSNK3uuOQpAraMzGUCaSdXBwMe/pVc1xKLRAs7cgEkfWkpdB8vUEmVjgk/8BNNSuPlGSSLGQy8n86L2GlfQfFcyzEbNqg/iaak2JxUTo9PZogBKylRyxPJFbLQ5p2b0NKz1GLzicOVX5mPr7UEOLRpf2jLfQBoyY0boM8n39hRYTVmFqgUnfcswI/ibikwKOrukjAwMrBe4oNI6MuWepahcNE5QBYvvPgdPYVNkDSR22lyhhmFlk4yx9WrCaBM2tOmUrtkIWbOaymvuNEbcJB78VgyyRiQBipQXGNjazMQBjqe1UI8F+J0Fw2uSu8nnRLGTHhcEE8f1Nd0fhMqUVza9zg4IXij+ZDgyKahaI7m0PkKyllY4BJQ/j0/UUWvuFitbxBSVIyRWTiaabl9I0ZcBeazsy9BHjVOnWqimK6IJFXGSaqzFzFCa184HZzRy3DmRkXOjS7uhzUOlcOcrto9wvY/lWfsGL2pG+lXKdUqXRkhqqiJ7G4jPzRml7NofOmReRL/cajlY7oaUZeqkUrAiNgcZoLIiOaSGGOKYC7aBHVwTg4yenrW6kYuJeSbjCjnuTWil2Mmu4lxJ5a4B+Y+lDdhxVxILlwCAFwOCaqM2EoJjmlY9Gx9KOYOUjlmfjBz9eaOYaiTWczCRSW/GqjLUmS0N6GbzIwpyTnmulO+hzNW1Lj3AQGJccjDY/UVd1sQo31ZYguRHlmb5f9nv7UEuLLUUpuZAz/KvQL6CglqxpyvCIeEHAwPQUhIl0ya6QPEFXymxjcOSaTGb9lAqzsEkcTAfMBxiob0GdNZRgxruOGB4bvWEi0bUGUAO/IPUVgy0W2YFRg1AyK+lSO0fzWGCPXH61UVqS2eH+IbsrNLGGZ4i5xu4wB2/WulsunFHPTSwSKqAgHOcVJt5mRcW4eZgueT2+tVuUnYt/YiIw4U9KlhcrNNLHlUj6d6LDTRn3F8QxDLg+tBY2NmuB8v60Cehq6dbOo3Pjb6npTM2ySSa3iYs2GNNOwWZk32qAN+5RRzUuZSgOsboyNuKbvc9BTUhSgaP2ZJ1LMoCgc56mn8RGxSBtBuUoqgetTzRK5GZN3PaSTiKNA7HpgVjKSbsjVQsrsvNo9mIlD4MpGSPSr5ImSlK5Rl0zTQzAFmI6YGcmsmoXsi05E58O2K2vmySYYjO0VTpxSvcOeTZQXQ7VxuEpwfasuWJd5dihDIR3x70JmkkaEEuBgdT/Ef6VaZk0XCFK5bAwPxrToRfUqu4X/DNTctIkV14ycn9KaaFYHZVGc5NNsFdjoZdjA96aYNXNewmEkchyQq+neuiDuYTViR2O7BOOmRVXFbQsC52AYXJHSquTy3LltesXBIJUdqpSuQ4JGrBeM0waTbt6hB0ApmTiuhvWN2uDKw3P1AHb0+lJokvWFw0W+Tr5hznPWk1caOnsNRR/vHBPUGsJQGmbEThssjZA7Vk0UXlmG3g4AqbFcxyHj7xFBZWnleYRIeAQRj6VrTp6XEpXlY8t1HWIpm+bHIxkUPc6oxMhLFLi4V/MPJ45p6A20dBbafa2kPmTOzqO4HWkTdso6trNrt2RxlVHfFIuMTnpb6ORsr0ouacpEka3EmChLfSmK9jbh0xI7YHID+hoSZDlcg1CB1tsfaVT/ZBzRy6bgpO5y8skpkMQLue3FZtmyJ1hijCi6YjuQKNOom29ia41CG2RfLAAHQUnJIFFsz5dYkPBlKhuoHeoc2XyJEcswePdnI9qTYJMsaJEZpmaBEG0ZLtwFoi29iZ6GjZzxeewd/McnAUjGf/AK1CavuJp2G6pcQxXGxpkeUDlYvup7ZpTkt0OMTMvL+5uQUg2pAB0B6+5qLtl2SMR1nLEmV8+2P8aVirixsM9cCmMuQyAEE9BVJkNFtbjOWxz2HYVfMZ8pBJITz39akodA4zyc46A00DRM8hIwPX8zTuCViDzMFjn2FJMC/Y3QiXB5AHI9TWsJ2M5xuXopfNlY7uAcD3NaqV2Q1ZGgqCReTg54FambNW0tYxIqsTgLk4rRKxg5OxrwWatIABhQM5PUmqM7stKTbyiJiNjjIPtRYQ+3mltZAPvREZAz70mNaHQadeI8wLKcdAfaoktAasdTZzxgBgw4rBoaINf16PT7ZmjZHkAz5ZPWiNNvcpaux5P4m19dYJEdsA44A6n6VV9LG8afK7s5GLTLoSbp3CAnhahrubqfY6K2tl021Dsyhm6AnmlclrmJZdWRoArR+YfUtwKeouVIzbmZGU/uE/LmixRjW1ihujI7vnPypilYu5rGC9tMFLU7uoDdaLMhuLKF3cag0uXgbPcdaNRpoz7yaOVdiySRyjqG4FS2ikmhtjZ3LXA2suP75PAoSY5SSNm90u0jCmV5ZZW5J+6v8AjTlFEKbKu7TLc5jtjNN/ek6fhU3S6Ds3uyle2sFy/m/Z4kz9TUuN9SlK2hTttPhgmZmZnOeFIIUf41HLYpybNKCYodqqipnJz/Qf40asTRTvTI87sjxxhj/COVFS1cpaFB4NqfJMzE9cqMUrIpNvcYgdDjaje+AMU0hNlxUg2jf97vwKLIWphLE2eOak0LMUUmOVJ+lNJktkuJcYC8DtT1J0GPvAwVOfehsdkIjEdetCY2iYSE4FMmwNzigBUJGB0xz+NAMvWb7WBYnitIsmSujUtZGLh42Prg1tFu+hi10Z0FhcM7qXYnHauiMr7nPOCWxuSyL5CsDznNXcy5Spc3guLZfm/fxd/UUnIpQ7iw35MQV+QOh9KnmHyGlZ323BZgKdxcrLt1rixQgeeoRhjjJpNpahGnc5S7lu7iY4uDcxdvM4K/iajmubqKQwWYJDu6q3XcnWlYd2WoJLSPh4/N/25GIosmLUeZLBiWW3hP8AtSKWP4ZNKw7dzO1G2sZoiYLZkkz95X4/KlYcW11KkMSKFEuSB6GkXq9iea4jTAhRU+g5/OlcFHuQG8kVSFkIB6nuaXMPl7laSYnPzMR9aLjsUmji3FtgLUroeoxLkwPlDg+tTcfLcQ3BkYlpCWPU0nILE0bBjyaXNfcGh5ZFzgAHvRzImxVnmCAheAahyLUSiZcng1Fy+Ugmk75ouNRIlYHpSKH5PWi4WELjNPmFYjEYU0XE0x2/HTincmwqSk9aLlWHFweO1DYrFaQDPFK5SFUgmi4NE2PyouKw9UBFO4NEyDFO5LRct5jG2atTJcbmjFeFWBzxWqqEOnctnVCF25+U1ftiPZIhW5O4c1DqFqBNFdbXxmp9oDgTm7bnDcU/aC5EM+0kqeaPaD5EKLsADJ4oUw5UEt5gfe61XMhchTa82v14NPnHy9hFvgGwT1pOYcjHtejGM1PtAUSA3oyQTScy+Uie53dTWbkVykEk+BnNTzD5Ri3QPU0+YOUZJMM5BqeYaiU55ck0uYdkQxzEGi4WLouflBBpXFyhJchu9JsOUqT3HB5pFJFTzznrTTAa0ueM0XAar8UAL55BIzSAaZqY7H//2Q==\" width=\"283\" height=\"200\" alt=\"\" /></span> \r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:24.0pt;\">\r\n	<span style=\"font-size:12.0pt;line-height:115%;font-family:宋体;\"></span> \r\n</p>', '0', '0', '1', '2016-09-08 15:07:52');
INSERT INTO `web_u_m_article` VALUES ('3', '0', '0', '0', '0', 'title', 'title', 'title', '3', '', 'title', '0', '0', '1', '2016-09-14 08:30:26');
INSERT INTO `web_u_m_article` VALUES ('4', '0', '0', '0', '0', '测试有用1', '测试有用', '测试有用', '2', '//uploads/147453069969334885.png', '<p>									</p><p>测试有用测试有用测试有用测试有用测试有用测试有用测试有用测试有用<img src=\"/attachments/2016/09/14745307181370476170974971.png\" title=\"14745307181370476170974971.png\" alt=\"logo.png\"/></p><p>								</p>', '0', '0', '1', '2016-09-22 15:52:01');
INSERT INTO `web_u_m_article` VALUES ('5', '0', '1475980286', '0', '1', '测试base_url1', '测试base_url', '测试base_url', '2', '////uploads/147453102112762398.jpg', '<p>\r\n	测试base_url测试base_url测试base_url测试base_url测试base_url测试base_url测试base_url<img src=\"/attachments/2016/09/14745310384973613281381449.png\" title=\"14745310384973613281381449.png\" alt=\"yis_b_09_jqm1.png\" />\r\n</p>', '0', '0', '1', '2016-09-22 15:57:20');
INSERT INTO `web_u_m_article` VALUES ('6', '1474881549', '1474881549', '1', '1', '测试本系新闻', '测试本系新闻', '测试本系新闻', '5', '', '测试本系新闻测试本系新闻测试本系新闻测试本系新闻测试本系新闻测试本系新闻测试本系新闻', '0', '0', '1', '2016-09-26 17:19:03');
INSERT INTO `web_u_m_article` VALUES ('7', '1474881572', '1474881572', '1', '1', '测试通知公告', '测试通知公告', '测试通知公告', '6', '', '测试通知公告测试通知公告测试通知公告测试通知公告测试通知公告测试通知公告测试通知公告测试通知公告测试通知公告测试通知公告', '0', '0', '1', '2016-09-26 17:19:31');
INSERT INTO `web_u_m_article` VALUES ('8', '1475068699', '1475068699', '1', '1', 'CESHIO', 'CESHIO', 'CESHIO', '5', '', 'CESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIOCESHIO', '0', '0', '1', '2016-09-28 21:18:04');
INSERT INTO `web_u_m_article` VALUES ('9', '1476771548', '1476771548', '1', '1', 'xx测试', 'xx测试', 'xx测试', '1', '/uploads/147433473766296586.jpg', 'xx测试', '0', '0', '1', '2016-10-18 14:19:00');

-- ----------------------------
-- Table structure for `web_u_m_banner`
-- ----------------------------
DROP TABLE IF EXISTS `web_u_m_banner`;
CREATE TABLE `web_u_m_banner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `create_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `update_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `banner` varchar(100) NOT NULL DEFAULT '',
  `site` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_u_m_banner
-- ----------------------------
INSERT INTO `web_u_m_banner` VALUES ('1', '1475306344', '1475306344', '1', '1', '计算机技术新2013年新生报考专栏', '#', '/2016/10/1475306344bd715006f371d3e6.jpg', '1');
INSERT INTO `web_u_m_banner` VALUES ('2', '1475306370', '1475306370', '1', '1', '江西信息应用职业技术学院一保通网页设计大赛', '#', '//2016/10/14753063701804037f5bd27160.png', '1');

-- ----------------------------
-- Table structure for `web_u_m_link`
-- ----------------------------
DROP TABLE IF EXISTS `web_u_m_link`;
CREATE TABLE `web_u_m_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `create_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `update_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `link_name` varchar(100) NOT NULL DEFAULT '',
  `link_url` varchar(100) NOT NULL DEFAULT '',
  `link_cate` int(10) NOT NULL DEFAULT '0',
  `site` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_u_m_link
-- ----------------------------
INSERT INTO `web_u_m_link` VALUES ('1', '1479718936', '1480406378', '1', '1', '景德镇陶瓷大学官网', 'http://www.jci.edu.cn/index.asp', '1', '1');
INSERT INTO `web_u_m_link` VALUES ('2', '1479718983', '1479718983', '1', '1', '图书馆官网', 'http://lib.jci.edu.cn/', '1', '1');
INSERT INTO `web_u_m_link` VALUES ('3', '1479797817', '1479719136', '1', '1', '教务管理系统', 'http://172.30.104.1/eams/login.action', '1', '1');
INSERT INTO `web_u_m_link` VALUES ('5', '1479800912', '0', '0', '0', '机械电子工程学院', 'http://localhost/webgroup/depsites/?site=jdxy', '2', '1');
INSERT INTO `web_u_m_link` VALUES ('6', '1479977692', '1479977692', '1', '1', '景德镇陶瓷大学科技艺术学院', 'http://www.jci-ky.cn/', '3', '1');

-- ----------------------------
-- Table structure for `web_u_m_msg_board`
-- ----------------------------
DROP TABLE IF EXISTS `web_u_m_msg_board`;
CREATE TABLE `web_u_m_msg_board` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `create_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `update_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `reply` text NOT NULL,
  `add_time` varchar(100) NOT NULL DEFAULT '',
  `is_reply` varchar(100) NOT NULL DEFAULT '',
  `site` int(10) NOT NULL DEFAULT '0',
  `reply_time` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_u_m_msg_board
-- ----------------------------
INSERT INTO `web_u_m_msg_board` VALUES ('1', '1474542711', '1474542711', '1', '1', '投诉', '学校有些东西', '', '2016-09-22 19:10:53', '0', '1', '2016-09-23 13:18:45', '');
INSERT INTO `web_u_m_msg_board` VALUES ('2', '1474544075', '1474607845', '1', '1', '投诉2', '学校水管爆了', '已经在维修', '2016-09-22 19:34:30', '1', '1', '2016-09-23 13:17:21', '');
INSERT INTO `web_u_m_msg_board` VALUES ('4', '0', '1480586821', '0', '1', '第三方', '地方撒', '', '2016-12-01 18:05:36', '0', '1', '', '4154564516@1545');
INSERT INTO `web_u_m_msg_board` VALUES ('5', '0', '0', '0', '0', '测试网站留言', '0', '', '2017-01-03 13:57:38', '0', '0', '', '0');
INSERT INTO `web_u_m_msg_board` VALUES ('6', '0', '0', '0', '0', '测试网站留言', '测试网站留言测试网站留言测试网站留言测试网站留言测试网站留言测试网站留言测试网站留言测试网站留言测试网站留言测试网站留言测试网站留言测试网站留言', '', '2017-01-03 14:02:50', '0', '0', '', '0');
INSERT INTO `web_u_m_msg_board` VALUES ('7', '0', '0', '0', '0', '测试网站留言', '测试网站留言测试网站留言测试网站留言测试网站留言测试网站留言', '', '2017-01-03 14:04:25', '0', '1', '', '0');

-- ----------------------------
-- Table structure for `web_u_m_temptags`
-- ----------------------------
DROP TABLE IF EXISTS `web_u_m_temptags`;
CREATE TABLE `web_u_m_temptags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `create_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `update_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `site` int(10) NOT NULL DEFAULT '0',
  `tags` varchar(100) NOT NULL DEFAULT '',
  `article_cate` int(10) NOT NULL DEFAULT '0',
  `max_length` varchar(100) NOT NULL DEFAULT '',
  `config_type` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_u_m_temptags
-- ----------------------------
INSERT INTO `web_u_m_temptags` VALUES ('2', '1474964184', '1479889261', '1', '1', '1', '新闻', '5', '10', '1');
INSERT INTO `web_u_m_temptags` VALUES ('3', '1474964228', '1479889247', '1', '1', '1', '通知', '6', '10', '1');
INSERT INTO `web_u_m_temptags` VALUES ('6', '1479889308', '0', '0', '0', '1', '列表页', '0', '20', '2');

-- ----------------------------
-- Table structure for `web_validations`
-- ----------------------------
DROP TABLE IF EXISTS `web_validations`;
CREATE TABLE `web_validations` (
  `k` varchar(20) DEFAULT NULL,
  `v` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_validations
-- ----------------------------
INSERT INTO `web_validations` VALUES ('required', '必填');
INSERT INTO `web_validations` VALUES ('valid_email', 'E-mail格式');
