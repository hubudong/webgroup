<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Banner_model extends My_Model {
	public function __construct() {
		parent::__construct ( 'web_u_m_banner' );
	}
	public function getBannerData($site){
//		$site = $this->verify_id($site);
//		$sql = "select * from web_u_m_banner where site =".$site;
		$banner = $this->db->get_where('web_u_m_banner',array('site'=>$site))->result_array();
		return $banner;
	}
}