<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Temp_model extends My_Model {
	public function __construct() {
		parent::__construct ( 'web_u_c_templates' );
	}
	public function getTempData($temp){
//		$siteSql = "select * from web_u_c_templates where classid =".$temp;
		$siteTemp = $this->db->get_where('web_u_c_templates',array('classid'=>$temp))->row_array();
		return $siteTemp;
	}
}