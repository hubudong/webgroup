<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Category_model extends My_Model {
	public function __construct() {
		parent::__construct ( 'web_u_c_category' );
	}
	public function getFirstCategory($site){
//		$sql = "select * from web_u_c_category where site = $site and level = 1";
        $this->db->where('site',$site);
        $this->db->where('level',1);
        $data = $this->db->get('web_u_c_category')->result_array();
		return $data;
	}
	public function  getSecondCategory($site){
//		$sql = "select * from web_u_c_category where site = $site and level = 2";
        $this->db->where('site',$site);
        $this->db->where('level',2);
        $data = $this->db->get('web_u_c_category')->result_array();
		return $data;
	}
	public function getCategoryName($id){
//		$id = $this->verify_id($id);
//		$sql = "select category_name from web_u_c_category where classid = $id ";
        $this->db->where('classid',$id);
        $this->db->select('category_name');
        $data = $this->db->get('web_u_c_category')->row_array();
		return $data;
	}
	public function getParentid($id){
//		$id = $this->verify_id($id);
		$this->db->where('classid',$id);
		$this->db->select('parentid');
		$data = $this->db->get('web_u_c_category')->row();
		return $data;
	}
	public function getAllSubCate($id){
//		$id = $this->verify_id($id);
		$data = $this->db->get_where('web_u_c_category',array('parentid'=>$id))->result();
		return $data;
	}
}