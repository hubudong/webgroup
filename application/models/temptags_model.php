<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Temptags_model extends My_Model {
	public function __construct() {
		parent::__construct ( 'web_u_m_tempTags' );
	}
	public function getTags($site,$configType){
//		$where = "site = $site and config_type = '$configType'";
//		$sql = "select id,site,tags,article_cate,max_length,config_type from web_u_m_tempTags where $where";
		$data = $this->db->select('id,site,tags,article_cate,max_length,config_type')->where('site',$site)->where('config_type',$configType)->get('web_u_m_tempTags')->result_array();
		return $data;
	}
	//获取站点全部标签
	public function getAllTags($site){
//		$where = "where site = $site order by id desc";
//		$sql = "select id,site,tags,article_cate,max_length,config_type from web_u_m_tempTags";
		$data = $this->db->select('id,site,tags,article_cate,max_length,config_type')->get('web_u_m_tempTags')->result_array();
		return $data;
	}
}