<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Article_model extends My_Model {
	public function __construct() {
		parent::__construct ( 'web_u_m_article' );
	}
	public function getArticle($user,$max,$cate){
		//$user = $this->verify_id($user);
		//$where = "user = $user and category = $cate order by id desc limit 0,$cate";
		//$sql = "select * from web_u_m_article where $where";
		//$data = $this->db->query($sql)->result_array();
		$data = $this->db->where('user',$user)
                         ->where('category',$cate)
                         ->order_by('id','desc')
                         ->limit($max)
		                 ->get('web_u_m_article')
		                 ->result_array();
		return $data;
	}
	public function getFocus($user,$max=5){
//		$user = $this->verify_id($user);
//		$where = "where user = $user and focus != '' order by id desc limit 0,$max";
//		$sql = "select id,focus,title from web_u_m_article $where";
//		$data = $this->db->query($sql)->result_array();
        $data = $this->db->select('id,focus,title')
                         ->where('user',$user)
                         ->where('focus !=','')
                         ->order_by('id','desc')
                         ->limit($max)
                         ->get('web_u_m_article')
                         ->result_array();
		return $data;
	}
	public function getArticleList($id,$site,$page,$pageSize){
//		$page = $this->verify_id($page);
		$start = ($page-1)*$pageSize;
//		$id = $this->verify_id($id);
//		$cateSql = "select * from web_u_c_category where parentid = {$id}";
        $cateData = $this->db->get_where('web_u_c_category',array('parentid'=>$id))->result_array();
//		$cateData = $this->db->query($cateSql)->result_array();
//		$sql= "select * from web_u_m_article where (category = ({mysql_real_escape_string($id)})";
//		foreach ($cateData as $key=>$value){
//			$sql.=" or category = {$value['classid']}";
//		}
//		$sql.= ") order by id desc limit $start,$pageSize";
//
//		$data = $this->db->query($sql)->result_array();
            $this->db->where('category',$id);
        foreach ($cateData as $value){
            $this->db->or_where('category', $value['classid']); 
        }
            $this->db->order_by('id','desc');
            $this->db->limit($pageSize,$start);
        $data = $this->db->get('web_u_m_article')->result_array();
		return $data;
	}
	public function getListPageData($id,$site,$pageSize,$page){
//		$page = $this->verify_id($page);
//		$id = $this->verify_id($id);
//		$cateSql = "select * from web_u_c_category where parentid = {$id}";
//		$cateData = $this->db->query($cateSql)->result_array();
        $cateData = $cateData = $this->db->get_where('web_u_c_category',array('parentid'=>$id))->result_array();
//		$sql= "select count(id) as pageMaxSize from web_u_m_article where (category = ({mysql_real_escape_string($id)})";
//		foreach ($cateData as $key=>$value){
//			$sql.=" or category = {$value['classid']}";
//		}
//		$sql.= ")";
//		$resData = $this->db->query($sql)->row_array();
            $this->db->where('category',$id);
        foreach ($cateData as $value){
            $this->db->or_where('category', $value['classid']); 
        }
        $data = $this->db->get('web_u_m_article')->num_rows();
//		$pageMaxSize = $resData['pageMaxSize'];
        $pageMaxSize = $data;
		$pages = ceil($pageMaxSize/$pageSize);
		$param = Array(
				"pageMaxSize"=>$pageMaxSize,
				"pageSize"=>$pageSize,
				"pages"=>$pages,
				"page"=>$page
				);
		return $param;
	}
	public function getArticleCon($id){
		//$id = $this->verify_id($id);
		//$sql = "select * from web_u_m_article where id = ({mysql_real_escape_string($id)})";
		//$data = $this->db->query($sql)->row_array();
		$data = $this->db->get_where('web_u_m_article',array('id'=>$id))->row_array();
//		点击量 喜+1
		$this->db->update('web_u_m_article',array('hits'=>($data['hits']+1)),array('id'=>$id));
		return $data;
	}

	public function getSearchPageData($user,$keyword,$pageSize,$page){
//		$page = $this->verify_id($page);
//		$sql= "select count(id) as pageMaxSize from web_u_m_article where user = {$user} and title like '%{$keyword}%'";
//		$resData = $this->db->query($sql)->row_array();
        $resData = $this->db->where('user',$user)->like('title', $keyword)->get('web_u_m_article')->num_rows(); 
		$pageMaxSize = $resData;
		$pages = ceil($pageMaxSize/$pageSize);
		$param = Array(
				"pageMaxSize"=>$pageMaxSize,
				"pageSize"=>$pageSize,
				"pages"=>$pages,
				"page"=>$page
		);
		return $param;
	}
	public function getSearchList($user,$keyword,$pageSize,$page){
//		$page = $this->verify_id($page);
		$start = ($page-1)*$pageSize;
//		$sql= "select * from web_u_m_article where user = {$user} and title like '%{$keyword}%' order by id desc limit {$start},{$pageSize} ";
        $data = $this->db->where('user',$user)
                            ->like('title', $keyword)
                            ->order_by('id','desc')
                            ->limit($pageSize,$start)
                            ->get('web_u_m_article')
                            ->result_array(); 
//		$data = $this->db->query($sql)->result_array();
		return $data;
	}
}