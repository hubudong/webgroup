<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Site_model extends My_Model {
	public function __construct() {
		parent::__construct ( 'web_u_c_site' );
	}
	public function getSiteData($site_url){
//		$site_url = $this->verify_id($site_url);
//		$sql = "select * from web_u_c_site where site_url = '{$site_url}' ";
		$siteData = $this->db->get_where('web_u_c_site',array('site_url'=>$site_url))->row_array();
		return $siteData;
	}
}