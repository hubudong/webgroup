<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Link_model extends My_Model {
	public function __construct() {
		parent::__construct ( 'web_u_m_link' );
	}
	public function getQuickLinks($cate,$site){
		$this->db->where('link_cate',$cate);
		$this->db->where('site',$site);
		$this->db->order_by("id", "desc");
		$this->db->limit(3);
		$query= $this->db->get('web_u_m_link');
		return $query->result();
	}
	public function getAllLinks($site){
		$this->db->where('site',$site);
		$this->db->order_by("id", "desc");
		$query= $this->db->get('web_u_m_link');
		return $query->result();
	}
}