<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->db = $this->load->database ( "default", TRUE );//连接数据库
		$this->siteData = $this->db->get($this->db->dbprefix('site_settings'))->row_array();//读取dilicms配置属性
		$this->load->library ( 'user_agent' );
		header ( "content-type:text/html;charset=utf-8" );
	}

	public function index()
	{
		$data['siteList'] = $this->siteList();
		if ($this->agent->is_mobile ()) {
			gotoUrl(base_url("wap"));
		} else {
			$this->load->view('home',$data);
		}
	}

	public function siteList(){
		$sql = "select * from web_u_c_site where 1=1";
		$data = $this->db->query($sql)->result_array();
		return $data;
	}
}
