<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Depsites extends My_Controller {

	public function __construct(){
		parent::__construct();
	}
	//分站首页调用
	public function index($site=1){
		$site_url = $this->input->get('site');
		$siteData = $this->site_model->getSiteData($site_url);
		$focus = $this->article_model->getFocus($siteData['user'],5);
		$tags = $this->temptags_model->getTags($siteData['classid'],1);
		if (!empty($tags)) {
			foreach ($tags as $key=>$value){
			    $articleData=$this->article_model->getArticle($siteData['user'],$value['max_length'],$value['article_cate']);
                for($i=0;$i<count($articleData);$i++){
                    $articleData[$i]['content']=trim(nl2br(strip_tags($articleData[$i]['content'])));
                }
				$data[$value['tags']] = $articleData;
				$data['vue']['news'][$value['tags']] = $data[$value['tags']] ;
			}
		}
		$data['banner'] = $this->banner_model->getBannerData($siteData['classid']);
		$data['siteTemp'] = $this->temp_model->getTempData($siteData['temp']);
		$data['siteData'] = $siteData;
		$data['focus']  = $focus;
		$data['tags'] = $tags;
		$data['firstCategory'] = $this->category_model->getFirstCategory($siteData['classid']);
		$data['secondCategory'] = $this->category_model->getSecondCategory($siteData['classid']);
		$data['quickLinks'] = $this->object_array($this->link_model->getQuickLinks(1,$siteData['classid']));
		$data['unitLinks'] = $this->object_array($this->link_model->getQuickLinks(2,$siteData['classid']));
		$data['friendLinks'] = $this->object_array($this->link_model->getQuickLinks(3,$siteData['classid']));
		
        //导航
        $data['vue']['nav']=$data['firstCategory'];
        for($i=0;$i<count($data['vue']['nav']);$i++){
            for($j=0;$j<count($data['secondCategory']);$j++){
                if($data['vue']['nav'][$i]['classid']==$data['secondCategory'][$j]['parentid']){
                    $data['vue']['nav'][$i]['child'][]=$data['secondCategory'][$j];
                }
            }
        }
        $data['vue']['nav'][]=array('category_name'=>'留言','slug'=>base_url('levelword').'/?site='.$siteData['site_url']);
        
		//内容
		$data['vue']['banner']=$data['banner'];
		$data['vue']['focus']  = $data['focus'];
		$data['vue']['tags']  = $data['tags'];
		
        //底部
        $data['vue']['siteData']=$data['siteData'];
        $data['vue']['quickLinks']=$data['quickLinks'];
        $data['vue']['unitLinks']=$data['unitLinks'];
        $data['vue']['friendLinks']=$data['friendLinks'];
        
//      print_r($data['vue']);
		if ($this->agent->is_mobile ()) {
			gotoUrl(base_url("m/wapsite/?site=".$site_url));
		} else {
			$this->load->view('tempIndex',$data);
		}
	}
}