<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends My_Controller {

	public function __construct(){
		parent::__construct();
	}
	//分站列表页
	public function index(){
		$pageSize = 15;
		$keywords = $this->input->post('keywords');
		$keywords = empty($keywords)?$this->input->get('keywords'):$keywords;
		$site_url = $this->input->get('site');
		$siteData = $this->site_model->getSiteData($site_url);
		$page = $this->input->get('page');
		$page = empty($page)?1:$page;
		$data['keywords'] = $keywords;
		$data['siteTemp'] = $this->temp_model->getTempData($siteData['temp']);
		$data['firstCategory'] = $this->category_model->getFirstCategory($siteData['classid']);
		$data['secondCategory'] = $this->category_model->getSecondCategory($siteData['classid']);
		$data['siteData'] = $siteData;
		$data['articleList'] = $this->article_model->getSearchList($siteData['user'],$keywords,$pageSize,$page);
		$data['param'] = $this->article_model->getSearchPageData($siteData['user'],$keywords,$pageSize,$page);
		$data['quickLinks'] = $this->object_array($this->link_model->getQuickLinks(1,$siteData['classid']));
		$data['unitLinks'] = $this->object_array($this->link_model->getQuickLinks(2,$siteData['classid']));
		$data['friendLinks'] = $this->object_array($this->link_model->getQuickLinks(3,$siteData['classid']));
		
        //导航
        $data['vue']['nav']=$data['firstCategory'];
        for($i=0;$i<count($data['vue']['nav']);$i++){
            for($j=0;$j<count($data['secondCategory']);$j++){
                if($data['vue']['nav'][$i]['classid']==$data['secondCategory'][$j]['parentid']){
                    $data['vue']['nav'][$i]['child'][]=$data['secondCategory'][$j];
                }
            }
        }
        $data['vue']['nav'][]=array('category_name'=>'留言','slug'=>base_url('levelword').'/?site='.$siteData['site_url']);
        
        //内容
        $data['vue']['keywords']=$data['keywords'];
        $data['vue']['articleList']=$data['articleList'];
        for($i=0;$i<count($data['articleList']);$i++){
            $data['vue']['articleList'][$i]['content']=trim(nl2br(strip_tags($data['vue']['articleList'][$i]['content'])));
        }
        $data['vue']['pagination']=$data['param'];
        //底部
        $data['vue']['siteData']=$data['siteData'];
        $data['vue']['quickLinks']=$data['quickLinks'];
        $data['vue']['unitLinks']=$data['unitLinks'];
        $data['vue']['friendLinks']=$data['friendLinks'];
		if ($this->agent->is_mobile ()) {
			gotoUrl(base_url("m/wapsearch/?site=".$site_url."&key=".$keywords."&page=".$page));
		} else {
			$this->load->view('tempSearch',$data);
		}
	}
}