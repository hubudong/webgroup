<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Levelword extends MY_Controller {
//  留言模块
	private $path;
	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$data=array();
		$site_url = $this->input->get('site');
		$siteData = $this->site_model->getSiteData($site_url);
		$data["siteData"]=$this->siteData;
		$data['siteTemp'] = $this->temp_model->getTempData($siteData['temp']);
		$data['firstCategory'] = $this->category_model->getFirstCategory($siteData['classid']);
		$data['secondCategory'] = $this->category_model->getSecondCategory($siteData['classid']);
		$data['siteData'] = $siteData;
		$data['quickLinks'] = $this->object_array($this->link_model->getQuickLinks(1,$siteData['classid']));
		$data['unitLinks'] = $this->object_array($this->link_model->getQuickLinks(2,$siteData['classid']));
		$data['friendLinks'] = $this->object_array($this->link_model->getQuickLinks(3,$siteData['classid']));
        //导航
        $data['vue']['nav']=$data['firstCategory'];
        for($i=0;$i<count($data['vue']['nav']);$i++){
            for($j=0;$j<count($data['secondCategory']);$j++){
                if($data['vue']['nav'][$i]['classid']==$data['secondCategory'][$j]['parentid']){
                    $data['vue']['nav'][$i]['child'][]=$data['secondCategory'][$j];
                }
            }
        }
        $data['vue']['nav'][]=array('category_name'=>'留言','slug'=>base_url('levelword').'/?site='.$siteData['site_url']);
        

        //底部
        $data['vue']['siteData']=$data['siteData'];
        $data['vue']['quickLinks']=$data['quickLinks'];
        $data['vue']['unitLinks']=$data['unitLinks'];
        $data['vue']['friendLinks']=$data['friendLinks'];
		if ($this->agent->is_mobile ()) {
			gotoUrl(base_url("m/wapmsg/?site=".$site_url));
		} else {
			$this->load->view('tempMsg',$data);
		}
	}

	public function leaveWords(){
		$data['site'] = $this->input->post('site');
		$data['title'] = $this->input->post('title');
		$data['email'] = $this->input->post('email');
		$data['add_time'] = date("Y-m-d H:i:s",time());
		$data['content'] = $this->input->post('content');
		$data['is_reply'] = 0;
		$site_url = $this->input->post('site_url');

		$re = $this->db->insert('web_u_m_msg_board',$data);

		if ($re) {
			$this->show_msg("提交成功",base_url('levelword/?site='.$site_url));
		}else{
			$this->show_msg("提交成功",base_url('levelword/?site='.$site_url));
		}
	}
	public function show_msg($msg,$url){
		echo "<script type='text/javascript'>alert('{$msg}');window.location.href='{$url}';</script>";
	}
}
