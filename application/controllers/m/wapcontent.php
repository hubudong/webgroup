<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class WapContent extends My_Controller {
	private $path;
	public function __construct(){
		parent::__construct();
		$this->load->library ( 'user_agent' );
		$this->path=Ver.Wap_Platform;
	}
	//分站首页调用
	public function index(){
		$id = $this->input->get('id');
		$site_url = $this->input->get('site');
		$siteData = $this->site_model->getSiteData($site_url);
		$data['siteData'] = $siteData;
		if (empty($siteData)) {
			exit("参数不正确");
		}
		$articleCon = $this->article_model->getArticleCon($id);
		$cid = $articleCon['category'];
		$data['category_name'] = $this->category_model->getCategoryName($cid);
		$data['articleCon'] = $articleCon;
		$data['firstCategory'] = $this->category_model->getFirstCategory($siteData['classid']);
		$data['secondCategory'] = $this->category_model->getSecondCategory($siteData['classid']);
		$data['links'] = $this->object_array($this->link_model->getAllLinks($siteData['classid']));
		$data["resourcePath"]=base_url("templates")."/".Ver;
		if ($this->agent->is_mobile ()) {
			$this->load->view($this->path.'site_content',$data);
		} else {
			gotoUrl(base_url("depsiteCon/?site=".$site_url."&id=".$id));
		}
	}
}