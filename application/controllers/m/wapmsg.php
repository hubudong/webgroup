<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class WapMsg extends My_Controller {
	private $path;
	public function __construct(){
		parent::__construct();
		$this->path=Ver.Wap_Platform;
	}
	//分站首页调用
	public function index(){
		$site_url = $this->input->get('site');
		$siteData = $this->site_model->getSiteData($site_url);
		if (empty($siteData)) {
			exit("参数不正确");
		}
		$data['siteData'] = $siteData;
		$data['firstCategory'] = $this->category_model->getFirstCategory($siteData['classid']);
		$data['secondCategory'] = $this->category_model->getSecondCategory($siteData['classid']);
		$data['links'] = $this->object_array($this->link_model->getAllLinks($siteData['classid']));
		$data["resourcePath"]=base_url("templates")."/".Ver;
		if ($this->agent->is_mobile ()) {
			$this->load->view($this->path.'wap_msg',$data);
		} else {
			gotoUrl(base_url("levelword/?site=".$site_url));
		}
	}
	public function leaveWords(){
		$data['site'] = $this->input->post('site');
		$data['title'] = $this->input->post('title');
		$data['email'] = $this->input->post('lxfs');
		$data['add_time'] = date("Y-m-d H:i:s",time());
		$data['content'] = $this->input->post('content');
		$data['is_reply'] = 0;
		$site_url = $this->input->post('site_url');

		$re = $this->db->insert('web_u_m_msg_board',$data);

		if ($re) {
			$this->show_msg("提交成功",base_url('m/wapmsg/?site='.$site_url));
		}else{
			$this->show_msg("提交成功",base_url('m/wapmsg/?site='.$site_url));
		}
	}
	public function show_msg($msg,$url){
		echo "<script type='text/javascript'>alert('{$msg}');window.location.href='{$url}';</script>";
	}
}