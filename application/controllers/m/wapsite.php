<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class WapSite extends My_Controller {
	private $path;
	public function __construct(){
		parent::__construct();
		$this->path=Ver.Wap_Platform;
	}
	//分站首页调用
	public function index(){
		$site_url = $this->input->get('site');
		$siteData = $this->site_model->getSiteData($site_url);
		if (empty($siteData)) {
			exit("参数不正确");
		}
		$tags = $this->temptags_model->getTags($siteData['classid'],1);
		if (!empty($tags)) {
			foreach ($tags as $key=>$value){
				$data[$value['tags']] = $this->article_model->getArticle($siteData['user'],5,$value['article_cate']);
			}
		}
		$data['banner'] = $this->banner_model->getBannerData($siteData['classid']);
		$data['siteData'] = $siteData;
		$data['tags'] = $tags;
		$data['firstCategory'] = $this->category_model->getFirstCategory($siteData['classid']);
		$data['secondCategory'] = $this->category_model->getSecondCategory($siteData['classid']);
		$data['links'] = $this->object_array($this->link_model->getAllLinks($siteData['classid']));
		$data["resourcePath"]=base_url("templates")."/".Ver;
		if ($this->agent->is_mobile ()) {
			$this->load->view($this->path.'site_index',$data);
		} else {
			gotoUrl(base_url("depsites/?site=".$site_url));
		}
	}
}