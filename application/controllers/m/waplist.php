<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class WapList extends My_Controller {
	private $path;
// 	private $pageSize = 10;
	public function __construct(){
		parent::__construct();
		$this->path=Ver.Wap_Platform;
	}
	//分站首页调用
	public function index(){
		$pageSize = 15;
		$cid = $this->input->get('id');
		$site_url = $this->input->get('site');
		$page = $this->input->get('page');
		$page = empty($page)?1:$page;
		$siteData = $this->site_model->getSiteData($site_url);
		if (empty($siteData)) {
			exit("参数不正确");
		}
		$param = $this->article_model->getListPageData($cid,$siteData['classid'],$pageSize,$page);
		$data['param'] = $param;
		$data['siteData'] = $siteData;
		$data['cid'] = $cid;
		$data['firstCategory'] = $this->category_model->getFirstCategory($siteData['classid']);
		$data['secondCategory'] = $this->category_model->getSecondCategory($siteData['classid']);
		$data['links'] = $this->object_array($this->link_model->getAllLinks($siteData['classid']));
		$data['category_name'] = $this->category_model->getCategoryName($cid);
		$data['articleList'] = $this->article_model->getArticleList($cid,$siteData['classid'],$page,$pageSize);
		$data["resourcePath"]=base_url("templates")."/".Ver;
		if ($this->agent->is_mobile ()) {
			$this->load->view($this->path.'site_list',$data);
		} else {
			gotoUrl(base_url("depsiteList/?site=".$site_url."&id=".$cid));
		}
	}
}