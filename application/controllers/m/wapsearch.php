<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class WapSearch extends My_Controller {
	private $path;
	// 	private $pageSize = 10;
	public function __construct(){
		parent::__construct();
		$this->path=Ver.Wap_Platform;
	}
	//分站首页调用
	public function index(){
		$pageSize = 15;
		$keyword = $this->input->post('keyword');
		$keyword = empty($keyword)?$this->input->get('key'):$keyword;
		$site_url = $this->input->post('site');
		$site_url = empty($site_url)?$this->input->get('site'):$site_url;
		$page = $this->input->get('page');
		$page = empty($page)?1:$page;
		$siteData = $this->site_model->getSiteData($site_url);
		if (empty($siteData)) {
			exit("参数不正确");
		}
		$data['siteData'] = $siteData;
		$param = $this->article_model->getSearchPageData($siteData['user'],$keyword,$pageSize,$page);
		$data['param'] = $param;
		$data['keyword'] = $keyword;
		$data['articleList'] = $this->article_model->getSearchList($siteData['classid'],$keyword,$pageSize,$page);
		$data['firstCategory'] = $this->category_model->getFirstCategory($siteData['classid']);
		$data['secondCategory'] = $this->category_model->getSecondCategory($siteData['classid']);
		$data['links'] = $this->object_array($this->link_model->getAllLinks($siteData['classid']));
		$data["resourcePath"]=base_url("templates")."/".Ver;
		if ($this->agent->is_mobile ()) {
			$this->load->view($this->path.'wap_search',$data);
		} else {
			gotoUrl(base_url("search/?site=".$site_url."&key=".$keyword."&page=".$page));
		}
	}
}