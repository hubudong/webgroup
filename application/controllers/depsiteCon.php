<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DepsiteCon extends My_Controller {

	public function __construct(){
		parent::__construct();
		$this->path=Ver.Wap_Platform;
	}
	//分站列表页
	public function index(){
		$site_url = $this->input->get('site');
		$id = $this->input->get('id');
		$siteData = $this->site_model->getSiteData($site_url);
		$data['siteTemp'] = $this->temp_model->getTempData($siteData['temp']);
		$data['firstCategory'] = $this->category_model->getFirstCategory($siteData['classid']);
		$data['secondCategory'] = $this->category_model->getSecondCategory($siteData['classid']);
		$articleCon = $this->article_model->getArticleCon($id);
		$cid = $articleCon['category'];
		$parentid = $this->object_array($this->category_model->getParentid($cid))['parentid'];
		if ($parentid==0) {
			$data['cid'] = $cid;
			$data['category_name_first'] = $this->category_model->getCategoryName($cid);
			$subCate = $this->object_array($this->category_model->getAllSubCate($cid));
		}else{
			$data['cid'] = $parentid;
			$data['category_name_first'] = $this->category_model->getCategoryName($parentid);
			$data['category_name'] = $this->category_model->getCategoryName($cid);
			$subCate = $this->object_array($this->category_model->getAllSubCate($parentid));
		}
		$data['subCate'] = $subCate;
		$data['articleCon'] = $articleCon;
		$data['siteData'] = $siteData;
		$data['quickLinks'] = $this->object_array($this->link_model->getQuickLinks(1,$siteData['classid']));
		$data['unitLinks'] = $this->object_array($this->link_model->getQuickLinks(2,$siteData['classid']));
		$data['friendLinks'] = $this->object_array($this->link_model->getQuickLinks(3,$siteData['classid']));
        //导航
        $data['vue']['nav']=$data['firstCategory'];
        for($i=0;$i<count($data['vue']['nav']);$i++){
            for($j=0;$j<count($data['secondCategory']);$j++){
                if($data['vue']['nav'][$i]['classid']==$data['secondCategory'][$j]['parentid']){
                    $data['vue']['nav'][$i]['child'][]=$data['secondCategory'][$j];
                }
            }
        }
        $data['vue']['nav'][]=array('category_name'=>'留言','slug'=>base_url('levelword').'/?site='.$siteData['site_url']);
        
        //侧边栏
        $data['vue']['subCate']=$data['subCate'];
        $data['vue']['sidebar']['prev']=$data['category_name_first'];
        if(!empty($data['category_name'])){
            $data['vue']['sidebar']['name']=$data['category_name'];
        }
        
        //内容
        $data['vue']['articleCon']=$data['articleCon'];

        //底部
        $data['vue']['siteData']=$data['siteData'];
        $data['vue']['quickLinks']=$data['quickLinks'];
        $data['vue']['unitLinks']=$data['unitLinks'];
        $data['vue']['friendLinks']=$data['friendLinks'];
        
		if ($this->agent->is_mobile ()) {
			gotoUrl(base_url("m/wapcontent/?site=".$site_url."&id=".$id));
		} else {
			$this->load->view('tempContent',$data);
		}
	}
}