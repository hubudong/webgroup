<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class DepsiteList extends My_Controller {

	public function __construct(){
		parent::__construct();
	}
	//分站列表页
	public function index(){
		$pageSize = 15;
		$site_url = $this->input->get('site');
		$id = $this->input->get('id');
		$page = $this->input->get('page');
		$page = empty($page)?1:$page;
		$siteData = $this->site_model->getSiteData($site_url);

		$param = $this->article_model->getListPageData($id,$siteData['classid'],$pageSize,$page);
		$data['param'] = $param;
		$data['cid'] = $id;
		$data['siteTemp'] = $this->temp_model->getTempData($siteData['temp']);
		$data['firstCategory'] = $this->category_model->getFirstCategory($siteData['classid']);
		$data['secondCategory'] = $this->category_model->getSecondCategory($siteData['classid']);
		$parentid = $this->object_array($this->category_model->getParentid($id))['parentid'];
		if ($parentid==0) {
			$data['id'] = $id;
			$data['category_name_first'] = $this->category_model->getCategoryName($id);
			$subCate = $this->object_array($this->category_model->getAllSubCate($id));
		}else{
			$data['id'] = $parentid;
			$data['category_name_first'] = $this->category_model->getCategoryName($parentid);
			$data['category_name'] = $this->category_model->getCategoryName($id);
			$subCate = $this->object_array($this->category_model->getAllSubCate($parentid));
		}
		$data['subCate'] = $subCate;
		$data['articleList'] = $this->article_model->getArticleList($id,$siteData['classid'],$page,$pageSize);
		$data['siteData'] = $siteData;
		$data['quickLinks'] = $this->object_array($this->link_model->getQuickLinks(1,$siteData['classid']));
		$data['unitLinks'] = $this->object_array($this->link_model->getQuickLinks(2,$siteData['classid']));
		$data['friendLinks'] = $this->object_array($this->link_model->getQuickLinks(3,$siteData['classid']));
		//导航
		$data['vue']['nav']=$data['firstCategory'];
        for($i=0;$i<count($data['vue']['nav']);$i++){
            for($j=0;$j<count($data['secondCategory']);$j++){
                if($data['vue']['nav'][$i]['classid']==$data['secondCategory'][$j]['parentid']){
                    $data['vue']['nav'][$i]['child'][]=$data['secondCategory'][$j];
                }
            }
        }
        $data['vue']['nav'][]=array('category_name'=>'留言','slug'=>base_url('levelword').'/?site='.$siteData['site_url']);
        
        //侧边栏
        $data['vue']['subCate']=$data['subCate'];
        $data['vue']['sidebar']['prev']=$data['category_name_first'];
        $data['vue']['sidebar']['id']=$data['id'];
        if(!empty($data['category_name'])){
            $data['vue']['sidebar']['name']=$data['category_name'];
        }
        
        //内容
		$data['vue']['articleList']=$data['articleList'];
		for($i=0;$i<count($data['articleList']);$i++){
		    $data['vue']['articleList'][$i]['content']=trim(nl2br(strip_tags($data['vue']['articleList'][$i]['content'])));
		}
		$data['vue']['pagination']=$data['param'];
        //底部
		$data['vue']['siteData']=$data['siteData'];
		$data['vue']['quickLinks']=$data['quickLinks'];
		$data['vue']['unitLinks']=$data['unitLinks'];
        $data['vue']['friendLinks']=$data['friendLinks'];
        
		if ($this->agent->is_mobile ()) {
			gotoUrl(base_url("m/waplist/?site=".$site_url."&id=".$id));
		} else {
			$this->load->view('tempList',$data);
		}

	}
}