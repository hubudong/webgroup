<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="<?=$resourcePath?>css/wap.css"/>
    <title>景德镇陶瓷大学站群系统</title>
</head>
<body>
	<div class="wrap">
		<div class="center">
			<div class="pic">
				<div class="logo">
					<img src="<?=$resourcePath?>img/logo.png" />
				</div>
				<div class="site_logo">
					<img src="<?=$resourcePath?>img/site.png" />
				</div>
			</div>
			<div class="scroll">
				<div class="site_list">
					<ul class="site_ul">
						<!--  li class="site_li"><a href="#">信息工程学院</a></li>
						<li class="site_li"><a href="#">信息工程学院</a></li>
						<li class="site_li"><a href="#">信息工程学院</a></li>
						<li class="site_li"><a href="#">信息工程学院</a></li>
						<li class="site_li"><a href="#">信息工程学院</a></li>
						<li class="site_li"><a href="#">信息工程学院</a></li-->
						<?php
							if (!empty($site_list)) {
								foreach ($site_list as $key=>$value){
						?>
						<li class="site_li"><a href="<?=base_url('m/wapsite/?site='.$value['site_url'])?>"><?=$value['site_name']?></a></li>
						<?php
								}
							}
						?>
					</ul>
				</div>
				<div class="foot">
					<div class="site_active">
					<!--
						<a href="<?=base_url('admin')?>">后台登录</a>
					 -->
						<a href="<?=base_url('manager')?>" style="background:#0b80a7;">站点登录</a>
					</div>
					<div class="site_info">
						<p> 联系电话：<a href="tel:87XXXXXX">87XXXXXX</a> <a href="tel:139XXXXXXXX">139XXXXXXXX</a> </p>
						<p>Copyright &copy; 2016</p>
						<p>技术支持：<a style="text-decoration:none;color:blue;" href="http://mia.pasp.cn/">移动互联协会</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>