<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="text-title text-warning">
								<b>友情链接</b>
							</h4>
						</div>
						<ul class="list-group clearfix">
							<?php
								if (!empty($links)) {
									foreach ($links as $key=>$value){
							?>
							<li class="list-group-item col-md-3 col-sm-4 col-xs-6 link"><a href="<?=$value['link_url']?>" title="<?=$value['link_name']?>"><?=$value['link_name']?></a></li>
							<?php
									}
								}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<footer class="container-fluid">
			<div class="row">
				<div class="col-xs-12 text-center">
					<p>

					</p>
					<p><?=$siteData['site_copy']?></p>
				</div>
			</div>
		</footer>