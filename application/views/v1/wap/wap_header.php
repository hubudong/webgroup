<header class="container-fluid">
			<div class="row">
				<div class="logo col-xs-2">
					<img alt="" src="<?=$resourcePath?>img/jci_bg.png">
				</div>
				<div class="header-title col-xs-8">
					<span><?=$siteData['site_name']?></span>
				</div>
				<div class="header-nav col-xs-2">
					<a class="btn pull-right" href="#" role="button">
						<span class="glyphicon glyphicon-list" aria-hidden="true"></span>
					</a>
				</div>
			</div>
		</header>
		<div class="container-fluid header-nav-menu hide">
			<?php
				$i = 0;
				$str = "<div class='row'><a class='col-xs-2' href='".base_url('m/wapsite/?site='.$siteData['site_url'])."'>首页</a>";
				foreach ($firstCategory as $key=>$value){
					if ($i==5) {
						$str.="</div><div class='row'><a class='col-xs-2' href='".base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$value['classid'])."'>".$value['category_name']."</a>";
					}else{
						$str.="<a class='col-xs-2' href='".base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$value['classid'])."'>".$value['category_name']."</a>";
					}
					$i++;
					foreach ($secondCategory as $k=>$v){
						if ($value['classid']==$v['parentid']) {
							if ($i==5) {
								$str.="</div><div class='row'><a class='col-xs-2' href='".base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$v['classid'])."'>".$v['category_name']."</a>";
							}else{
								$str.="<a class='col-xs-2' href='".base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$v['classid'])."'>".$v['category_name']."</a>";
							}
						}
						$i++;
					}
				}
				$str .= "<a class='col-xs-2' href='".base_url('/m/wapmsg/?site='.$siteData['site_url'])."'>留言</a></div>";
				echo $str;
			?>
		</div>
		<div class="container-fluid search">
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1">
					<form action="<?=base_url('m/wapsearch')?>" method="post">
						<div class="input-group">
							<input class="site" type="hidden" name="site" value="<?=$siteData['site_url']?>">
							<input type="text" class="form-control" name="keyword">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-primary">
									<span class="glyphicon glyphicon-search" ></span>搜索
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>