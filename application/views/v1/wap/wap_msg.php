<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title><?=$siteData['site_name']?></title>
		<?php require_once 'wap_loadSource.php'; ?>
	</head>
	<body>
		<header class="container-fluid">
			<div class="row">
				<div class="header-return col-xs-2">
					<a class="btn" href="javascript:history.back(-1)" role="button">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					</a>
				</div>
				<div class="header-title col-xs-8">
					<span>留言</span>
				</div>
				<div class="header-nav col-xs-2">
					<a class="btn pull-right" href="#" role="button">
						<span class="glyphicon glyphicon-list" aria-hidden="true"></span>
					</a>
				</div>
			</div>
		</header>
		<div class="container-fluid header-nav-menu hide">
			<?php
				$i = 0;
				$str = "<div class='row'><a class='col-xs-2' href='".base_url('m/wapsite/?site='.$siteData['site_url'])."'>首页</a>";
				foreach ($firstCategory as $key=>$value){
					if ($i==5) {
						$str.="</div><div class='row'><a class='col-xs-2' href='".base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$value['classid'])."'>".$value['category_name']."</a>";
					}else{
						$str.="<a class='col-xs-2' href='".base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$value['classid'])."'>".$value['category_name']."</a>";
					}
					$i++;
					foreach ($secondCategory as $k=>$v){
						if ($value['classid']==$v['parentid']) {
							if ($i==5) {
								$str.="</div><div class='row'><a class='col-xs-2' href='".base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$v['classid'])."'>".$v['category_name']."</a>";
							}else{
								$str.="<a class='col-xs-2' href='".base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$v['classid'])."'>".$v['category_name']."</a>";
							}
						}
						$i++;
					}
				}
				$str .= "<a class='col-xs-2' href='".base_url('/m/wapmsg/?site='.$siteData['site_url'])."'>留言</a></div>";
				echo $str;
			?>
		</div>
		<div class="container-fluid list-content">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="msg">
							<form class="form-horizontal" role="form" action="<?=base_url('m/wapmsg/leaveWords')?>" method="post">
								<div class="form-group">
								    <div class="col-xs-10 col-xs-offset-1">
								      <input type="text" class="form-control" name="title" id="title" placeholder="请输入标题">
								    </div>
								</div>
								<div class="form-group">
								    <div class="col-xs-10 col-xs-offset-1">
								      <input type="email" class="form-control" name="email" id="email" placeholder="请输入邮箱地址">
								    </div>
								</div>
								<div class="form-group">
									<div class="col-xs-10 col-xs-offset-1">
								    	<textarea class="form-control" rows="5" name="content"></textarea>
								    </div>
								</div>
								<input type="hidden" name="site" value="<?=$siteData['classid']?>">
								<input type="hidden" name="site_url" value="<?=$siteData['site_url']?>">
								<button type="submit" class="btn btn-primary submit">提交</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php require_once 'wap_footer.php'; ?>
	</body>
</html>