<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title><?=$siteData['site_name']?></title>
		<?php require_once 'wap_loadSource.php'; ?>
	</head>
	<body>
		<header class="container-fluid">
			<div class="row">
				<div class="header-return col-xs-2">
					<a class="btn" href="javascript:history.back(-1)" role="button">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					</a>
				</div>
				<div class="header-title col-xs-8">
					<span>详情</span>
				</div>
				<div class="header-nav col-xs-2">
					<a class="btn pull-right" href="#" role="button">
						<span class="glyphicon glyphicon-list" aria-hidden="true"></span>
					</a>
				</div>
			</div>
		</header>
		<div class="container-fluid header-nav-menu hide">
			<?php
				$i = 0;
				$str = "<div class='row'><a class='col-xs-2' href='".base_url('m/wapsite/?site='.$siteData['site_url'])."'>首页</a>";
				foreach ($firstCategory as $key=>$value){
					if ($i==5) {
						$str.="</div><div class='row'><a class='col-xs-2' href='".base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$value['classid'])."'>".$value['category_name']."</a>";
					}else{
						$str.="<a class='col-xs-2' href='".base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$value['classid'])."'>".$value['category_name']."</a>";
					}
					$i++;
					foreach ($secondCategory as $k=>$v){
						if ($value['classid']==$v['parentid']) {
							if ($i==5) {
								$str.="</div><div class='row'><a class='col-xs-2' href='".base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$v['classid'])."'>".$v['category_name']."</a>";
							}else{
								$str.="<a class='col-xs-2' href='".base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$v['classid'])."'>".$v['category_name']."</a>";
							}
						}
						$i++;
					}
				}
				$str .= "<a class='col-xs-2' href='".base_url('/m/wapmsg/?site='.$siteData['site_url'])."'>留言</a></div>";
				echo $str;
			?>
		</div>
		<div class="container-fluid search">
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1">
					<form action="<?=base_url('m/wapsearch')?>" method="post">
						<div class="input-group">
							<input class="site" type="hidden" name="site" value="<?=$siteData['site_url']?>">
							<input type="text" class="form-control" name="keyword">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-primary">
									<span class="glyphicon glyphicon-search" ></span>搜索
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="container-fluid list-content">
			<div class="row">
				<div class="col-xs-12">
					<div class="content">
						<h4 style="text-align: center;"><b><?=$articleCon['title']?></b></h4>
						<hr/>
						<p class="clearfix" style="text-align: center;text-indent:0;">
							<span class="col-xs-6">文章作者：<?=$articleCon['author']?></span>
							<span class="col-xs-6">文章来源：<?=$articleCon['source']?></span>
						</p>
						<p class="clearfix" style="text-align: center;text-indent:0;">
							<span class="col-xs-6">发布时间：<?=date("Y-m-d",strtotime($articleCon['add_time']))?></span>
							<span class="col-xs-6">点击量：<?=$articleCon['hits']?></span>
						</p>
						<hr/>
						<p style="text-indent:2em;padding:5px 10px;">
							<?=$articleCon['content']?>
						</p>
					</div>
				</div>
			</div>
		</div>
		<?php require_once 'wap_footer.php'; ?>
	</body>
</html>