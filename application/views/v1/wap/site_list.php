<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title><?=$siteData['site_name']?></title>
		<?php require_once 'wap_loadSource.php'; ?>
	</head>
	<body>
		<?php require_once 'wap_header.php'; ?>
		<div class="container-fluid list-content">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading clearfix">
							<h4 class="text-title text-warning pull-left"><strong><?=$category_name['category_name']?></strong></h4>
						</div>
						<ul class="list-group">
						<?php
							if (!empty($articleList)) {
								foreach ($articleList as $key=>$value){
						?>
						<li class="list-group-item"><a href="<?=base_url('m/wapcontent/?site='.$siteData['site_url'].'&id='.$value['id'])?>"><?=$value['title']?></a></li>
						<?php
								}
							}else{
								echo '<li class="list-group-item"><a href="#">暂无新闻</a></li';
							}
						?>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<nav>
						  <ul class="pager">
						    <li class="col-xs-4 col-xs-offset-1"><a href="<?php if($param['page']<=1){ echo 'javascript:;';}else{ echo base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$cid.'&page='.($param['page']-1));} ?>">上一页</a></li>
						    <li class="col-xs-4 col-xs-offset-2"><a href="<?php if($param['page']>=$param['pages']){echo 'javascript:;';}else{ echo base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$cid.'&page='.($param['page']+1));} ?>">下一页</a></li>
						  </ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<?php require_once 'wap_footer.php'; ?>
	</body>
</html>