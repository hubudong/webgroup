<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title><?=$siteData['site_name']?></title>
		<?php require_once 'wap_loadSource.php'; ?>
	</head>
	<body>
		<?php require_once 'wap_header.php'; ?>
		<div class="container nav-panel">
			<div class="row">
				<div id="myCarousel" class="carousel slide">
					<!-- 轮播（Carousel）指标 -->
					<?php
						$j = 0;
						if (!empty($banner)) {
							foreach ($banner as $key=>$value){
								$j++;
							}
						}
					?>
					<ol class="carousel-indicators">
					<?php
						if ($j==0) {
							echo '<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>';
						}else{
							for ($k=0;$k<$j;$k++){
								if ($k==0) {
									echo '<li data-target="#myCarousel" data-slide-to="0" class="active"></li>';
								}else{
					?>
									<li data-target="#myCarousel" data-slide-to="<?=$k?>"></li>
					<?php
								}
							}
						}
					?>
					</ol>
					<!-- 轮播（Carousel）项目 -->
					<div class="carousel-inner">
						<?php
							if ($j==0) {
								echo '<div class="item active">
							<a href="#"><img src="'.$resourcePath.'img/slide1.png" alt=""></a>
							<div class="carousel-caption">标题 1</div>
						</div>
						<div class="item">
							<img src="'.$resourcePath.'img/slide2.png" alt="">
							<div class="carousel-caption">标题 2</div>
						</div>
						<div class="item">
							<img src="'.$resourcePath.'img/slide3.png" alt="">
							<div class="carousel-caption">标题 3</div>
						</div>';
							}else{
								foreach ($banner as $k=>$v){
									if ($k==0) {
										echo '<div class="item active">
							<a href="'.$v['url'].'"><img src="'.base_url('attachments').$v['banner'].'" alt="'.$v['title'].'"></a>
							<!--div class="carousel-caption">'.$v['title'].'</div-->
						</div>';
									}else{
						?>
						<div class="item">
							<a href="<?=$v['url']?>"><img src="<?=base_url('attachments').$v['banner']?>" alt="<?=$v['title']?>"></a>
							<!--div class="carousel-caption"><?=$v['title']?></div-->
						</div>
						<?php
									}
								}
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<?php
			$arr_index = array();
			if (!empty($tags)){
				foreach ($tags as $key=>$value){
					${$value['tags'].'code'} = '';
					foreach ($$value['tags'] as $k=>$v){
						${$value['tags'].'code'} .= '<li class="list-group-item"><a href="'.base_url('m/wapcontent').'/?site='.$siteData['site_url'].'&id='.$v['id'].'"><span class="newsTxt">'.$v['title'].'</span></a></li>';
						$arr_index = Array('@@'.$value['tags'].'@@' => ${$value['tags'].'code'})+$arr_index;
					}
				}
			}
		?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading clearfix">
							<?php 
								if ($tags) {
							?>
							<h4 class="text-title text-warning pull-left"><strong>新闻动态</strong></h4>
							<?php 
								}else{
									echo '<h4 class="text-title text-warning pull-left"><strong>暂无栏目</strong></h4>';
								}
							?>
							<?php 
								if(!empty($tags)){
							?>
							<a class="more pull-right" href="<?=base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$tags['0']['article_cate'])?>">
							<?php 
								}else{
							?>
							<a class="more pull-right" href="#">
							<?php 		
								}
							?>
								<span class="glyphicon glyphicon-plus text-title" aria-hidden="true"></span>更多
							</a>
						</div>
						<ul class="list-group">
							<!-- li class="list-group-item"><a href="#">广西首家农产品精深加工科技企业孵化器落户贵港</a></li-->
							<?php
								if (!empty($arr_index)) {
									echo $arr_index['@@新闻@@'];
								}else {
									echo "暂无新闻";
								}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading clearfix">
						<?php 
						if(!empty($tags)){
						?>
							<h4 class="text-title text-warning pull-left"><strong>通知公告</strong></h4>
						<?php 
							}else{
								echo '<h4 class="text-title text-warning pull-left"><strong>暂无栏目</strong></h4>';
							}
						?>
							<?php 
								if(!empty($tags)){
							?>
							<a class="more pull-right" href="<?=base_url('m/waplist/?site='.$siteData['site_url'].'&id='.$tags['1']['article_cate'])?>">
							<?php 
								}else{
							?>
							<a class="more pull-right" href="#">
							<?php 
								}
							?>
								<span class="glyphicon glyphicon-plus text-title" aria-hidden="true"></span>更多
							</a>
						</div>
						<ul class="list-group">
							<?php
								if (!empty($arr_index)) {
									echo $arr_index['@@通知@@'];
								}else {
									echo "暂无新闻";
								}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?php require_once 'wap_footer.php'; ?>
	</body>
	<script>
		$(function(){
        	// 初始化轮播
       		$("#myCarousel").carousel('cycle');
        });
    </script>
</html>
