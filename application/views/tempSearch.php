<?php
	$this->load->view('tempTop');
	//列表页
	$articleCode = "";
	$friendCode = "";
	$subCode = "";
	$pageCode = "";
	if (!empty($articleList)) {
		foreach ($articleList as $key=>$value){
			$articleCode .= '<li class="newsLi clr"><a href="'.base_url('depsiteCon').'/?site='.$siteData['site_url'].'&id='.$value['id'].'"><span class="newsTxt">'.$value['title'].'</span><span class="newsTime">'.date("Y-m-d",strtotime($value['add_time'])).'</span></a></li>';
		}
	}else{
		$articleCode = "<li class='newsLi clr'><span class='newsTxt'>没有数据</span></li>";
	}
	if (!empty($friendLinks)) {
		foreach ($friendLinks as $key=>$value){
			$friendCode .= "<option value='".$value['id']."'>".$value['link_name']."</option>";
		}
	}
	$upCode = "";
	$downCode = "";
	if (!empty($param)) {
		if ($param['page']<=1) {
			$upCode = "javascript:;";
		}else{
			$upCode = base_url('search/?site='.$siteData['site_url'].'&keywords='.$keywords.'&page='.($param['page']-1));
		}
		if ($param['page']>=$param['pages']) {
			$downCode = "javascript:;";
		}else{
			$downCode = base_url('search/?site='.$siteData['site_url'].'&keywords='.$keywords.'&page='.($param['page']+1));
		}
	}
	$pageCode.="<a href='".$upCode."'>上一页</a>&nbsp;<span>共".$param['pages']."页&nbsp;".$param['pageMaxSize']."条<span/>&nbsp;<a href='".$downCode."'>下一页</a>";
	$indexCode = base_url('depsites/?site='.$siteData['site_url']);
	$arr_list = Array(
			"@@关键字@@" => $keywords,
			"@@首页链接@@" => $indexCode,
			"@@数量@@" => $param['pageMaxSize'],
			"@@搜索列表@@" => $articleCode,
			"@@友情链接@@" => $friendCode,
			"@@翻页@@" => $pageCode
			);
	echo strtr($siteTemp['search'],$arr_list);

	$this->load->view('tempBottom');
?>