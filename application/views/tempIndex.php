<?php
	//头部
	$this->load->view('tempTop');
	//主页内容
	$arr_index = array();
	if (!empty($tags)){
		foreach ($tags as $key=>$value){
			${$value['tags'].'code'} = '';
			foreach ($$value['tags'] as $k=>$v){
				${$value['tags'].'code'} .= '<li class="newsLi clr"><a href="'.base_url('depsiteCon').'/?site='.$siteData['site_url'].'&id='.$v['id'].'"><span class="newsTxt">'.$v['title'].'</span><span class="newsTime">'.date("Y-m-d",strtotime($v['add_time'])).'</span></a></li>';
				$arr_index = Array('@@'.$value['tags'].'@@' => ${$value['tags'].'code'})+$arr_index;
			}
		}
	}
	$pictureCode = [];
	$titleCode = [];
	$urlCode = [];
	//$focus遍历
	if (!empty($focus)) {
		foreach ($focus as $key=>$value){
			$pictureCode[]= base_url('attachments').$value['focus'];
			$titleCode[]= $value['title'];
			$urlCode[]= base_url()."depsiteCon/?site={$siteData['site_url']}&id={$value['id']}";
		}
	}
	$pictureCode = substr(json_encode($pictureCode),1 ,-1);
	$titleCode = substr(json_encode($titleCode),1 ,-1);
	$urlCode = substr(json_encode($urlCode),1 ,-1);
	$arr_index += Array("@@焦点图@@" => $pictureCode,"@@焦点图标题@@"=>$titleCode,"@@焦点图链接@@" => $urlCode);

	//banner图
	$bannerArray = [];
	if (!empty($banner)) {
		foreach ($banner as $key=>$value){
			$bannerArray[]=[
			'txt' => $value['title'],
			'url' => $value['url'],
			'pic' => base_url('attachments').$value['banner'],
			];
		}
	}
	$bannerCode = substr(json_encode($bannerArray),1,-1);
	$arr_index += Array("@@Banner@@" => $bannerCode);
	echo strtr($siteTemp['index'],$arr_index);

	//底部
	$this->load->view('tempBottom');
?>
