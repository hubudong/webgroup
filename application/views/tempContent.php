<?php
	$this->load->view('tempTop');
	$friendCode = "";
	$subCode = "";
	if (!empty($friendLinks)) {
		foreach ($friendLinks as $key=>$value){
			$friendCode .= "<option value='".$value['id']."'>".$value['link_name']."</option>";
		}
	}
	if (!empty($subCate)) {
		foreach ($subCate as $key=>$value){
			$subCode .= "<li class='subPageMenuLi'><a href='".base_url('depsiteList').'/?site='.$siteData['site_url'].'&id='.$value['classid']."'>".$value['category_name']."</a></li>";
		}
	}
	$articleCode = "文章作者：".$articleCon['author']." / "."文章来源：".$articleCon['source']." / "."发布时间：".date("Y-m-d",strtotime($articleCon['add_time']))." / "."点击量：".$articleCon['hits'];
	if (empty($category_name_first)) {
		$category_name_first['category_name'] = "";
	}
	if (empty($category_name)) {
		$category_name['category_name'] = "";
	}
	$indexCode = base_url('depsites/?site='.$siteData['site_url']);
	$cateCode = base_url('depsiteList/?site='.$siteData['site_url'].'&id='.$cid);
	$arr_list = Array(
			"@@栏目链接@@" => $cateCode,
			"@@首页链接@@" => $indexCode,
			"@@栏目名1@@" => $category_name_first['category_name'],
			"@@栏目名2@@" => $category_name['category_name'],
			"@@文章标题@@" => $articleCon['title'],
			"@@文章内容@@" => $articleCon['content'],
			"@@文章详细@@" => $articleCode,
			"@@友情链接@@" =>$friendCode,
			"@@侧边导航@@" => $subCode
			);
	echo strtr($siteTemp['content'],$arr_list);

	$this->load->view('tempBottom');
?>