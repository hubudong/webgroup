<?php
    $vueCode="
<script>
var page = new Vue({
  el: '#page',
  data: ".json_encode($vue).",
  computed: {
    base_url: function(){
      return '".base_url()."';
    },
    articleLink: function(){
      return this.base_url+'depsiteCon/?site='+this.siteData.site_url+'&id=';
    },
    searchAction: function () {
      return this.base_url + 'search/?site=' + this.siteData.site_url;
    },
    navLink: function() {
      return this.base_url + 'depsiteList/?site=' + this.siteData.site_url + '&id=';
    },
    indexLink: function () {
      return this.base_url+'depsites/?site='+this.siteData.site_url;
    }
  }
})
</script>
";
	//底部版权信息
	$arr_foot = array(
			"@@copyRight@@" => $siteData['site_copy']
	);
//	链接
	$quickCode = "";
//	单位
	$unitCode = "";
	if (!empty($quickLinks)) {
		foreach ($quickLinks as $key=>$value){
			$quickCode.="<a target='_blank' href='".$value['link_url']."'>".$value['link_name']."</a>&nbsp;";
		}
	}
	if (!empty($unitLinks)) {
		foreach ($unitLinks as $key=>$value){
			$unitCode.="<option value='".$value['link_url']."'>".$value['link_name']."</option>";
		}
	}
	$arr_foot= array("@@链接@@" => $quickCode,"@@单位@@"=>$unitCode)+$arr_foot;
	$arr_foot= array("@@vue@@" => $vueCode)+$arr_foot;
	echo strtr($siteTemp['common_foot'],$arr_foot);
?>