<?php
	$this->load->view('tempTop');
	//列表页
	$articleCode = "";
	$friendCode = "";
	$subCode = "";
	$pageCode = "";
	if (!empty($articleList)) {
		foreach ($articleList as $key=>$value){
			$articleCode .= '<li class="newsLi clr"><a href="'.base_url('depsiteCon').'/?site='.$siteData['site_url'].'&id='.$value['id'].'"><span class="newsTxt">'.$value['title'].'</span><span class="newsTime">'.date("Y-m-d",strtotime($value['add_time'])).'</span></a></li>';
		}
	}else{
		$articleCode = "<li class='newsLi clr'><span class='newsTxt'>暂无新闻</span></li>";
	}
	if (!empty($friendLinks)) {
		foreach ($friendLinks as $key=>$value){
			$friendCode .= "<option value='".$value['id']."'>".$value['link_name']."</option>";
		}
	}
	if (!empty($subCate)) {
		foreach ($subCate as $key=>$value){
			$subCode .= "<li class='subPageMenuLi'><a href='".base_url('depsiteList').'/?site='.$siteData['site_url'].'&id='.$value['classid']."'>".$value['category_name']."</a></li>";
		}
	}
	if (empty($category_name_first)) {
		$category_name_first['category_name'] = "";
	}
	if (empty($category_name)) {
		$category_name['category_name'] = "";
	}
	$upCode = "";
	$downCode = "";
	if ($param['page']<=1) {
		$upCode = "javascript:;";
	}else{
		$upCode = base_url('depsiteList/?site='.$siteData['site_url'].'&id='.$cid.'&page='.($param['page']-1));
	}
	if ($param['page']>=$param['pages']) {
		$downCode = "javascript:;";
	}else{
		$downCode = base_url('depsiteList/?site='.$siteData['site_url'].'&id='.$cid.'&page='.($param['page']+1));
	}
	$pageCode.="<a href='".$upCode."'>上一页</a>&nbsp;<span>共".$param['pages']."页&nbsp;".$param['pageMaxSize']."条<span/>&nbsp;<a href='".$downCode."'>下一页</a>";
	$indexCode = base_url('depsites/?site='.$siteData['site_url']);
	$cateCode = base_url('depsiteList/?site='.$siteData['site_url'].'&id='.$id);
	$arr_list = Array(
			"@@栏目链接@@" => $cateCode,
			"@@首页链接@@" => $indexCode,
			"@@栏目名1@@" => $category_name_first['category_name'],
			"@@栏目名2@@" => $category_name['category_name'],
			"@@列表页@@" => $articleCode,
			"@@友情链接@@" => $friendCode,
			"@@侧边导航@@" => $subCode,
			"@@翻页@@" => $pageCode
			);
	echo strtr($siteTemp['list'],$arr_list);

	$this->load->view('tempBottom');
?>