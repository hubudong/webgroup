<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class My_Model extends CI_Model{
	public function __construct() {
		parent::__construct ();
	}
	//防注入攻击
	public function inject_check($sql_str) {
		return preg_match('/lect|insert|and|or|update|delete|\'|\/\*|\*|\.\.\/|\.\/|union|into|load_file|outfile/', $sql_str);
	}
	//防注入攻击
	public function verify_id($id=null) {
		if(!$id) {
			exit('没有提交参数！');
		} elseif($this->inject_check($id)) {
			exit('提交的参数非法！');
		}
// 		$id = intval($id);
		return $id;
	}
}