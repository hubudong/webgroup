<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class My_Controller extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->db = $this->load->database ( "default", TRUE );//连接数据库
		$this->siteData = $this->db->get($this->db->dbprefix('site_settings'))->row_array();//读取dilicms配置属性
		$this->load->library ( 'user_agent' );
		header ( "content-type:text/html;charset=utf-8" );
		$this->load->model('article_model');
		$this->load->model('temptags_model');
		$this->load->model('site_model');
		$this->load->model('temp_model');
		$this->load->model('category_model');
		$this->load->model('banner_model');
		$this->load->model('link_model');
	}
	//stdClass Object转array
	public function object_array($array) {
		if(is_object($array)) {
			$array = (array)$array;
		} if(is_array($array)) {
			foreach($array as $key=>$value) {
				$array[$key] = $this->object_array($value);
			}
		}
		return $array;
	}
}