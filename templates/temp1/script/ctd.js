$(document).ready(function(e) {
		/*ALL 导航二级菜单*/
		var liWidth = $(".navLi").width();
		var s = 0;
		//var subNavItemsMove=[0,0,0,0,0,0,0,0,0,0];
		var subNavItemsMove = new Array();
		$('.navLi').each(function(index){
			s = index * liWidth;
			subNavItemsMove.push(s);
		});
		$(".subNavItem:first").slideDown('fast');
		$('.navLi').each(function(index, element) {
				$(this).hover(
						function(){
								var subNavItem=$('.subNavItem');
								var subNavItems=$("#subNavItems");
								$(this).addClass("navLiCurrent");
								subNavItem.not(":hidden").css('display','none');
								subNavItem.eq(index).css('display','block');
								subNavItems.css({"left":subNavItemsMove[index]})
						},
						function(){
								$(this).removeClass("navLiCurrent");
						}
				);
		});
		/*ALL LOGO增加链接跳转*/
		$('#logo').on("click",function(){
				window.location.href='#';
		});
		
		/*Index Tab页切换*/
		var tabExists=$('.tab');
		if(tabExists){
				$('.tab').each(function(index, element) {
						$(this).on("click",function(){
								$('.tab.current').removeClass('current');
								$(this).addClass('current');
								$('#news').animate({left:(index==0)?(0):(-600+'px')},400);
								$('#notice').animate({right:(index==0)?(-600+'px'):(0)},400);
						});
				});
		};
		
		/*Index 日期时间*/
		var getDateTimeAndWeek=function (){
				//今天是 2013年5月1日 星期三
				var d=new Date();
				var week;
				switch(d.getDay()){
						case 0:week="星期日";	break;
						case 1:week="星期一";	break;
						case 2:week="星期二";	break;
						case 3:week="星期三";	break;
						case 4:week="星期四";	break;
						case 5:week="星期五";	break;
						case 6:week="星期六";	break;
				}
				return "今天是 "+d.getFullYear()+"年"+(d.getMonth()+1)+"月"+d.getDate()+"日 "+week
		};
		var dTDiv=$('#dTime');
		if(dTDiv){
				dTDiv.text(getDateTimeAndWeek());
		}
});
